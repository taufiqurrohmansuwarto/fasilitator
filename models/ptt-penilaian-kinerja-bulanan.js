const { getKnex } = require("../db/knex");
const knex = getKnex();
const { Model } = require("objection");
const { build } = require("joi");

Model.knex(knex);

class PttPenilaianKinerjaBulanan extends Model {
  static get tableName() {
    return "ptt_penilaian_kinerja_bulanan";
  }

  static get modifiers() {
    return {
      bulananSimple(builder, tahun, bulan) {
        builder.where("tahun", tahun).andWhere("bulan", bulan);
      },
    };
  }

  static get relationMappings() {
    return {
      targetTahunan: {
        modelClass: require("./ptt-penilaian-target-tahunan.model"),
        relation: Model.BelongsToOneRelation,
        join: {
          from: "ptt_penilaian_kinerja_bulanan.id_target_tahunan",
          to: "ptt_penilaian_target_tahunan.id",
        },
      },
    };
  }
}

module.exports = PttPenilaianKinerjaBulanan;
