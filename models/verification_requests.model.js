const { getKnex } = require("../db/knex");
const knex = getKnex();
const { Model } = require("objection");

Model.knex(knex);

class VerificationRequest extends Model {
  static get tableName() {
    return "verification_requests";
  }

  static get idColumn() {
    return "id";
  }
}

module.exports = VerificationRequest;
