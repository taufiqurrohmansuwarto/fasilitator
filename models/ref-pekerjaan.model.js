const { getKnex } = require("../db/knex");
const knex = getKnex();
const { Model } = require("objection");

Model.knex(knex);

class RefPekerjaan extends Model {
  static get tableName() {
    return "ref_pekerjaan";
  }

  static get modifiers() {}

  static get idColumn() {
    return "pekerjaan_id";
  }
}

module.exports = RefPekerjaan;
