const { getKnex } = require("../db/knex");
const knex = getKnex();
const { Model } = require("objection");

Model.knex(knex);

class Jabatan extends Model {
  static get tableName() {
    return "jabatan";
  }

  static get idColumn() {
    return "id_jabatan";
  }

  static get modifiers() {
    return {
      simple(builder) {
        builder.select("id_jabatan", "name");
      },
    };
  }
}

module.exports = Jabatan;
