const { getKnex } = require("../db/knex");
const knex = getKnex();
const { Model } = require("objection");

Model.knex(knex);

class RefStatusSuamiIstri extends Model {
  static get tableName() {
    return "ref_status_suami_istri";
  }

  static get modifiers() {}

  static get idColumn() {
    return "status_suami_istri_id";
  }
}

module.exports = RefStatusSuamiIstri;
