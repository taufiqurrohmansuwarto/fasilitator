const { getKnex } = require("../db/knex");
const knex = getKnex();
const { Model } = require("objection");

Model.knex(knex);

class Skpd extends Model {
  static get tableName() {
    return "skpd";
  }

  static get relationMappings() {
    return {
      children: {
        modelClass: Skpd,
        relation: Model.HasManyRelation,
        join: {
          from: "skpd.id",
          to: "skpd.pId",
        },
      },
    };
  }

  static get modifiers() {
    return {
      simple(builder) {
        builder.select("id", "pId", "name");
      },
    };
  }

  static get idColumn() {
    return "id";
  }
}

module.exports = Skpd;
