const { getKnex } = require("../db/knex");
const knex = getKnex();
const { Model } = require("objection");

Model.knex(knex);

class Sessions extends Model {
  static get tableName() {
    return "sessions";
  }

  static get idColumn() {
    return "id";
  }
}

module.exports = Sessions;
