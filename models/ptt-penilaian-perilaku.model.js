const { getKnex } = require("../db/knex");
const knex = getKnex();
const { Model } = require("objection");

Model.knex(knex);

class PttPenilaianPerilaku extends Model {
  static get tableName() {
    return "ptt_penilaian_perilaku";
  }

  $beforeInsert() {
    this.dibuat_pada = new Date();
  }

  $beforeUpdate() {
    this.diupdate_pada = new Date();
  }
}

module.exports = PttPenilaianPerilaku;
