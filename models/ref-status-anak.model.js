const { getKnex } = require("../db/knex");
const knex = getKnex();
const { Model } = require("objection");

Model.knex(knex);

class RefStatusAnak extends Model {
  static get tableName() {
    return "ref_status_anak";
  }

  static get modifiers() {}

  static get idColumn() {
    return "status_anak_id";
  }
}

module.exports = RefStatusAnak;
