const { getKnex } = require("../db/knex");
const knex = getKnex();
const { Model } = require("objection");

Model.knex(knex);

class SettingAgama extends Model {
  static get tableName() {
    return "setting_agama";
  }

  static get modifiers() {}

  static get idColumn() {
    return "agama_id";
  }
}

module.exports = SettingAgama;
