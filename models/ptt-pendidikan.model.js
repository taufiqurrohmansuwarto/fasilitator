const { getKnex } = require("../db/knex");
const knex = getKnex();
const { Model } = require("objection");
const Jenjang = require("./jenjang.model");

Model.knex(knex);

class PttPendidikan extends Model {
  static get tableName() {
    return "ptt_pendidikan";
  }

  static get modifiers() {
    return {
      aktif(builder) {
        builder.where("ptt_pendidikan.aktif", "Y");
      },
    };
  }

  static get relationMappings() {
    return {
      jenjang: {
        modelClass: Jenjang,
        relation: Model.BelongsToOneRelation,
        join: {
          from: "jenjang.id_jenjang",
          to: "ptt_pendidikan.id_jenjang",
        },
      },
    };
  }

  static get idColumn() {
    return "id_ptt_pendidikan";
  }
}

module.exports = PttPendidikan;
