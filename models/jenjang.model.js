const { getKnex } = require("../db/knex");;
const knex = getKnex();
const { Model } = require("objection");

Model.knex(knex);

class Jenjang extends Model {
  static get tableName() {
    return "jenjang";
  }

  static get idColumn() {
    return "id_jenjang";
  }
}

module.exports = Jenjang;
