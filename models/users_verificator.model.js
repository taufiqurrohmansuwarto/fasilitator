const { getKnex } = require("../db/knex");
const knex = getKnex();
const { Model } = require("objection");

Model.knex(knex);

class UsersVerificator extends Model {
  static get tableName() {
    return "users_verificator";
  }

  static get idColumn() {
    return "id";
  }
}

module.exports = UsersVerificator;
