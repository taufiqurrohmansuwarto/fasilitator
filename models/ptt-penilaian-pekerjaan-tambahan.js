const { getKnex } = require("../db/knex");
const knex = getKnex();
const { Model } = require("objection");

Model.knex(knex);

class PttPenilaianPekerjaanTambahan extends Model {
  static get tableName() {
    return "ptt_penilaian_pekerjaan_tambahan";
  }

  $beforeInsert() {
    this.dibuat_pada = new Date();
  }

  $beforeUpdate() {
    this.diupdate_pada = new Date();
  }
}

module.exports = PttPenilaianPekerjaanTambahan;
