const { getKnex } = require("../db/knex");
const knex = getKnex();
const { Model } = require("objection");

Model.knex(knex);

class PttPenilaianTargetTahunan extends Model {
  static get tableName() {
    return "ptt_penilaian_target_tahunan";
  }

  static get relationMappings() {
    return {
      bulanan: {
        modelClass: require("./ptt-penilaian-kinerja-bulanan"),
        relation: Model.HasManyRelation,
        join: {
          from: "ptt_penilaian_target_tahunan.id",
          to: "ptt_penilaian_kinerja_bulanan.id_target_tahunan",
        },
      },
    };
  }

  static get modifiers() {
    return {
      simple(builder) {
        builder.select("id", "rincian_pekerjaan", "satuan_kuantitas");
      },
    };
  }

  $beforeInsert() {
    this.dibuat_pada = new Date();
  }

  $beforeUpdate() {
    this.diupdate_pada = new Date();
  }
}

module.exports = PttPenilaianTargetTahunan;
