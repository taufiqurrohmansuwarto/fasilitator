const { getKnex } = require("../db/knex");
const knex = getKnex();
const { Model } = require("objection");

const RefStatusSuamiIstri = require("./ref-status-suami-istri.model");
const RefStatusAnak = require("./ref-status-anak.model");
const suamiIstri = require("./rwyt-suami-istri.model");
const refPekerjaanAnak = require("./ref-pekerjaan-anak.model");

Model.knex(knex);

class RwytAnak extends Model {
  static get tableName() {
    return "rwyt_anak";
  }

  static get jsonSchema() {
    return {
      type: "object",
      required: [
        "id_ptt",
        "nama",
        "tempat_lahir",
        "tgl_lahir",
        "status_anak_id",
        "pekerjaan_anak_id",
      ],
      properties: {
        id_ptt: { type: "integer" },
      },
    };
  }

  static get modifiers() {
    return {};
  }

  static get relationMappings() {
    return {
      statusSuamiIstri: {
        modelClass: RefStatusSuamiIstri,
        relation: Model.BelongsToOneRelation,
        join: {
          from: "rwyt_anak.suami_istri_id",
          to: "ref_status_suami_istri.status_suami_istri_id",
        },
      },
      statusAnak: {
        modelClass: RefStatusAnak,
        relation: Model.BelongsToOneRelation,
        join: {
          from: "rwyt_anak.status_anak_id",
          to: "ref_status_anak.status_anak_id",
        },
      },
      suamiIstri: {
        modelClass: suamiIstri,
        relation: Model.BelongsToOneRelation,
        join: {
          from: "rwyt_anak.suami_istri_id",
          to: "rwyt_suami_istri.suami_istri_id",
        },
      },
      pekerjaanAnak: {
        modelClass: refPekerjaanAnak,
        relation: Model.BelongsToOneRelation,
        join: {
          from: "rwyt_anak.pekerjaan_anak_id",
          to: "ref_pekerjaan_anak.pekerjaan_id",
        },
      },
    };
  }

  static get idColumn() {
    return "anak_id";
  }
}

module.exports = RwytAnak;
