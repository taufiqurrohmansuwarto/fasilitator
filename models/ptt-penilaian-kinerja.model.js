const { getKnex } = require("../db/knex");
const knex = getKnex();
const { Model } = require("objection");

Model.knex(knex);

class PttPenilaianKinerja extends Model {
  static get tableName() {
    return "ptt_penilaian_kinerja";
  }

  $beforeInsert() {
    this.dibuat_pada = new Date();
    this.diupdate_pada = new Date();
  }

  $beforeUpdate() {
    this.diupdate_pada = new Date();
  }

  static get relationMappings() {
    return {
      listTargetTahunan: {
        modelClass: require("./ptt-penilaian-target-tahunan.model"),
        relation: Model.HasManyRelation,
        join: {
          from: "ptt_penilaian_kinerja.id",
          to: "ptt_penilaian_target_tahunan.id_kinerja",
        },
      },
      skpd: {
        modelClass: require("./skpd.model"),
        relation: Model.BelongsToOneRelation,
        join: {
          from: "ptt_penilaian_kinerja.id_skpd",
          to: "skpd.id",
        },
      },
      jabatan: {
        modelClass: require("./jabatan.model"),
        relation: Model.BelongsToOneRelation,
        join: {
          from: "ptt_penilaian_kinerja.id_jabatan",
          to: "jabatan.id_jabatan",
        },
      },
      perilakuKinerja: {
        modelClass: require("./ptt-penilaian-perilaku.model"),
        relation: Model.HasOneRelation,
        join: {
          from: "ptt_penilaian_kinerja.id",
          to: "ptt_penilaian_perilaku.id_kinerja",
        },
      },
      tambahan: {
        modelClass: require("./ptt-penilaian-pekerjaan-tambahan"),
        relation: Model.HasManyRelation,
        join: {
          from: "ptt_penilaian_kinerja.id",
          to: "ptt_penilaian_pekerjaan_tambahan.id_kinerja",
        },
      },
      bulanan: {
        modelClass: require("./ptt-penilaian-kinerja-bulanan"),
        relation: Model.HasManyRelation,
        join: {
          from: "ptt_penilaian_kinerja.id",
          to: "ptt_penilaian_kinerja_bulanan.id_kinerja",
        },
      },
    };
  }

  static get modifiers() {
    return {
      aktif(builder) {
        builder.where("ptt_penilaian_kinerja.aktif", true);
      },
    };
  }
}

module.exports = PttPenilaianKinerja;
