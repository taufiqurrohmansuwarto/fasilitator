const { getKnex } = require("../db/knex");
const knex = getKnex();
const { Model } = require("objection");

Model.knex(knex);

class RefPekerjaanAnak extends Model {
  static get tableName() {
    return "ref_pekerjaan_anak";
  }

  static get modifiers() {}

  static get idColumn() {
    return "pekerjaan_id";
  }
}

module.exports = RefPekerjaanAnak;
