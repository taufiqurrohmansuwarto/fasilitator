const { getKnex } = require("../db/knex");
const knex = getKnex();
const { Model } = require("objection");

Model.knex(knex);

class RwytSuamiIstri extends Model {
  static get tableName() {
    return "rwyt_suami_istri";
  }

  static get jsonSchema() {
    return {
      properties: {
        aktif: {
          type: "string",
          default: "N",
        },
      },
    };
  }

  static get modifiers() {
    return {
      simple(builder) {
        builder.select("suami_istri_id", "id_ptt", "nama_suami_istri");
      },
    };
  }

  static get relationMappings() {
    const RefPekerjaan = require("./ref-pekerjaan.model");
    const statusSuamiIstri = require("./ref-status-suami-istri.model");
    return {
      pekerjaan: {
        modelClass: RefPekerjaan,
        relation: Model.BelongsToOneRelation,
        join: {
          from: "rwyt_suami_istri.pekerjaan_id",
          to: "ref_pekerjaan.pekerjaan_id",
        },
      },
      status: {
        modelClass: statusSuamiIstri,
        relation: Model.BelongsToOneRelation,
        join: {
          from: "rwyt_suami_istri.status_suami_istri_id",
          to: "ref_status_suami_istri.status_suami_istri_id",
        },
      },
    };
  }

  static get idColumn() {
    return "suami_istri_id";
  }
}

module.exports = RwytSuamiIstri;
