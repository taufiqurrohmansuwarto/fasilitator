const { getKnex } = require("../db/knex");
const knex = getKnex();
const { Model } = require("objection");

Model.knex(knex);

class LogPtt extends Model {
  static get tableName() {
    return "log_ptt";
  }

  static get idColumn() {
    return "log_id";
  }
}

module.exports = LogPtt;
