const { getKnex } = require("../db/knex");
const knex = getKnex();
const { Model } = require("objection");

Model.knex(knex);

class PttPenilaian extends Model {
  static get tableName() {
    return "ptt_penilaian";
  }

  static get modifiers() {
    return {
      aktif(builder) {
        builder.where("ptt_penilaian.aktif", "Y");
      },
    };
  }

  static get idColumn() {
    return "id_ptt_penilaian";
  }
}

module.exports = PttPenilaian;
