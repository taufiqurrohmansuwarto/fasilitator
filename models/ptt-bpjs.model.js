const { getKnex } = require("../db/knex");
const knex = getKnex();
const { Model } = require("objection");

Model.knex(knex);

class PttBpjs extends Model {
  static get tableName() {
    return "ptt_bpjs";
  }

  static get idColumn() {
    return "id_ptt_bpjs";
  }
}

module.exports = PttBpjs;
