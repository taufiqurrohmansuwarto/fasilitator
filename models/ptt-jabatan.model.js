const { getKnex } = require("../db/knex");
const knex = getKnex();
const { Model } = require("objection");
const Jabatan = require("./jabatan.model");

Model.knex(knex);

class PttJabatan extends Model {
  static get tableName() {
    return "ptt_jabatan";
  }

  static get jsonSchema() {
    return {
      type: "object",
      properties: {
        aktif: {
          type: "string",
          default: "N",
        },
      },
    };
  }

  static get relationMappings() {
    return {
      jabatan: {
        modelClass: Jabatan,
        relation: Model.BelongsToOneRelation,
        join: {
          from: "ptt_jabatan.id_jabatan",
          to: "jabatan.id_jabatan",
        },
      },
    };
  }

  static get modifiers() {
    return {
      aktif(builder) {
        builder.where("ptt_jabatan.aktif", "Y").first();
      },
      simpleAktif(builder) {
        builder
          .where("ptt_jabatan.aktif", "Y")
          .select("id_ptt_jab", "id_ptt", "tgl_mulai")
          .first();
      },
    };
  }

  static get idColumn() {
    return "id_ptt_jab";
  }
}

module.exports = PttJabatan;
