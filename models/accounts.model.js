const { getKnex } = require("../db/knex");
const knex = getKnex();
const { Model } = require("objection");

Model.knex(knex);

class Accounts extends Model {
  static get tableName() {
    return "accounts";
  }

  static get idColumn() {
    return "id";
  }
}

module.exports = Accounts;
