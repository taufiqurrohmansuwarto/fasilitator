const { getKnex } = require("../db/knex");
const knex = getKnex();
const { Model } = require("objection");

Model.knex(knex);

class RefSatuanKinerja extends Model {
  static get tableName() {
    return "ref_satuan_kinerja";
  }
}

module.exports = RefSatuanKinerja;
