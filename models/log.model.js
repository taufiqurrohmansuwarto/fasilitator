const { getKnex } = require("../db/knex");
const knex = getKnex();
const { Model } = require("objection");

Model.knex(knex);

class Log extends Model {
  static get tableName() {
    return "log";
  }

  static get idColumn() {
    return "id";
  }
}

module.exports = Log;
