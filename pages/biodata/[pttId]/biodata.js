import { UploadOutlined } from "@ant-design/icons";
import { PageContainer } from "@ant-design/pro-layout";
import {
  Button,
  Card,
  Col,
  DatePicker,
  Form,
  Image,
  Input,
  message,
  Row,
  Select,
  TreeSelect,
  Upload,
} from "antd";
import axios from "axios";
import moment from "moment";
import React from "react";
import { useMutation, useQueries, useQuery, useQueryClient } from "react-query";
import { patchEmployee } from "../../../api/employees";
import { fetchBiodata } from "../../../api/personal-info";
import { refAgama, refSkpd, refStatusKawin } from "../../../api/ref";
import BiodataLayout from "../../../layouts/BiodataLayout";

const dateFormat = "DD-MM-YYYY";

const FormBiodata = ({
  pttId,
  initialValues,
  agama,
  kawin,
  skpd,
  mutation,
}) => {
  const [form] = Form.useForm();
  const { thn_lahir } = initialValues;

  const handleFinish = async (values) => {
    const {
      thn_lahir: thn_lahir_moment,
      jalan,
      rt,
      rw,
      kelurahan,
      kecamatan,
      kabupaten,
      provinsi,
      fileList,
      ...biodata
    } = values;

    const alamat = `${jalan}|${rt}|${rw}|${kelurahan}|${kecamatan}|${kabupaten}|${provinsi}`;
    const thn_lahir = moment(thn_lahir_moment).format("YYYY-MM-DD");

    let data = { alamat, thn_lahir, ...biodata, id_ptt: pttId };

    if (values.fileList?.filter((file) => !!file.uid).length !== 0) {
      data = {
        ...data,
        foto: values.fileList[0].originFileObj,
      };
    }

    const formData = new FormData();
    Object.keys(data).forEach((d) => {
      formData.append(d, data[d]);
    });

    await mutation.mutateAsync({ id_ptt: pttId, data: formData });
    message.success("Update Berhasil");
  };

  const layout = {
    labelCol: { span: 4 },
    wrapperCol: { span: 8 },
  };
  const tailLayout = {
    wrapperCol: { offset: 4, span: 8 },
  };

  const normFile = (info) => {
    let fileList = [...info.fileList];
    fileList = fileList.slice(-1);
    return fileList;
  };

  return (
    <Form
      name="biodata"
      {...layout}
      form={form}
      onFinish={handleFinish}
      initialValues={{
        ...initialValues,
        thn_lahir: moment(thn_lahir),
      }}
    >
      <Row>
        <Col offset={4}>
          <Image width={150} src={initialValues?.fileList[0]?.url} />
        </Col>
      </Row>
      <Form.Item
        name="fileList"
        label="Foto"
        valuePropName="fileList"
        getValueFromEvent={normFile}
      >
        <Upload
          multiple={false}
          accept={".jpg,.png"}
          showUploadList={{ showRemoveIcon: false }}
        >
          <Button icon={<UploadOutlined />}>Click to upload</Button>
        </Upload>
      </Form.Item>
      <Form.Item label="Tahun lahir" name="thn_lahir">
        <DatePicker format={dateFormat} />
      </Form.Item>
      <Form.Item label="Tempat Kerja" name="id_skpd">
        <TreeSelect showSearch treeNodeFilterProp="title" treeData={skpd} />
      </Form.Item>
      <Form.Item name="nama" label="Nama">
        <Input readOnly />
      </Form.Item>
      <Form.Item name="email" label="Email">
        <Input />
      </Form.Item>
      <Form.Item name="jalan" label="Alamat Rumah">
        <Input />
      </Form.Item>

      <Form.Item name="rt" label="RT">
        <Input />
      </Form.Item>
      <Form.Item name="rw" label="RW">
        <Input />
      </Form.Item>
      <Form.Item name="id_agama" label="Agama">
        <Select showSearch optionFilterProp="agama">
          {agama?.map((a) => (
            <Select.Option key={a.id_agama} agama={a.agama}>
              {a?.agama}
            </Select.Option>
          ))}
        </Select>
      </Form.Item>
      <Form.Item name="id_kawin" label="Status">
        <Select showSearch optionFilterProp="status">
          {kawin?.map((a) => (
            <Select.Option key={a.id_kawin} status={a.status_kawin}>
              {a?.status_kawin}
            </Select.Option>
          ))}
        </Select>
      </Form.Item>
      <Form.Item name="email" label="Email">
        <Input />
      </Form.Item>
      <Form.Item name="kelurahan" label="Kelurahan">
        <Input />
      </Form.Item>
      <Form.Item name="kecamatan" label="Kecamatan">
        <Input />
      </Form.Item>
      <Form.Item name="kabupaten" label="Kabupaten">
        <Input />
      </Form.Item>
      <Form.Item name="provinsi" label="Provinsi">
        <Input />
      </Form.Item>
      <Form.Item {...tailLayout}>
        <Button htmlType="submit" loading={mutation?.isLoading} type="primary">
          Submit
        </Button>
      </Form.Item>
    </Form>
  );
};

function Biodata({ session, pttId, biodata }) {
  const { data, isLoading } = useQuery(
    ["biodata", pttId],
    () => fetchBiodata(pttId),
    { initialData: biodata }
  );

  const queryClient = useQueryClient();
  const mutation = useMutation((data) => patchEmployee(data), {
    onSettled: async () => {
      queryClient.invalidateQueries(["biodata", pttId]);
    },
  });

  const [agama, kawin, skpd] = useQueries([
    { queryKey: "ref-agama", queryFn: refAgama, refetchOnWindowFocus: false },

    {
      queryKey: "ref-status-kawin",
      queryFn: refStatusKawin,
      refetchOnWindowFocus: false,
    },
    {
      queryKey: "ref-skpd",
      queryFn: refSkpd,
      refetchOnWindowFocus: false,
    },
  ]);

  return (
    <BiodataLayout session={session}>
      <PageContainer loading={isLoading} content={`${data?.nama}`}>
        <Card loading={isLoading}>
          <FormBiodata
            pttId={pttId}
            initialValues={data}
            agama={agama?.data}
            kawin={kawin?.data}
            skpd={skpd?.data}
            mutation={mutation}
          />
        </Card>
      </PageContainer>
    </BiodataLayout>
  );
}

export async function getServerSideProps(ctx) {
  const { pttId } = ctx?.params;

  const { data: biodata } = await axios.get(`/ptt-biodata/${pttId}/biodata`, {
    headers: ctx.req ? { cookie: ctx.req.headers.cookie } : undefined,
  });

  return {
    props: {
      pttId,
      biodata,
    },
  };
}

Biodata.auth = true;

export default Biodata;
