import ProTable from "@ant-design/pro-table";
import { Divider, message, Popconfirm, Space } from "antd";
import axios from "axios";
import React from "react";
import { useMutation, useQuery, useQueryClient } from "react-query";
import {
  fetchPttPenilaian,
  removePttPenilaian,
} from "../../../api/ptt-penilaian";
import CreatePenilaian from "../../../components/CreatePenilaian";
import UpdatePenilaian from "../../../components/UpdatePenilaian";
import BiodataLayout from "../../../layouts/BiodataLayout";

function Penilaian({ session, pttId, pttPenilaian }) {
  const { data } = useQuery(
    ["ptt-penilaian", pttId],
    () => fetchPttPenilaian(pttId),
    { initialData: pttPenilaian }
  );

  const client = useQueryClient();
  const removeMutation = useMutation((data) => removePttPenilaian(data), {
    onSuccess: () => {
      message.success("Berhasil dihapus");
    },
    onSettled: () => {
      client.invalidateQueries(["ptt-penilaian", pttId]);
    },
  });

  const handleRemoveMutation = async (id) => {
    await removeMutation.mutateAsync({ pttId, id });
  };

  const renderBerkas = (_, row) => {
    return <div>{row?.file}</div>;
  };

  const renderTahun = (_, row) => {
    return <div>{row?.tahun}</div>;
  };

  const renderNilai = (_, row) => {
    return <div>{row?.nilai}</div>;
  };

  const renderRekomendasi = (_, row) => {
    return <div>{row?.rekomendasi}</div>;
  };

  const renderAksi = (_, row) => {
    return (
      <Space split={<Divider type="vertical" />}>
        <UpdatePenilaian pttId={pttId} initialValues={row} />
        <Popconfirm
          title="Are you sure？"
          okText="Yes"
          cancelText="No"
          onConfirm={() => {
            handleRemoveMutation(row?.id_ptt_penilaian);
          }}
        >
          <a href="#">Delete</a>
        </Popconfirm>
      </Space>
    );
  };

  const columns = [
    {
      title: "Berkas",
      key: "berkas",
      dataIndex: "berkas",
      search: false,
      render: renderBerkas,
    },

    {
      title: "Tahun",
      key: "tahun",
      dataIndex: "tahun",
      search: false,
      render: renderTahun,
    },

    {
      title: "Nilai",
      key: "nilai",
      dataIndex: "nilai",
      search: false,
      render: renderNilai,
    },

    {
      title: "Rekomendasi",
      key: "rekomendasi",
      dataIndex: "rekomendasi",
      search: false,
      render: renderRekomendasi,
    },

    {
      title: "Aksi",
      dataIndex: "aksi",
      search: false,
      render: renderAksi,
    },
  ];

  return (
    <BiodataLayout session={session}>
      <ProTable
        toolBarRender={() => [<CreatePenilaian pttId={pttId} />]}
        rowKey="id_ptt_penilaian"
        search={false}
        pagination={false}
        dataSource={data}
        columns={columns}
      />
    </BiodataLayout>
  );
}

export async function getServerSideProps(ctx) {
  try {
    const { pttId } = ctx.params;
    const { data: pttPenilaian } = await axios.get(
      `/ptt-biodata/${pttId}/penilaian`,
      {
        headers: ctx.req ? { cookie: ctx.req.headers.cookie } : undefined,
      }
    );
    return {
      props: {
        pttId,
        pttPenilaian,
      },
    };
  } catch (error) {
    return {
      props: {},
    };
  }
}

Penilaian.auth = true;

export default Penilaian;
