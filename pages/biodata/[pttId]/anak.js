import ProTable from "@ant-design/pro-table";
import { Divider, message, Popconfirm, Space } from "antd";
import axios from "axios";
import moment from "moment";
import React from "react";
import { useMutation, useQueries, useQuery, useQueryClient } from "react-query";
import { fetchPttAnak, removePttAnak } from "../../../api/ptt-anak";
import { fetchPttSuamiIstri } from "../../../api/ptt-suami-istri";
import { refPekerjaanAnak, refStatusAnak } from "../../../api/ref";
import CreateAnak from "../../../components/CreateAnak";
import UpdateAnak from "../../../components/UpdateAnak";
import BiodataLayout from "../../../layouts/BiodataLayout";

function Anak({ session, pttId, anak }) {
  const { data } = useQuery(["rwyt-anak", pttId], () => fetchPttAnak(pttId), {
    initialData: anak,
  });

  const queryClient = useQueryClient();

  const removeMutation = useMutation((data) => removePttAnak(data), {
    onSettled: () => {
      queryClient.invalidateQueries(["rwyt-anak", pttId]);
    },
  });

  const [statusAnak, pekerjaanAnak, suamiIstri] = useQueries([
    {
      queryKey: "ref-status-anak",
      queryFn: refStatusAnak,
      refetchOnWindowFocus: false,
    },
    {
      queryKey: "ref-pekerjaani-anak",
      queryFn: refPekerjaanAnak,
      refetchOnWindowFocus: false,
    },
    {
      queryKey: "suami-istri-anak",
      queryFn: async () => {
        const data = await fetchPttSuamiIstri(pttId);
        const result = data?.map((d) => ({
          id: d?.suami_istri_id,
          key: d?.suami_istri_id,
          value: d?.suami_istri_id,
          title: d?.nama_suami_istri,
          label: d?.nama_suami_istri,
        }));
        return result;
      },
      refetchOnWindowFocus: false,
    },
  ]);

  const handleRemove = async (id) => {
    await removeMutation.mutateAsync({ pttId, id });
    message.success("Data anak berhasil dihapus");
  };

  const renderNamaSuamiIstri = (_, row) => {
    return <div>{row?.suamiIstri?.nama_suami_istri}</div>;
  };

  const renderNamaAnak = (_, row) => {
    return <div>{row?.nama}</div>;
  };

  const renderTempatLahir = (_, row) => {
    return <div>{row?.tempat_lahir}</div>;
  };

  const renderTanggalLahir = (_, row) => {
    return <div>{moment(row?.tgl_lahir).format("DD-MM-YYYY")}</div>;
  };

  const renderStatusAnak = (_, row) => {
    return <div>{row?.statusAnak?.status_anak}</div>;
  };

  const renderPekerjaan = (_, row) => {
    return <div>{row?.pekerjaanAnak?.pekerjaan}</div>;
  };

  const renderAksi = (_, row) => {
    return (
      <Space split={<Divider type="vertical" />}>
        <UpdateAnak
          pttId={pttId}
          initialValues={row}
          refStatusAnak={statusAnak?.data}
          refPekerjaanAnak={pekerjaanAnak?.data}
          refSuamiIstri={suamiIstri?.data}
        />
        <Popconfirm
          title="Are you sure？"
          okText="Yes"
          cancelText="No"
          onConfirm={() => handleRemove(row?.anak_id)}
        >
          <a href="#">Delete</a>
        </Popconfirm>
      </Space>
    );
  };

  const columns = [
    {
      title: "Nama Orang Tua Ayah / Ibu",
      key: "nama_orang_tua",
      dataIndex: "nama_orang_tua",
      search: false,
      render: renderNamaSuamiIstri,
    },
    {
      title: "Nama Anak",
      dataIndex: "nama_anak",
      search: false,
      render: renderNamaAnak,
    },
    {
      title: "Tempat Lahir",
      dataIndex: "tempat_lahir",
      key: "tempat_lahir",
      search: false,
      render: renderTempatLahir,
    },
    {
      title: "Tanggal Lahir",
      dataIndex: "tanggal_lahir",
      search: false,
      render: renderTanggalLahir,
    },
    {
      title: "Status Anak",
      dataIndex: "status_anak",
      search: false,
      render: renderStatusAnak,
    },
    {
      title: "Pekerjaan",
      dataIndex: "pekerjaan",
      key: "pekerjaan",
      search: false,
      render: renderPekerjaan,
    },
    {
      title: "Aksi",
      dataIndex: "aksi",
      search: false,
      render: renderAksi,
    },
  ];

  return (
    <BiodataLayout session={session}>
      <ProTable
        search={false}
        rowKey="anak_id"
        toolBarRender={() => [
          <CreateAnak
            refStatusAnak={statusAnak?.data}
            refPekerjaanAnak={pekerjaanAnak?.data}
            refSuamiIstri={suamiIstri?.data}
            pttId={pttId}
          />,
        ]}
        pagination={false}
        dataSource={data}
        columns={columns}
      />
    </BiodataLayout>
  );
}

export async function getServerSideProps(ctx) {
  const { pttId } = ctx?.params;
  const { data: anak } = await axios.get(`/ptt-biodata/${pttId}/anak`, {
    headers: ctx.req ? { cookie: ctx.req.headers.cookie } : undefined,
  });

  return {
    props: {
      pttId,
      anak,
    },
  };
}

Anak.auth = true;

export default Anak;
