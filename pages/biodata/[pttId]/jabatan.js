import ProTable from "@ant-design/pro-table";
import { Divider, message, Popconfirm, Space } from "antd";
import axios from "axios";
import React from "react";
import { useMutation, useQuery, useQueryClient } from "react-query";
import {
  fetchPttJabatan,
  removePttJabatan,
  updatePttJabatan,
} from "../../../api/ptt-jabatan";
import CreateJabatan from "../../../components/CreateJabatan";
import UpdateJabatan from "../../../components/UpdateJabatan";
import BiodataLayout from "../../../layouts/BiodataLayout";

function Jabatan({ session, pttId, pttJabatan }) {
  const { data } = useQuery(
    ["ptt-jabatan", pttId],
    () => fetchPttJabatan(pttId),
    { initialData: pttJabatan }
  );

  const client = useQueryClient();
  const removeMutation = useMutation((data) => removePttJabatan(data), {
    onSettled: () => {
      client.invalidateQueries(["ptt-jabatan", pttId]);
    },
    onSuccess: () => {
      message.success("Berhasil dihapus");
    },
  });

  const activateMutation = useMutation((data) => updatePttJabatan(data), {
    onSettled: () => {
      client.invalidateQueries(["ptt-jabatan", pttId]);
    },
    onSuccess: () => {
      message.success("Berhasil diaktifkan");
    },
  });

  const handleRemove = async (id) => {
    await removeMutation.mutateAsync({ pttId, id });
  };

  const handleActive = async (id) => {
    const formData = new FormData();
    formData.append("aktif", "Y");
    await activateMutation.mutateAsync({ pttId, id, data: formData });
  };

  const renderBerkas = (_, row) => {
    return <div>{row?.file}</div>;
  };

  const renderNamaJabatan = (_, row) => {
    return <div>{row?.jabatan?.name}</div>;
  };

  const renderKeterangan = (_, row) => {
    return <div>{row?.ket}</div>;
  };

  const renderAktif = (_, row) => {
    return <div>{row?.aktif}</div>;
  };

  const renderAksi = (_, row) => {
    return (
      <Space split={<Divider type="vertical" />}>
        <UpdateJabatan initialValues={row} pttId={pttId} />
        <a onClick={() => handleActive(row?.id_ptt_jab)}>Activate</a>
        <Popconfirm
          title="Are you sure？"
          okText="Yes"
          cancelText="No"
          onConfirm={() => {
            handleRemove(row?.id_ptt_jab);
          }}
        >
          <a href="#">Delete</a>
        </Popconfirm>
      </Space>
    );
  };

  const columns = [
    {
      title: "Berkas",
      key: "berkas",
      dataIndex: "berkas",
      search: false,
      render: renderBerkas,
    },

    {
      title: "Nama Jabatan",
      key: "nama_jabatan",
      dataIndex: "nama_jabatan",
      search: false,
      render: renderNamaJabatan,
    },

    {
      title: "Tanggal Mulai Kontrak",
      key: "tgl_mulai",
      dataIndex: "tgl_mulai",
      search: false,
      valueType: "date",
    },

    {
      title: "Tanggal Akhir Kontrak",
      key: "tgl_akhir",
      dataIndex: "tgl_akhir",
      search: false,
      valueType: "date",
    },

    {
      title: "Gaji",
      key: "gaji",
      dataIndex: "gaji",
      search: false,
    },

    {
      title: "Keterangan",
      key: "keterangan",
      dataIndex: "keterangan",
      search: false,
      render: renderKeterangan,
    },

    {
      title: "Aktif (Sesuai SK)",
      key: "aktif",
      dataIndex: "aktif",
      search: false,
      render: renderAktif,
    },

    {
      title: "Aksi",
      dataIndex: "aksi",
      search: false,
      render: renderAksi,
    },
  ];

  return (
    <BiodataLayout session={session}>
      <ProTable
        rowKey="id_ptt_jab"
        pagination={false}
        dataSource={data}
        search={false}
        columns={columns}
        toolBarRender={() => [<CreateJabatan pttId={pttId} />]}
      />
    </BiodataLayout>
  );
}

export async function getServerSideProps(ctx) {
  try {
    const { pttId } = ctx.params;
    const { data: pttJabatan } = await axios.get(
      `/ptt-biodata/${pttId}/jabatan`,
      {
        headers: ctx.req ? { cookie: ctx.req.headers.cookie } : undefined,
      }
    );
    return {
      props: {
        pttId,
        pttJabatan,
      },
    };
  } catch (error) {}
}

Jabatan.auth = true;

export default Jabatan;
