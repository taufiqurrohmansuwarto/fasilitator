import ProTable from "@ant-design/pro-table";
import { Divider, Image, message, Popconfirm, Space } from "antd";
import axios from "axios";
import React from "react";
import { useMutation, useQuery, useQueryClient } from "react-query";
import { fetchPttBpjs, removePttBpjs } from "../../../api/ptt-bpjs";
import CreateBpjs from "../../../components/CreateBpjs";
import UpdateBpjs from "../../../components/UpdateBpjs";
import BiodataLayout from "../../../layouts/BiodataLayout";

function Bpjs({ session, pttId, pttBpjs }) {
  const { data } = useQuery(
    ["ptt-bpjs", pttId],
    () => {
      return fetchPttBpjs(pttId);
    },
    { initialData: pttBpjs }
  );

  const client = useQueryClient();
  const removeMutation = useMutation((data) => removePttBpjs(data), {
    onSettled: () => {
      client.invalidateQueries(["ptt-bpjs", pttId]);
    },
    onSuccess: () => {
      message.success("Data berhasil dihapus");
    },
  });

  const handleRemove = async (id) => {
    const data = { pttId, id };
    await removeMutation.mutateAsync(data);
  };

  const renderAksi = (_, row) => {
    return (
      <Space split={<Divider type="vertical" />}>
        <UpdateBpjs initialValues={row} pttId={pttId} />
        <Popconfirm
          title="Are you sure？"
          okText="Yes"
          cancelText="No"
          onConfirm={() => handleRemove(row?.id_ptt_bpjs)}
        >
          <a href="#">Delete</a>
        </Popconfirm>
      </Space>
    );
  };

  const renderFile = (_, row) => {
    const url = row?.fileList?.[0].url;
    return <Image src={url} width={50} />;
  };

  const columns = [
    {
      title: "Berkas",
      dataIndex: "file",
      render: renderFile,
      search: false,
    },

    {
      title: "Nomer BPJS",
      key: "no_bpjs",
      dataIndex: "no_bpjs",
      search: false,
    },

    {
      title: "Kelas BPJS",
      key: "kelas",
      dataIndex: "kelas",
      search: false,
    },

    {
      title: "Aksi",
      dataIndex: "aksi",
      search: false,
      render: renderAksi,
    },
  ];

  return (
    <BiodataLayout session={session}>
      <ProTable
        search={false}
        toolBarRender={() => [<CreateBpjs pttId={pttId} />]}
        pagination={false}
        dataSource={data}
        columns={columns}
        rowKey="id_ptt_bpjs"
      />
    </BiodataLayout>
  );
}

export async function getServerSideProps(ctx) {
  try {
    const { pttId } = ctx.params;
    const { data: pttBpjs } = await axios.get(`/ptt-biodata/${pttId}/bpjs`, {
      headers: ctx.req ? { cookie: ctx.req.headers.cookie } : undefined,
    });
    return {
      props: {
        pttId,
        pttBpjs,
      },
    };
  } catch (error) {}
}

Bpjs.auth = true;

export default Bpjs;
