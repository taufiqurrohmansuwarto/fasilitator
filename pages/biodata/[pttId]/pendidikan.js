import ProTable from "@ant-design/pro-table";
import { Divider, message, Popconfirm, Space } from "antd";
import axios from "axios";
import { useMutation, useQuery, useQueryClient } from "react-query";
import {
  fetchPttPendidikan,
  removePttPendidikan,
  updatePttPendiidkan,
} from "../../../api/ptt-pendidikan";
import { refJenjang } from "../../../api/ref";
import CreatePendidikan from "../../../components/CreatePendidikan";
import UpdatePendidikan from "../../../components/UpdatePendidikan";
import BiodataLayout from "../../../layouts/BiodataLayout";

function Pendidikan({ session, pttId, pttPendidikan }) {
  const { data, isLoading } = useQuery(
    ["ptt-pendidikan", pttId],
    () => fetchPttPendidikan(pttId),
    { initialData: pttPendidikan }
  );

  const client = useQueryClient();
  const removeMutation = useMutation((data) => removePttPendidikan(data), {
    onSettled: () => {
      client.invalidateQueries(["ptt-pendidikan", pttId]);
    },
    onSuccess: () => {
      message.success("Berhasil dihapus");
    },
  });

  const activeMutation = useMutation((data) => updatePttPendiidkan(data), {
    onSettled: () => {
      client.invalidateQueries(["ptt-pendidikan", pttId]);
    },
    onSuccess: () => {
      message.success("Berhasil diaktifkan");
    },
  });

  const handleRemove = async (id) => {
    await removeMutation.mutateAsync({ pttId, id });
  };

  const handleActive = async (id) => {
    const formData = new FormData();
    formData.append("aktif", "Y");
    await activeMutation.mutateAsync({ pttId, data: formData, id });
  };

  const renderJenjang = (_, row) => {
    return <div>{row?.jenjang?.nama_jenjang}</div>;
  };

  const renderNamaSekolah = (_, row) => {
    const jenjangId = row?.id_jenjang;

    if (!jenjangId) {
      return <div>-</div>;
    }

    if (jenjangId > 3) {
      return <div>{row?.nama_pt}</div>;
    }

    if (jenjangId <= 3) {
      return <div>{row?.nama_sekolah_sma}</div>;
    }
  };

  const renderTahunLulus = (_, row) => {
    const jenjangId = row?.id_jenjang;

    if (!jenjangId) {
      return <div>-</div>;
    }

    if (jenjangId > 3) {
      return <div>{row?.thn_lulus_pt}</div>;
    }

    if (jenjangId <= 3) {
      return <div>{row?.thn_lulus_sma}</div>;
    }
  };

  const renderIjazah = (_, row) => {
    const jenjangId = row?.id_jenjang;

    if (!jenjangId) {
      return <div>-</div>;
    }

    if (jenjangId > 3) {
      return <div>{row?.fileIjazahPT?.[0]?.url}</div>;
    }

    if (jenjangId <= 3) {
      return <div>{row?.fileIjazahSma?.[0]?.url}</div>;
    }
  };

  const renderFileNilai = (_, row) => {
    const jenjangId = row?.id_jenjang;

    if (!jenjangId) {
      return <div>-</div>;
    }

    if (jenjangId > 3) {
      return <div>{row?.fileNilaiPT?.[0]?.url}</div>;
    }

    if (jenjangId <= 3) {
      return <div>{row?.fileNilaiSma?.[0]?.url}</div>;
    }
  };

  const renderAksi = (_, row) => {
    return (
      <Space split={<Divider type="vertical" />}>
        <UpdatePendidikan
          pttId={pttId}
          initialValues={row}
          dataJenjang={dataJenjang}
        />
        <a onClick={() => handleActive(row?.id_ptt_pendidikan)}>Activate</a>
        <Popconfirm
          title="Are you sure？"
          okText="Yes"
          cancelText="No"
          onConfirm={() => handleRemove(row?.id_ptt_pendidikan)}
        >
          <a href="#">Delete</a>
        </Popconfirm>
      </Space>
    );
  };

  const columns = [
    {
      title: "Berkas Ijazah",
      dataIndex: "berkas_ijazah",
      search: false,
      render: renderIjazah,
    },
    {
      title: "Berkas Nilai",
      dataIndex: "berkas_nilai",
      search: false,
      render: renderFileNilai,
    },
    {
      title: "Jenjang",
      dataIndex: "jenjang",
      search: false,
      render: renderJenjang,
    },
    {
      title: "Nama Sekolah",
      dataIndex: "nama_sekolah",
      search: false,
      render: renderNamaSekolah,
    },
    {
      title: "Tahun Lulus",
      dataIndex: "tahun_lulus",
      search: false,
      render: renderTahunLulus,
    },
    {
      title: "Aktif Sesuai SK",
      dataIndex: "aktif",
      key: "aktif",
      search: false,
    },
    {
      title: "Aksi",
      dataIndex: "aksi",
      search: false,
      render: renderAksi,
    },
  ];

  const { data: dataJenjang } = useQuery("ref-jenjang", refJenjang, {
    refetchOnWindowFocus: false,
  });

  return (
    <BiodataLayout session={session}>
      <ProTable
        loading={isLoading}
        dataSource={data}
        rowKey="id_ptt_pendidikan"
        search={false}
        columns={columns}
        pagination={false}
        toolBarRender={() => [
          <CreatePendidikan dataJenjang={dataJenjang} pttId={pttId} />,
        ]}
      />
    </BiodataLayout>
  );
}

export async function getServerSideProps(ctx) {
  try {
    const { pttId } = ctx.params;
    const { data: pttPendidikan } = await axios.get(
      `/ptt-biodata/${pttId}/pendidikan`,
      {
        headers: ctx.req ? { cookie: ctx.req.headers.cookie } : undefined,
      }
    );
    return {
      props: {
        pttId,
        pttPendidikan,
      },
    };
  } catch (error) {}
}

Pendidikan.auth = true;

export default Pendidikan;
