import ProTable from "@ant-design/pro-table";
import { Divider, message, Popconfirm, Space } from "antd";
import axios from "axios";
import moment from "moment";
import React from "react";
import { useMutation, useQueries, useQuery, useQueryClient } from "react-query";
import {
  fetchPttSuamiIstri,
  removePttSuamiIstri,
  updatePttSuamiIstri,
} from "../../../api/ptt-suami-istri";
import { refPekerjaan, refStatusSuamiIstri } from "../../../api/ref";
import CreateSuamiIstri from "../../../components/CreateSuamiIstri";
import UpdateSuamiIstri from "../../../components/UpdateSuamiIstri";
import BiodataLayout from "../../../layouts/BiodataLayout";

function SuamiIstri({ session, pttId, suamiIstri }) {
  const { data } = useQuery(
    ["rwyt-suami-istri", pttId],
    () => fetchPttSuamiIstri(pttId),
    {
      initialData: suamiIstri,
    }
  );

  const queryClient = useQueryClient();

  const updateMutation = useMutation((data) => updatePttSuamiIstri(data), {
    onSettled: () => {
      queryClient.invalidateQueries(["rwyt-suami-istri", pttId]);
    },
  });

  const removeMutation = useMutation((data) => removePttSuamiIstri(data), {
    onSettled: () => {
      queryClient.invalidateQueries(["rwyt-suami-istri", pttId]);
    },
  });

  const [refStatusPernikahan, refStatusPekerjaan] = useQueries([
    {
      queryKey: "ref-status-suami-istri",
      queryFn: refStatusSuamiIstri,
      refetchOnWindowFocus: false,
    },
    {
      queryKey: "ref-pekerjaan",
      queryFn: refPekerjaan,
      refetchOnWindowFocus: false,
    },
  ]);

  const handleRemove = async (id) => {
    await removeMutation.mutateAsync({ pttId, id });
    message.success("Data suami/istri berhasil dihapus");
  };

  const handleUpdate = async (id, data) => {
    await updateMutation.mutate({ pttId, id, data });
    message.success("Data berhasil diaktifkan");
  };

  const renderStatusIstri = (_, row) => {
    return <div>{row?.status?.status_suami_istri}</div>;
  };

  const renderTanggalLahir = (_, row) => {
    return <div>{moment(row?.tgl_lahir).format("DD-MM-YYYY")}</div>;
  };

  const renderStatusPekerjaan = (_, row) => {
    return <div>{row?.pekerjaan?.pekerjaan}</div>;
  };

  const renderAksi = (_, row) => {
    return (
      <Space split={<Divider type="vertical" />}>
        <UpdateSuamiIstri
          initialValues={row}
          pttId={pttId}
          refStatusPernikahan={refStatusPernikahan?.data}
          refStatusPekerjaan={refStatusPekerjaan?.data}
        />
        <a
          href="#"
          onClick={() => handleUpdate(row?.suami_istri_id, { aktif: "Y" })}
        >
          Activate
        </a>
        <Popconfirm
          title="Are you sure？"
          okText="Yes"
          cancelText="No"
          onConfirm={() => handleRemove(row?.suami_istri_id)}
        >
          <a href="#">Delete</a>
        </Popconfirm>
      </Space>
    );
  };

  const columns = [
    {
      title: "Nama Lengkap Suami Istri",
      key: "nama_suami_istri",
      dataIndex: "nama_suami_istri",
      search: false,
    },
    {
      title: "Status",
      dataIndex: "status_suami_istri",
      search: false,
      render: renderStatusIstri,
    },
    {
      title: "Tempat Lahir",
      dataIndex: "tempat_lahir",
      key: "tempat_lahir",
      search: false,
    },
    {
      title: "Tanggal Lahir",
      dataIndex: "tgl_lahir",
      search: false,
      render: renderTanggalLahir,
    },
    {
      title: "Status Pekerjaan",
      dataIndex: "status_pekerjaan",
      search: false,
      render: renderStatusPekerjaan,
    },
    {
      title: "Instansi / Tempat Bekerja",
      dataIndex: "instansi",
      key: "instansi",
      search: false,
    },
    {
      title: "Aktif",
      dataIndex: "aktif",
      key: "aktif",
      search: false,
    },
    {
      title: "Aksi",
      dataIndex: "aksi",
      search: false,
      render: renderAksi,
    },
  ];

  return (
    <BiodataLayout session={session}>
      <ProTable
        title={() => <div>Riwayat Suami Istri</div>}
        rowKey="suami_istri_id"
        toolBarRender={() => [
          <CreateSuamiIstri
            refStatusPekerjaan={refStatusPekerjaan?.data}
            refStatusPernikahan={refStatusPernikahan?.data}
            pttId={pttId}
          />,
        ]}
        search={false}
        pagination={false}
        dataSource={data}
        columns={columns}
      />
    </BiodataLayout>
  );
}

export async function getServerSideProps(ctx) {
  const { pttId } = ctx.params;
  const { data: suamiIstri } = await axios.get(
    `/ptt-biodata/${pttId}/suami-istri`,
    {
      headers: ctx.req ? { cookie: ctx.req.headers.cookie } : undefined,
    }
  );
  return {
    props: {
      pttId,
      suamiIstri,
    },
  };
}

SuamiIstri.auth = true;

export default SuamiIstri;
