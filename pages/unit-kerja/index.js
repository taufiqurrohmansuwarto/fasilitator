import { PageContainer } from "@ant-design/pro-layout";
import {
  Button,
  Card,
  Divider,
  Input,
  message,
  Skeleton,
  Space,
  TreeSelect,
} from "antd";
import React, { useEffect, useState } from "react";
import { useMutation, useQuery, useQueryClient } from "react-query";
import { detailSkpd, refSkpd, removeSkpd, updateSkpd } from "../../api/ref";
import CreateSkpd from "../../components/CreateSkpd";
import Layout from "../../layouts/Layout";

const showDetail = (detailSkpd) => {
  if (detailSkpd?.length === 0) {
    return "";
  } else {
    return detailSkpd?.map((e) => e?.name).join(" - ");
  }
};

const Form = ({ skpd, fetching }) => {
  if (!skpd) {
    return null;
  }

  const client = useQueryClient();
  const updateMutation = useMutation((data) => updateSkpd(data), {
    onSettled: () => {
      client.invalidateQueries("ref-skpd");
    },
    onSuccess: () => {
      message.success("Update berhasil dihapus");
    },
  });

  const removeMutation = useMutation((data) => removeSkpd(data), {
    onSettled: () => {
      client.invalidateQueries("ref-skpd");
    },
    onSuccess: () => {
      message.success("Berhasil Di hapus");
    },
  });

  const [name, setName] = useState(
    skpd?.detail[skpd?.detail?.length - 1]?.name
  );

  const handleUpdate = async () => {
    const id = skpd?.data_id_skpd;
    await updateMutation.mutateAsync({ id, name });
  };

  const handleRemove = async () => {
    const id = skpd?.data_id_skpd;
    await removeMutation.mutateAsync(id);
  };

  return (
    <div>
      {/* <div>{JSON.stringify(skpd)}</div> */}
      <div>{showDetail(skpd?.detail)}</div>
      <div>{skpd?.totalEmployee}</div>
      <Input
        value={name}
        onChange={(e) => {
          setName(e?.target?.value?.toUpperCase());
        }}
      />
      <Divider />
      <Skeleton loading={fetching}>
        <Space>
          <Button onClick={handleUpdate}>Edit</Button>
          <CreateSkpd skpd={showDetail(skpd?.detail)} id={skpd?.data_id_skpd} />
          <Button onClick={handleRemove}>Hapus</Button>
        </Space>
      </Skeleton>
    </div>
  );
};

function UnitKerja({ session }) {
  const { data } = useQuery("ref-skpd", refSkpd);

  const [id, setId] = useState();
  const {
    data: skpd,
    refetch,
    isFetching,
  } = useQuery(["detail-skpd", id], () => detailSkpd(id), {
    enabled: !!id,
    refetchOnWindowFocus: false,
  });

  useEffect(() => {
    if (id) {
      refetch();
    }
  }, [id]);

  return (
    <Layout session={session}>
      <PageContainer title="Unit Kerja">
        <Card>
          <TreeSelect
            style={{ width: "80%" }}
            treeData={data}
            showSearch
            treeNodeFilterProp="title"
            treeDefaultExpandedKeys={[session?.user?.skpd?.id]}
            onSelect={(id) => {
              if (id) {
                setId(id);
              }
            }}
          />
          <Form skpd={skpd} fetching={isFetching} />
        </Card>
      </PageContainer>
    </Layout>
  );
}

UnitKerja.auth = true;
export default UnitKerja;
