import { PageContainer } from "@/components/ProComponents/PageContainer";
import ProTable from "@ant-design/pro-table";
import { Divider, Select, Slider, Space, TreeSelect } from "antd";
import axios from "axios";
import omit from "lodash/omit";
import moment from "moment";
import { useRouter } from "next/router";
import { useEffect, useRef, useState } from "react";
import { useQueries, useQuery } from "react-query";
import { fetchEmployees } from "../../api/employees";
import {
  refAgama,
  refJabatan,
  refJenjang,
  refSkpd,
  refStatusKawin,
} from "../../api/ref";
import AktivasiPttpk from "../../components/AktivasiPttpk";
import Layout from "../../layouts/Layout";
import { skpdSerialize } from "../../lib/serialize/components.serialize";

const Pegawai = ({ session, listPtt }) => {
  const [query, setQuery] = useState({ offset: 0 });
  const { data, refetch, isFetching } = useQuery(
    "ptt-biodata",
    () => {
      return fetchEmployees(query);
    },
    { enabled: false, initialData: listPtt }
  );

  const [agama, jenjang, jabatan, statusKawin, skpd] = useQueries([
    { queryKey: "ref-agama", queryFn: refAgama, refetchOnWindowFocus: false },
    {
      queryKey: "ref-jenjang",
      queryFn: refJenjang,
      refetchOnWindowFocus: false,
    },
    {
      queryKey: "ref-jabatan",
      queryFn: refJabatan,
      refetchOnWindowFocus: false,
    },
    {
      queryKey: "ref-status-kawin",
      queryFn: refStatusKawin,
      refetchOnWindowFocus: false,
    },
    {
      queryKey: "ref-skpd",
      queryFn: refSkpd,
      refetchOnWindowFocus: false,
    },
  ]);

  const router = useRouter();

  const RenderPendidikanTerakhir = ({
    jenjang,
    jurusan_sekolah,
    nama_sekolah,
  }) => {
    return (
      <>
        <div>{jenjang?.nama_jenjang}</div>
        <div>{jurusan_sekolah}</div>
        <div>{nama_sekolah}</div>
      </>
    );
  };

  const RenderJabatanTerakhir = ({ jabatan, tgl_mulai }) => {
    return (
      <>
        <div>{jabatan}</div>
        <div>{moment(tgl_mulai).format("DD-MM-YYYY")}</div>
      </>
    );
  };

  const RenderIdentitas = ({
    nama,
    niptt,
    tempat_lahir,
    thn_lahir,
    usia,
    jk,
  }) => {
    return (
      <>
        <div>{nama}</div>
        <div>{niptt}</div>
        <div>
          {tempat_lahir}, {moment(thn_lahir).format("DD-MM-YYYY")}
        </div>
        <div>{usia} Tahun</div>
        <div>{jk === "L" ? "Laki-Laki" : "Perempuan"}</div>
      </>
    );
  };

  const columns = [
    // searching
    { title: "Nama", key: "nama", hideInTable: true },
    {
      title: "SKPD",
      key: "id_skpd",
      hideInTable: true,
      renderFormItem: (
        _,
        { type, defaultRender, formItemProps, fieldProps, ...rest }
      ) => {
        return (
          <TreeSelect
            allowClear
            showSearch
            multiple
            treeNodeFilterProp="title"
            {...fieldProps}
            treeData={skpd?.data}
          />
        );
      },
    },
    {
      title: "Jabatan",
      key: "id_jabatan",
      hideInTable: true,
      renderFormItem: (
        _,
        { type, defaultRender, formItemProps, fieldProps, ...rest },
        form
      ) => {
        return (
          <TreeSelect
            allowClear
            showSearch
            multiple
            treeNodeFilterProp="title"
            {...fieldProps}
            treeData={jabatan?.data}
          />
        );
      },
    },
    {
      title: "Agama",
      key: "id_agama",
      hideInTable: true,
      renderFormItem: (
        _,
        { type, defaultRender, formItemProps, fieldProps, ...rest }
      ) => {
        return (
          <Select mode="multiple" optionFilterProp="title" {...fieldProps}>
            {agama?.data?.map((d) => (
              <Select.Option
                key={d?.id_agama}
                value={d?.id_agama}
                title={d?.agama}
              >
                {d?.agama}
              </Select.Option>
            ))}
          </Select>
        );
      },
    },
    {
      title: "Kawin",
      key: "id_kawin",
      hideInTable: true,
      renderFormItem: (
        _,
        { type, defaultRender, formItemProps, fieldProps, ...rest }
      ) => {
        return (
          <Select mode="multiple" optionFilterProp="title" {...fieldProps}>
            {statusKawin?.data?.map((d) => (
              <Select.Option
                key={d?.id_kawin}
                value={d?.id_kawin}
                title={d?.status_kawin}
              >
                {d?.status_kawin}
              </Select.Option>
            ))}
          </Select>
        );
      },
    },
    {
      title: "Jenjang",
      key: "id_jenjang",
      hideInTable: true,
      renderFormItem: (
        _,
        { type, defaultRender, formItemProps, fieldProps, ...rest }
      ) => {
        return (
          <Select mode="multiple" optionFilterProp="title" {...fieldProps}>
            {jenjang?.data?.map((d) => (
              <Select.Option
                key={d?.id_jenjang}
                value={d?.id_jenjang}
                title={d?.nama_jenjang}
              >
                {d?.nama_jenjang}
              </Select.Option>
            ))}
          </Select>
        );
      },
    },

    {
      title: "Usia",
      key: "usia",
      hideInTable: true,
      renderFormItem: (
        _,
        { type, defaultRender, formItemProps, fieldProps, ...rest }
      ) => {
        return <Slider range {...fieldProps} />;
      },
    },

    // end of searching
    { title: "Foto", key: "foto", dataIndex: "foto", search: false },
    {
      title: "Status Kawin",
      key: "status_kawin",
      search: false,
      hideInTable: true,
    },
    {
      title: "Identitas",
      search: false,
      key: "identitas",
      render: (_, row) => {
        const { nama, niptt, tempat_lahir, thn_lahir, jk, usia } = row;
        return (
          <RenderIdentitas
            nama={nama}
            tempat_lahir={tempat_lahir}
            thn_lahir={thn_lahir}
            niptt={niptt}
            jk={jk}
            usia={usia}
          />
        );
      },
    },
    {
      title: "Pendidikan Terakhir",
      search: false,
      key: "pendidikan_terakhir",
      render: (_, row) => {
        const currentJenjang = row?.pendidikan?.[0];
        return (
          <RenderPendidikanTerakhir
            jenjang={currentJenjang?.jenjang}
            jurusan_sekolah={currentJenjang?.jurusan_sekolah}
            nama_sekolah={currentJenjang?.nama_sekolah}
          />
        );
      },
    },
    {
      title: "Jabatan Terakhir",
      search: false,
      key: "jabatan_terakhir",
      render: (_, row) => {
        const data = row?.jabatan?.[0];
        return (
          <RenderJabatanTerakhir
            jabatan={data?.jabatan?.name}
            tgl_mulai={data?.tgl_mulai}
          />
        );
      },
    },
    {
      title: "Perangkat Daerah",
      search: false,
      key: "perangkat_daerah",
      width: 300,
      render: (_, row) => {
        return <div>{skpdSerialize(row?.detailSkpd?.[0]?.detail)}</div>;
      },
    },
    {
      title: "Aksi",
      key: "aksi",
      search: false,
      render: (_, row) => {
        return (
          <Space split={<Divider type="vertical" />}>
            <AktivasiPttpk initialValues={row} />
          </Space>
        );
      },
    },
  ];

  useEffect(() => {
    refetch();
  }, [query]);

  const ref = useRef();

  return (
    <Layout session={session}>
      <PageContainer title="List Pegawai">
        <ProTable
          loading={isFetching}
          actionRef={ref}
          columns={columns}
          dataSource={data?.data}
          onReset={() => {
            setQuery({});
          }}
          rowKey="id_ptt"
          request={async (params) => {
            const offset = params?.current - 1;
            const takeQuery = omit(params, ["current", "pageSize"]);
            const filterQuery = { offset, ...takeQuery };
            setQuery({ ...query, ...filterQuery });
          }}
          pagination={{
            total: data?.total,
            showSizeChanger: false,
            position: ["topRight", "bottomRight"],
          }}
        />
      </PageContainer>
    </Layout>
  );
};

export async function getServerSideProps(ctx) {
  const { data: listPtt } = await axios.get(`/ptt-biodata`, {
    headers: ctx.req ? { cookie: ctx.req.headers.cookie } : undefined,
  });

  return {
    props: {
      listPtt,
    },
  };
}

Pegawai.auth = true;

export default Pegawai;
