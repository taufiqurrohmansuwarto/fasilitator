import ProForm, { ProFormText } from "@ant-design/pro-form";
import { PageContainer, WaterMark } from "@ant-design/pro-layout";
import { Card, message } from "antd";
import { useRouter } from "next/router";
import React from "react";
import { createEmployee } from "../../api/employees";
import ProFormTree from "../../components/form/ProFormTree";
import Layout from "../../layouts/Layout";
import axios from "../../lib/axios";

function PegawaiBaru({ session }) {
  const router = useRouter();
  return (
    <Layout session={session}>
      <PageContainer
        title="Pegawai Baru"
        waterMarkProps={<WaterMark content="" />}
      >
        <Card>
          <ProForm
            onFinish={async (values) => {
              await createEmployee(values);
              message.success("Berhasil");
              router.push("/");
            }}
          >
            <ProForm.Group>
              <ProFormText
                name="niptt"
                label="NIPTT"
                tooltip="Nomor Induk Pegawai Tidak Tetap"
                width="md"
                placeholder="NIPTT"
                rules={[
                  {
                    required: true,
                    message: "NIPTT tidak boleh kosong",
                  },
                  {
                    min: 10,
                    message: "NIPTT minimal 10",
                  },
                ]}
              />
            </ProForm.Group>
            <ProForm.Group>
              <ProFormText
                width="md"
                name="nama"
                label="Nama Lengkap"
                tooltip="Nama Lengkap"
                placeholder="Nama Lengkap"
                rules={[
                  {
                    required: true,
                    message: "Nama Lengkap tidak boleh kosong",
                  },
                ]}
              />
            </ProForm.Group>
            <ProForm.Group>
              <ProFormText
                width="md"
                name="nik"
                label="NIK"
                tooltip="Nomor Induk Kependudukan"
                placeholder="Nomor Induk Kependudukan"
                rules={[{ required: true, message: "NIK tidak boleh kosong" }]}
              />
            </ProForm.Group>
            <ProForm.Group>
              <ProFormTree
                width="xl"
                name="id_skpd"
                label="Satuan Kerja"
                tooltip="Satuan Kerja"
                placeholder="Satuan Kerja"
                rules={[
                  {
                    required: true,
                    message: "Satuan Kerja tidak boleh kosong",
                  },
                ]}
              />
            </ProForm.Group>
          </ProForm>
        </Card>
      </PageContainer>
    </Layout>
  );
}

export async function getServerSideProps({ req, res }) {
  const headers = req ? { cookie: req?.headers?.cookie } : undefined;
  const skpd = await axios.get("/skpd", { headers });
  return {
    props: {
      skpd: skpd?.data,
    },
  };
}

PegawaiBaru.auth = true;

export default PegawaiBaru;
