import { Button } from "antd";
import axios from "axios";
import { signOut } from "next-auth/client";
import Layout from "../layouts/Layout";

axios.defaults.withCredentials = true;

const Home = ({ session }) => {
  return (
    <Layout session={session}>
      <Button type="primary" onClick={signOut}>
        Sign out
      </Button>
      <div>{JSON.stringify(session)}</div>
    </Layout>
  );
};

Home.auth = true;

export default Home;
