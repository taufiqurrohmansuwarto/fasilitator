import { PageContainer } from "@ant-design/pro-layout";
import { TreeSelect } from "antd";
import React, { useState } from "react";
import { useQuery } from "react-query";
import { refJabatan } from "../../api/ref";
import Layout from "../../layouts/Layout";

function JenisJabatan({ session }) {
  const { data: dataJabatan, isLoading } = useQuery("ref-jabatan", refJabatan);
  const [id, setId] = useState();
  const { data: detailJabatan } = useQuery(["detail-jabatan", id]);

  return (
    <Layout session={session}>
      <PageContainer title="Jenis Jabatan">
        <TreeSelect
          treeDefaultExpandedKeys={["1"]}
          placeholder="Ketikkan nama jabatan"
          treeData={dataJabatan}
          style={{ width: "50%" }}
          onSelect={(id) => {
            if (id) {
              setId(id);
            }
          }}
          treeNodeFilterProp="title"
          showSearch
        />
      </PageContainer>
    </Layout>
  );
}

JenisJabatan.auth = true;
export default JenisJabatan;
