import { Button, Result } from "antd";
import { useRouter } from "next/router";

const Custom403 = () => {
  const router = useRouter();
  return (
    <Result
      status="403"
      title="403"
      subTitle="Hmm.. maaf sepertinya anda tidak diperbolehkan mengakses halaman ini"
      extra={[
        <Button onClick={() => router.push("/")}>Kembali Ke Beranda</Button>,
      ]}
    />
  );
};

export default Custom403;
