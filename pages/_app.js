import "@ant-design/pro-form/dist/form.css";
import "@ant-design/pro-layout/dist/layout.css";
import "@ant-design/pro-table/dist/table.css";
import "antd/dist/antd.css";
import id from "antd/lib/locale/en_US";
import { Provider, signIn, useSession } from "next-auth/client";
import dynamic from "next/dynamic";
import React from "react";
import { QueryClient, QueryClientProvider } from "react-query";
import { Hydrate } from "react-query/hydration";

const AntdProvider = dynamic(() => import("antd/es/config-provider"), {
  ssr: false,
});

const Auth = ({ children }) => {
  const [session, loading] = useSession();
  const isUser = !!session?.user;

  React.useEffect(() => {
    if (loading) return;
    if (!isUser) signIn();
  }, [session, loading]);

  if (isUser) {
    return <>{React.cloneElement(children, { session })}</>;
  } else {
    return null;
  }
};

function MyApp({ Component, pageProps }) {
  const queryClientRef = React.useRef();
  if (!queryClientRef.current) {
    queryClientRef.current = new QueryClient();
  }

  return (
    <Provider
      options={{
        clientMaxAge: 0,
        keepAlive: 0,
        basePath: "/pttpk-fasilitator/api/auth",
      }}
      session={pageProps.session}
    >
      <AntdProvider locale={id}>
        <QueryClientProvider client={queryClientRef.current}>
          <Hydrate state={pageProps.dehydratedState}>
            {Component.auth ? (
              <Auth>
                <Component {...pageProps} />
              </Auth>
            ) : (
              <Component {...pageProps} />
            )}
          </Hydrate>
        </QueryClientProvider>
      </AntdProvider>
    </Provider>
  );
}

export default MyApp;
