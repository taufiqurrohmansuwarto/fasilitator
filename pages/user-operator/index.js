import ProForm, { ProFormRadio, ProFormText } from "@ant-design/pro-form";
import { PageContainer, WaterMark } from "@ant-design/pro-layout";
import ProTable from "@ant-design/pro-table";
import { Button, Card, message } from "antd";
import id from "antd/lib/locale/id_ID";
import dynamic from "next/dynamic";
import { useEffect, useRef, useState } from "react";
import { useMutation, useQuery, useQueryClient } from "react-query";
import { fetchUser, fetchUsers, updateUser } from "../../api/users";
import ProFormTree from "../../components/form/ProFormTree";
import UsersCreate from "../../components/UsersCreate";
import Layout from "../../layouts/Layout";

const DrawerForm = dynamic(
  () => import("@ant-design/pro-form").then((mod) => mod.DrawerForm),
  { ssr: false }
);

function EditOperator({ id, session, reload }) {
  const level = session?.user?.level;

  const { data, refetch, isLoading } = useQuery(
    ["users", id],
    () => fetchUser(id),
    {
      enabled: false,
    }
  );

  const queryClient = useQueryClient();
  const mutation = useMutation((data) => updateUser(data), {
    onSuccess: () => {
      queryClient.invalidateQueries(["users", id]);
      message.success("Berhasil Update");
      setShowDrawer(false);
      reload();
    },
  });

  const [showDrawer, setShowDrawer] = useState(false);

  return (
    <DrawerForm
      title="Edit Operator"
      visible={showDrawer}
      onFinish={async (values) => {
        mutation.mutate({ id, data: values });
      }}
      onVisibleChange={(visible) => {
        if (visible) {
          refetch();
          setShowDrawer(true);
        } else {
          refetch();
          setShowDrawer(false);
        }
      }}
      submitter={{
        searchConfig: {
          resetText: "Reset",
          submitText: "Submit",
        },
        resetButtonProps: {
          style: {
            // Hide the reset button
            display: "none",
          },
        },
      }}
      trigger={<Button onClick={() => setShowDrawer(true)}>Edit</Button>}
    >
      {!isLoading && (
        <>
          <ProForm.Group>
            <ProFormText
              width="xl"
              name="username"
              label="Username"
              tooltip="Username"
              placeholder="Username"
              initialValue={data?.data?.username}
              rules={[{ required: true, message: "Username harus diisi" }]}
            />
          </ProForm.Group>
          <ProForm.Group>
            <ProFormText.Password
              label="Password"
              name="password"
              placeholder="Password"
              width="xl"
              rules={[{ min: 8, message: "Minimal 8 Karakter" }]}
            />
          </ProForm.Group>
          <ProForm.Group>
            <ProFormText
              name="nama_lengkap"
              label="Nama Lengkap"
              tooltip="Username"
              placeholder="Username"
              initialValue={data?.data?.nama_lengkap}
              rules={[
                { required: true, message: "Nama lengkap tidak boleh kosong" },
              ]}
            />
            <ProFormText
              name="email"
              label="Email"
              tooltip="Username"
              placeholder="Username"
              initialValue={data?.data?.email}
            />
            <ProFormText
              name="no_telp"
              label="Nomer Telepon"
              tooltip="Username"
              placeholder="Username"
              initialValue={data?.data?.no_telp}
            />
          </ProForm.Group>
          <ProForm.Group>
            <ProFormTree
              width="xl"
              name="id_skpd"
              label="Perangkat Daerah"
              tooltip="Perangkat Daerah"
              placeholder="Perangkat Daerah"
              initialValue={data?.data?.skpd?.id}
            />
          </ProForm.Group>
          {level === "admin" && (
            <ProForm.Group>
              <ProFormRadio.Group
                initialValue={data?.data?.blokir}
                name="blokir"
                label="Status"
                options={[
                  { label: "Blokir", value: "Y" },
                  { label: "Aktif", value: "N" },
                ]}
              />
            </ProForm.Group>
          )}
        </>
      )}
    </DrawerForm>
  );
}

function UserOperator({ session }) {
  const [page, setPage] = useState(0);
  const [query, setQuery] = useState({
    page: 0,
    username: "",
    pageSize: 20,
  });

  const { data, refetch } = useQuery(
    "users",
    () => {
      return fetchUsers(query);
    },
    { enabled: false }
  );

  const ref = useRef();

  useEffect(() => {
    refetch();
  }, [page, query]);

  const columns = [
    {
      title: "Username",
      dataIndex: "username",
      key: "username",
    },
    {
      title: "Nama Lengkap",
      dataIndex: "nama_lengkap",
      key: "nama_lengkap",
      search: false,
    },
    {
      title: "Perangkat Daerah",
      search: false,
      key: "perangkat_daerah",
      render: (_, row) => {
        return <div>{row?.skpd?.name}</div>;
      },
    },
    {
      title: "Email",
      search: false,
      dataIndex: "email",
      key: "email",
    },
    {
      title: "No. Telepon",
      dataIndex: "no_telp",
      key: "no_telp",
      search: false,
    },
    {
      title: "Level",
      dataIndex: "level",
      key: "level",
      search: false,
    },
    {
      title: "Blokir",
      dataIndex: "blokir",
      key: "blokir",
      search: false,
    },
    {
      title: "Aksi",
      search: false,
      key: "action",
      render: (_, row) => {
        return (
          <EditOperator reload={refetch} id={row?.username} session={session} />
        );
      },
    },
  ];

  return (
    <Layout session={session}>
      <PageContainer
        title="User Operator"
        waterMarkProps={<WaterMark content="" />}
      >
        <Card>
          <ProTable
            toolBarRender={() => {
              const isAdmin = session?.user?.level === "admin";
              if (isAdmin) {
                return [<UsersCreate reload={refetch} />];
              } else {
                return;
              }
            }}
            actionRef={ref}
            rowKey="username"
            locale={id}
            columns={columns}
            request={async (params) => {
              const { username } = params;
              const page = params?.current - 1;
              setQuery({ ...query, page, username });
            }}
            onReset={() => {
              setPage(0);
            }}
            dataSource={data?.results?.data}
            pagination={{
              showSizeChanger: false,
              position: ["bottomRight", "topRight"],
              total: data?.results?.total,
              pageSize: data?.results?.pageSize,
            }}
          />
        </Card>
      </PageContainer>
    </Layout>
  );
}

UserOperator.auth = true;

export default UserOperator;
