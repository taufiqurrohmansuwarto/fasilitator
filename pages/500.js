import { Button, Result } from "antd";
import { useRouter } from "next/router";

const Custom500 = () => {
  const router = useRouter();
  return (
    <Result
      status="500"
      title="500"
      subTitle="Hmm... ada sesuatu yang salah"
      extra={[
        <Button onClick={() => router.push("/")}>Kembali Ke Beranda</Button>,
      ]}
    />
  );
};

export default Custom500;
