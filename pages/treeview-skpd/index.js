import { DownOutlined } from "@ant-design/icons";
import { PageContainer } from "@ant-design/pro-layout";
import { Card, Tree } from "antd";
import React from "react";
import { useMutation, useQuery, useQueryClient } from "react-query";
import { fetchChildrenSkpd, fetchTreeviewSkpd } from "../../api/treeview";
import Layout from "../../layouts/Layout";

function TreeviewSkpd({ session }) {
  const { data, isLoading } = useQuery("treeview-skpd", fetchTreeviewSkpd, {
    refetchOnWindowFocus: false,
  });

  const queryClient = useQueryClient();

  function updateTreeData(list, key, children) {
    return list.map((node) => {
      if (node.key === key) {
        return { ...node, children };
      }

      if (node.children) {
        return {
          ...node,
          children: updateTreeData(node.children, key, children),
        };
      }

      return node;
    });
  }

  const mutation = useMutation(fetchChildrenSkpd, {
    onSuccess: async (data) => {
      await queryClient.cancelQueries("treeview-skpd");
      queryClient.setQueryData("treeview-skpd", (old) => {
        return {
          ...old,
          items: updateTreeData(old?.items, data?.id, data?.results),
        };
      });

      const previousValue = await queryClient.getQueryData("treeview-skpd");

      return previousValue;
    },
  });

  return (
    <Layout session={session}>
      <PageContainer loading={isLoading}>
        <Card>
          <Tree
            treeData={data?.items}
            showLine
            onRightClick={({ event, node }) => alert(JSON.stringify(node))}
            switcherIcon={<DownOutlined />}
            defaultExpandedKeys={[session?.user?.skpd?.id]}
            loadData={({ key }) => {
              mutation.mutate(key);
              return new Promise((resolve, reject) => {
                if (!mutation.isLoading) {
                  resolve();
                }
              });
            }}
          />
        </Card>
      </PageContainer>
    </Layout>
  );
}

TreeviewSkpd.auth = true;

export default TreeviewSkpd;
