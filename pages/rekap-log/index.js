import React from "react";
import Layout from "../../layouts/Layout";

function RekapLog({ session }) {
  return <Layout session={session}></Layout>;
}

RekapLog.auth = true;
export default RekapLog;
