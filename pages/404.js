import { Button, Result } from "antd";
import { useRouter } from "next/router";

const Custom404 = () => {
  const router = useRouter();
  return (
    <Result
      status="404"
      title="404"
      subTitle="Hmm.. Maaf sepertinya halaman yang kamu cari tidak ditemukan"
      extra={
        <Button
          type="primary"
          onClick={() => {
            router.push("/");
          }}
        >
          Kembali Ke Beranda
        </Button>
      }
    />
  );
};

export default Custom404;
