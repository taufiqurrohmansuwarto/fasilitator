import nc from "next-connect";
import auth from "../../../../middlewares/auth";
const pttBiodataController = require("../../../../controllers/ptt-biodata.controller");
const handler = nc();

export default handler
  .use(auth)
  .get(pttBiodataController.index)
  .post(pttBiodataController.post);
