import nc from "next-connect";
import pttAnakController from "../../../../../../controllers/ptt-anak.controller";
import auth from "../../../../../../middlewares/auth";
import checkOwnEmployee from "../../../../../../middlewares/check-own-employee";
const handler = nc();

export default handler
  .use(auth)
  .use(checkOwnEmployee)
  .get(pttAnakController.index)
  .post(pttAnakController.post);
