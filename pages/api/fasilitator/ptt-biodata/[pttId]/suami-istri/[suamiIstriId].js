import nc from "next-connect";
import pttSuamiIstriController from "../../../../../../controllers/ptt-suami-istri.controller";
import auth from "../../../../../../middlewares/auth";
import checkOwnEmployee from "../../../../../../middlewares/check-own-employee";
const handler = nc();

export default handler
  .use(auth)
  .use(checkOwnEmployee)
  .delete(pttSuamiIstriController.remove)
  .patch(pttSuamiIstriController.patch);
