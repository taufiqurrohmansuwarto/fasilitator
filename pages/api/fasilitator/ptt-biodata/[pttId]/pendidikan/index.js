import nc from "next-connect";
import pttPendidikanController from "../../../../../../controllers/ptt-pendidikan.controller";
import auth from "../../../../../../middlewares/auth";
import checkOwnEmployee from "../../../../../../middlewares/check-own-employee";
const handler = nc();

export const config = {
  api: {
    bodyParser: false,
  },
};

const Multer = require("multer");

export default handler
  .use(auth)
  .use(checkOwnEmployee)
  .get(pttPendidikanController.index)
  .post(
    Multer().fields([
      { name: "fileNilai", maxCount: 1 },
      { name: "fileIjazah", maxCount: 1 },
    ]),
    pttPendidikanController.post
  );
