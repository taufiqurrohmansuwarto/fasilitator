import nc from "next-connect";
import pttJabatanController from "../../../../../../controllers/ptt-jabatan.controller";
import auth from "../../../../../../middlewares/auth";
import checkOwnEmployee from "../../../../../../middlewares/check-own-employee";
const handler = nc();

// disable default bodyparser
export const config = {
  api: {
    bodyParser: false,
  },
};

const Multer = require("multer");

export default handler
  .use(auth)
  .use(checkOwnEmployee)
  .get(pttJabatanController.index)
  .post(Multer().single("jabatan"), pttJabatanController.post);
