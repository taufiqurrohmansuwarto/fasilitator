import nc from "next-connect";
import pttBpjsController from "../../../../../../controllers/ptt-bpjs.controller";
import auth from "../../../../../../middlewares/auth";
import checkOwnEmployee from "../../../../../../middlewares/check-own-employee";
const handler = nc();

// disable default bodyparser
export const config = {
  api: {
    bodyParser: false,
  },
};

const Multer = require("multer");

export default handler
  .use(auth)
  .use(checkOwnEmployee)
  .get(pttBpjsController.index)
  .post(Multer().single("bpjs"), pttBpjsController.post);
