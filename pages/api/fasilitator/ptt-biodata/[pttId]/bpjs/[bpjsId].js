import nc from "next-connect";
import pttBpjsController from "../../../../../../controllers/ptt-bpjs.controller";
import auth from "../../../../../../middlewares/auth";
import checkOwnEmployee from "../../../../../../middlewares/check-own-employee";
const handler = nc();

const Multer = require("multer");
// disable default bodyparser
export const config = {
  api: {
    bodyParser: false,
  },
};

export default handler
  .use(auth)
  .use(checkOwnEmployee)
  .get(pttBpjsController.detail)
  .patch(Multer().single("bpjs"), pttBpjsController.patch)
  .delete(pttBpjsController.remove);
