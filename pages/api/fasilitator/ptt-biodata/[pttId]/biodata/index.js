import nc from "next-connect";
import pttBiodataController from "../../../../../../controllers/ptt-biodata.controller";
import auth from "../../../../../../middlewares/auth";
import checkOwnEmployee from "../../../../../../middlewares/check-own-employee";

export const config = {
  api: {
    bodyParser: false, // Disallow body parsing, consume as stream
  },
};

const Multer = require("multer");

const handler = nc();

export default handler
  .use(auth)
  .use(checkOwnEmployee)
  .get(pttBiodataController.detail)
  .patch(Multer().single("foto"), pttBiodataController.patch);
