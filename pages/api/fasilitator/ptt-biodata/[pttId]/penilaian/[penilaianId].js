import nc from "next-connect";
import pttPenilaianController from "../../../../../../controllers/ptt-penilaian.controller";
import auth from "../../../../../../middlewares/auth";
import checkOwnEmployee from "../../../../../../middlewares/check-own-employee";
const handler = nc();

const Multer = require("multer");
// disable default bodyparser
export const config = {
  api: {
    bodyParser: false,
  },
};

export default handler
  .use(auth)
  .use(checkOwnEmployee)
  .get(pttPenilaianController.detail)
  .patch(Multer().single("penilaian"), pttPenilaianController.patch)
  .delete(pttPenilaianController.remove);
