import nc from "next-connect";
import usersController from "../../../../controllers/users.controller";
import auth from "../../../../middlewares/auth";
const handler = nc();

export default handler
  .use(auth)
  .get(usersController.detail)
  .delete(usersController.remove)
  .patch(usersController.patch);
