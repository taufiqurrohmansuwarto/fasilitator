import nc from "next-connect";
import users from "../../../../controllers/users.controller";
import auth from "../../../../middlewares/auth";

const handler = nc();

export default handler.use(auth).get(users.index).post(users.post);
