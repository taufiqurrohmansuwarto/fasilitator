import nc from "next-connect";
import logUsersController from "../../../controllers/log-users.controller";
import auth from "../../../middlewares/auth";
const handler = nc();

export default handler.use(auth).get(logUsersController.index);
