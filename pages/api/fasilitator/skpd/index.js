const skpdController = require("../../../../controllers/skpd.controller.js");
import nc from "next-connect";
import auth from "../../../../middlewares/auth.js";

const handler = nc();

export default handler.use(auth).get(skpdController.index);
