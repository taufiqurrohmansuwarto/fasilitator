import nc from "next-connect";
import logPttController from "../../../controllers/log-ptt.controller";
import auth from "../../../middlewares/auth";
const handler = nc();

export default handler.use(auth).get(logPttController.index);
