import nc from "next-connect";
import refJenjangController from "../../../../controllers/ref-jenjang.controller";
import auth from "../../../../middlewares/auth";
const handler = nc();

export default handler.use(auth).get(refJenjangController.index);
