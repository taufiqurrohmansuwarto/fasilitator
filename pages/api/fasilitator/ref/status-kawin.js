import nc from "next-connect";
import refStatusKawinController from "../../../../controllers/ref-status-kawin.controller";
import auth from "../../../../middlewares/auth";

const handler = nc();

export default handler.use(auth).get(refStatusKawinController.index);
