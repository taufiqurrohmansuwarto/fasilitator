import nc from "next-connect";
import refJabatanController from "../../../../controllers/ref-jabatan.controller";
import auth from "../../../../middlewares/auth";
const handler = nc();

export default handler.use(auth).get(refJabatanController.index);
