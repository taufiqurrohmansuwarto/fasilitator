import nc from "next-connect";
import refStatusAnakController from "../../../../controllers/ref-status-anak.controller";
import auth from "../../../../middlewares/auth";
const handler = nc();

export default handler.use(auth).get(refStatusAnakController.index);
