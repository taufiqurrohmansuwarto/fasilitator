import nc from "next-connect";
import refSkpdController from "../../../../../controllers/ref-skpd.controller";
import auth from "../../../../../middlewares/auth";
const handler = nc();

export default handler.use(auth).get(refSkpdController.index);
