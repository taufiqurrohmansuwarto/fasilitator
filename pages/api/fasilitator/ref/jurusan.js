import nc from "next-connect";
import refJurusanController from "../../../../controllers/ref-jurusan.controller";
import auth from "../../../../middlewares/auth";
const handler = nc();

export default handler.use(auth).get(refJurusanController.index);
