import nc from "next-connect";
import refPekerjaanAnakController from "../../../../controllers/ref-pekerjaan-anak.controller";
import auth from "../../../../middlewares/auth";
const handler = nc();

export default handler.use(auth).get(refPekerjaanAnakController.index);
