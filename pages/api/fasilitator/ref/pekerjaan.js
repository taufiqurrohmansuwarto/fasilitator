import nc from "next-connect";
import refPekerjaanController from "../../../../controllers/ref-pekerjaan.controller";
import auth from "../../../../middlewares/auth";
const handler = nc();

export default handler.use(auth).get(refPekerjaanController.index);
