import nc from "next-connect";
import refStatusSuamiIstriController from "../../../../controllers/ref-status-suami-istri.controller";
import auth from "../../../../middlewares/auth";
const handler = nc();

export default handler.use(auth).get(refStatusSuamiIstriController.index);
