import nc from "next-connect";
import refAgamaController from "../../../../controllers/ref-agama.controller";
import auth from "../../../../middlewares/auth";

const handler = nc();

export default handler.use(auth).get(refAgamaController.index);
