import nc from "next-connect";
import statistikController from "../../../../controllers/statistik.controller";
import auth from "../../../../middlewares/auth";
const handler = nc();

export default handler.use(auth).get(statistikController.index);
