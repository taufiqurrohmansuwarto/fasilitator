import nc from "next-connect";
import auth from "../../../../middlewares/auth";
const handler = nc();
const treeviewSkpdController = require("../../../../controllers/treeview-skpd.controller");

export default handler.use(auth).get(treeviewSkpdController.index);
