import arrayToTree from "array-to-tree";
import has from "lodash/has";
import md5 from "md5";
import NextAuth from "next-auth";
import Providers from "next-auth/providers";
import MenuAdmin from "../../../models/menu-admin.model";
import MenuUser from "../../../models/menu-user.model";
import Users from "../../../models/users.model";

function slugify(string) {
  return string
    .toString()
    .trim()
    .toLowerCase()
    .replace(/\s+/g, "-")
    .replace(/[^\w\-]+/g, "")
    .replace(/\-\-+/g, "-")
    .replace(/^-+/, "")
    .replace(/-+$/, "");
}

export default (req, res) =>
  NextAuth(req, res, {
    providers: [
      Providers.Credentials({
        id: "fasilitator",
        name: "fasilitator",
        credentials: {
          username: {
            label: "Username",
            type: "text",
          },
          password: {
            label: "Password",
            type: "password",
          },
        },
        authorize: async ({ username, password: userPassword }) => {
          if (!username || !userPassword) {
            return Promise.resolve(null);
          }

          const user = await Users.query()
            .where({ username })
            .first()
            .withGraphFetched("[skpd(simple)]");

          if (user) {
            const menu =
              user.level === "admin"
                ? await MenuAdmin.query()
                    .where("aktif", "Y")
                    .select("title as name", "id", "parent_id", "status")
                    .orderBy("menu_order", "ASC")
                : await MenuUser.query()
                    .where("aktif", "Y")
                    .select("title as name", "id", "parent_id", "status")
                    .orderBy("menu_order", "ASC");

            const currentMenu = menu.map((m) => ({
              ...m,
              path: `/${slugify(m.name)}`,
            }));

            const { password } = user;
            if (md5(userPassword) === password) {
              const currentUser = {
                ...user,
                menu: {
                  path: "/",
                  routes: arrayToTree(currentMenu, {
                    customID: "id",
                    parentProperty: "parent_id",
                    childrenProperty: "routes",
                  }),
                },
                name: user.username,
                skpd: user.skpd,
                id: user.username,
                level: user.level,
              };

              return Promise.resolve(currentUser);
            }
          } else {
            return Promise.resolve(null);
          }
        },
      }),
    ],
    callbacks: {
      jwt: async (token, user, account) => {
        if (user) {
          if (account) {
            const { id } = account;
            if (id === "fasilitator") {
              token.role = "fasilitator";
              token.level = user.level;
              token.menu = user.menu;
              token.skpd = user.skpd;
              token.id = user.id;
              token.level = user.level;
            }
          }
        }
        return Promise.resolve(token);
      },
      session: async (session, user) => {
        session.user.role = user.role;
        session.user.id = user.id;

        if (has(user, "menu")) {
          session.user.menu = user.menu;
        }

        if (has(user, "level")) {
          session.user.level = user.level;
        }

        if (has(user, "skpd")) {
          session.user.skpd = user.skpd;
        }

        if (has(user, "detail_skpd")) {
          session.user.detail_skpd = user.detail_skpd;
        }

        if (has(user, "image")) {
          session.user.image = user?.image;
        }

        return Promise.resolve(session);
      },
    },
    debug: true,
    secret: process.env.AUTH_SECRET,
    jwt: {},
    session: { jwt: true },
    theme: "light",
  });
