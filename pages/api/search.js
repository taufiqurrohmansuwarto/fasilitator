import nc from "next-connect";
import auth from "../../middlewares/auth";
const handler = nc();
const searchController = require("../../controllers/search.controller");

export default handler.use(auth).get(searchController.index);
