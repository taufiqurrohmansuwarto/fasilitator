export const skpdSerialize = (listSkpd) => {
  return listSkpd?.map((skpd) => skpd?.name).join(" - ");
};

export const alamatSerialize = (alamat) => {
  const [jalan, rt, rw, kelurahan, kecamatan, kabupaten, provinsi] =
    alamat?.split("|");

  return {
    jalan,
    rt,
    rw,
    kelurahan,
    kecamatan,
    kabupaten,
    provinsi,
  };
};

export const withFileSerialize = (data, id, file, url) => {
  if (data?.length !== 0) {
    const result = data?.map((d) => ({
      ...d,
      fileList: [
        {
          uid: d[id],
          name: d[file],
          status: "success",
          url: `${process.env.HOST_URL}/${url}/${d[file]}`,
        },
      ],
    }));
    return result;
  } else {
    return data;
  }
};

export const pendidikanWithFileSerialize = (listPendidikan) => {
  if (listPendidikan?.length !== 0) {
    return listPendidikan?.map((pendidikan) => ({
      ...pendidikan,
      ...filePendidikanSerialize(pendidikan),
    }));
  } else {
    return [];
  }
};

export const filePendidikanSerialize = (obj) => {
  const {
    id_jenjang,
    id_ptt_pendidikan,
    file_nilai_sma,
    file_ijazah_sma,
    file_nilai_pt,
    file_ijazah_pt,
  } = obj;

  if (id_jenjang < 4) {
    return {
      fileNilaiSma: [
        {
          uid: id_ptt_pendidikan,
          success: true,
          name: file_nilai_sma,
          url: `${process.env.HOST_URL}/pendidikan/${file_nilai_sma}`,
        },
      ],
      fileIjazahSma: [
        {
          uid: id_ptt_pendidikan,
          success: true,
          name: file_ijazah_sma,
          url: `${process.env.HOST_URL}/pendidikan/${file_ijazah_sma}`,
        },
      ],
    };
  } else {
    return {
      fileNilaiPT: [
        {
          uid: id_ptt_pendidikan,
          success: true,
          name: file_nilai_pt,
          url: `${process.env.HOST_URL}/pendidikan/${file_nilai_pt}`,
        },
      ],
      fileIjazahPT: [
        {
          uid: id_ptt_pendidikan,
          success: true,
          name: file_ijazah_pt,
          url: `${process.env.HOST_URL}/pendidikan/${file_ijazah_pt}`,
        },
      ],
    };
  }
};
