// untuk melakukan log pada modul
const logModel = require("../models/log.model");

export default async (username, modul, aksi, id_ptt) => {
  const tgl = new Date();
  const jam = new Date();
  await logModel.query().insert({
    username,
    id_ptt,
    modul,
    aksi,
    tgl,
    jam,
  });
};
