const { nanoid } = require("nanoid");
const upload = (mc, file) => {
  const { fieldname, originalname, buffer, size } = file;
  return new Promise((resolve, reject) => {
    const id = nanoid(5);
    const filename = `${id}-${originalname}`;
    return mc.putObject(
      `pttpk`,
      `${fieldname}/${filename}`,
      buffer,
      size,
      function (err) {
        if (err) {
          reject(err);
        } else {
          resolve(filename);
        }
      }
    );
  });
};

const uploadPendidikan = (mc, file) => {
  const { originalname, buffer, size } = file;
  const filename = nanoid(5) + "-" + originalname;
  return new Promise((resolve, reject) => {
    return mc.putObject(
      `pttpk`,
      `pendidikan/${filename}`,
      buffer,
      size,
      function (err) {
        if (err) {
          reject(err);
        } else {
          resolve(filename);
        }
      }
    );
  });
};

const download = (mc, file, path) => {
  return new Promise((resolve, reject) => {});
};

const remove = (mc, file, path) => {
  return new Promise((resolve, reject) => {});
};

module.exports = {
  upload,
  download,
  remove,
  uploadPendidikan,
};
