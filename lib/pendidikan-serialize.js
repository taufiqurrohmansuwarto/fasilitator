export default function (listPendidikan) {
  if (listPendidikan?.length === 0) {
    return [];
  } else {
    return listPendidikan.map((pendidikan) => {
      const {
        id_ptt_pendidikan,
        id_jenjang,
        // kuliah
        id_ptt,
        nama_pt,
        fakultas_pt,
        jurusan_prodi_pt,
        akreditasi_pt,
        thn_lulus_pt,
        no_ijazah_pt,
        tgl_ijazah_pt,
        ipk_pt,
        file_ijazah_pt,
        file_nilai_pt,
        // sma
        nama_sekolah_sma,
        jurusan_sma,
        akreditasi_sma,
        thn_lulus_sma,
        no_ijazah_sma,
        tgl_ijazah_sma,
        nilai_akhir_sma,
        nilai_un_sma,
        file_nilai_sma,
        file_ijazah_sma,
        // aktif
        aktif,
      } = pendidikan;

      let hasil;
      if (pendidikan.id_jenjang < 4) {
        hasil = {
          id_ptt_pendidikan,
          id_jenjang,
          id_ptt,
          jenjang: { ...pendidikan.jenjang },
          nama_sekolah: nama_sekolah_sma,
          jurusan_sekolah: jurusan_sma,
          akreditasi: akreditasi_sma,
          thn_lulus: thn_lulus_sma,
          no_ijazah: no_ijazah_sma,
          tgl_ijazah: tgl_ijazah_sma,
          file_ijazah: file_ijazah_sma,
          nilai: nilai_akhir_sma,
          nilai_un: nilai_un_sma,
          file_nilai: file_nilai_sma,
          aktif,
        };
      } else {
        hasil = {
          id_ptt_pendidikan,
          id_jenjang,
          id_ptt,
          jenjang: { ...pendidikan.jenjang },
          nama_sekolah: nama_pt,
          fakultas: fakultas_pt,
          jurusan_sekolah: jurusan_prodi_pt,
          akreditasi: akreditasi_pt,
          thn_lulus: thn_lulus_pt,
          no_ijazah: no_ijazah_pt,
          tgl_ijazah: tgl_ijazah_pt,
          ipk: ipk_pt,
          file_ijazah: file_ijazah_pt,
          file_nilai: file_nilai_pt,
          aktif,
        };
      }
      return hasil;
    });
  }
}
