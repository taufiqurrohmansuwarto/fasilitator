import { getSession } from "next-auth/client";
import nc from "next-connect";

const onError = (err, req, res, next) => {};
const onNoMatch = (req, res) => {
  res.status(404).end("page not found");
};

const authMiddleware = async (req, res, next) => {
  const session = await getSession({ req });
  if (!session) {
    res.status(401).json({ code: 404, message: "Not Authorized!" });
  } else {
    req.session = session;
    next();
  }
};

const auth = nc().use(authMiddleware);
const handler = nc({ onError, onNoMatch });
handler.use(auth);

export default handler;
