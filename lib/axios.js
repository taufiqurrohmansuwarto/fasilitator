import { notification } from "antd";
import axios from "axios";

axios.defaults.baseURL =
  "http://localhost:3000/pttpk-fasilitator/api/fasilitator";

axios.defaults.withCredentials = true;

// fucker handler
axios.interceptors.response.use(
  function (response) {
    return response;
  },
  function (error) {
    const description = error?.response?.data?.message;
    notification.error({ description, message: "Peringatan" });
    return Promise.reject(error);
  }
);

export default axios;
