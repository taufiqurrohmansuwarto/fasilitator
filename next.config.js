const withAntdLess = require("next-plugin-antd-less");

module.exports = withAntdLess({
  basePath: "/pttpk-fasilitator",
  // optional
  images: {
    domains: ["iput.online"],
  },

  modifyVars: { "@primary-color": "#73d13d" },
  lessVarsFilePath: "./styles/antd.less",
  // optional
  //   lessVarsFilePath: "./src/styles/variables.less",
  // optional
  // lessVarsFilePathAppendToEndOfContent: false,
  // optional https://github.com/webpack-contrib/css-loader#object
  cssLoaderOptions: {},

  // Other Config Here...

  webpack(config) {
    return config;
  },

  // future: {
  // if you use webpack5
  // webpack5: true,
  // },
});
