// .babelrc.js
module.exports = {
  presets: [["next/babel"]],
  plugins: [
    ["import", { libraryName: "antd", style: true }],
    [
      "import",
      { libraryName: "@ant-design/pro-layout", style: true },
      "antd-pro-layout",
    ],
    [
      "import",
      { libraryName: "@ant-design/pro-utils", style: true },
      "antd-pro-utils",
    ],
    [
      "import",
      { libraryName: "@ant-design/pro-form", style: true },
      "antd-pro-form",
    ],
    [
      "import",
      { libraryName: "@ant-design/pro-field", style: true },
      "antd-pro-field",
    ],
  ],
};
