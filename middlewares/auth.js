// melakukan otentifikasi dulu dan di isi data2
const { getSession } = require("next-auth/client");
const Minio = require("minio");
const minioSetting = {
  endPoint: "iput.online",
  port: 9000,
  useSSL: true,
  accessKey: process.env.MINIO_ACCESSKEY,
  secretKey: process.env.MINIO_SECRETKEY,
};

const mc = new Minio.Client(minioSetting);

module.exports = async (req, res, next) => {
  const session = await getSession({ req });
  if (!session) {
    res.status(401).json({ code: 404, message: "Not Authorized!" });
  } else {
    req.session = session;
    req.mc = mc;
    next();
  }
};
