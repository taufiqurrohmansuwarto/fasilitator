/**
 * middleware untuk membatasi data pegawai sesuai dengan fasilitator perangkat daerah masing-masing
 */
const pttBiodataModel = require("../models/ptt-biodata.models");

export default async (req, res, next) => {
  const { pttId } = req.query;
  const { id: idSkpdFasilitator } = req.session?.user?.skpd;
  const isAdmin = req.session?.user?.level === "admin";

  try {
    const result = await pttBiodataModel
      .query()
      .findById(pttId)
      .first()
      .select("id_skpd");

    if (!result) {
      res.status(404).json({ code: 404, message: "Data Not Found" });
    } else {
      // kadang, ada pegawai yang tidak ada id_skpd tapi kalau admin bisa melakukan akses pegawai tersebut
      const pegawaiSkpdNull = isAdmin && !result?.id_skpd;
      const isRightSkpd = result?.id_skpd?.startsWith(idSkpdFasilitator);

      if (pegawaiSkpdNull) {
        next();
      } else if (isRightSkpd) {
        next();
      } else {
        res.status(403).json({ code: 403, message: "Forbidden" });
      }
    }
  } catch (error) {
    res.status(400).json({ code: 400, message: "Internal Server Errorr" });
  }
};
