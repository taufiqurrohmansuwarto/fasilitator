const has = require("lodash/has");

const model = require("../models/rwyt-suami-istri.model");

const index = async (req, res) => {
  const { pttId } = req.query;
  try {
    const result = await model
      .query()
      .where("id_ptt", pttId)
      .withGraphFetched("[pekerjaan, status]");
    res.status(200).json(result);
  } catch (error) {
    res.status(400).json({ code: 400, message: "Internal Server Error" });
  }
};

const post = async (req, res) => {
  const { pttId } = req.query;
  const { body } = req;
  try {
    const data = { ...body, id_ptt: pttId };
    await model.query().insert(data);
    res.status(200).json({ code: 200 });
  } catch (error) {
    console.log(error);
    res.status(400).json({ code: 400, message: "Internal Server Error" });
  }
};

const patch = async (req, res) => {
  const { pttId, suamiIstriId } = req.query;
  const { body } = req;

  try {
    if (has(body, "aktif") && body?.aktif === "Y") {
      const result = await model
        .query()
        .where("id_ptt", pttId)
        .select("suami_istri_id");

      const selainResult = result?.filter(
        (r) => r?.suami_istri_id !== parseInt(suamiIstriId)
      );

      if (selainResult?.length === 0) {
        await model
          .query()
          .patch(body)
          .where("id_ptt", pttId)
          .andWhere("suami_istri_id", suamiIstriId);
      } else {
        const idSelainResult = selainResult?.map((r) => r?.suami_istri_id);
        await model
          .query()
          .patch(body)
          .where("id_ptt", pttId)
          .andWhere("suami_istri_id", suamiIstriId);
        await model
          .query()
          .patch({ aktif: "N" })
          .whereIn("suami_istri_id", idSelainResult)
          .andWhere("id_ptt", pttId);
      }
    } else {
      await model
        .query()
        .patch(body)
        .where("id_ptt", pttId)
        .andWhere("suami_istri_id", suamiIstriId);
    }
    res.status(200).json({ code: 200 });
  } catch (error) {
    res.status(400).json({ code: 400, message: "Internal Server Error" });
  }
};

const remove = async (req, res) => {
  const { pttId, suamiIstriId } = req.query;
  try {
    await model
      .query()
      .delete()
      .where("id_ptt", pttId)
      .andWhere("suami_istri_id", suamiIstriId);
    res.status(200).json({ code: 200, message: "Success" });
  } catch (error) {
    res.status(400).json({ code: 400, message: "Internal Server Error" });
  }
};

const detail = async (req, res) => {
  const { pttId, suamiIstriId } = req.query;
  try {
    const result = await model
      .query()
      .where("id_ptt", pttId)
      .andWhere("suami_istri_id", suamiIstriId)
      .withGraphFetched("[pekerjaan, status]")
      .first();
    res.status(200).json(result);
  } catch (error) {
    res.status(400).json({ code: 400, message: "Internal Server Error" });
  }
};

export default {
  index,
  patch,
  post,
  remove,
  detail,
};
