const model = require("../models/ref-status-suami-istri.model");

const index = async (req, res) => {
  try {
    const result = await model.query().select({
      id: "status_suami_istri_id",
      key: "status_suami_istri_id",
      value: "status_suami_istri_id",
      title: "status_suami_istri",
      label: "status_suami_istri",
    });
    res.status(200).json(result);
  } catch (error) {
    console.log(error);
    res.status(400).json({ code: 400, message: "Internal Server Error" });
  }
};

const post = async (req, res) => {};
const detail = async (req, res) => {};
const patch = async (req, res) => {};
const remove = async (req, res) => {};

module.exports = {
  index,
  post,
  detail,
  patch,
  remove,
};
