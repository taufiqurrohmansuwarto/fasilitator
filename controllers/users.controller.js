const has = require("lodash/has");
const omit = require("lodash/omit");
const model = require("../models/users.model");
const md5 = require("md5");

const index = async (req, res) => {
  const { page = 0, pageSize = 20 } = req.query;
  const { session } = req;

  const isUser = session?.user?.level === "user";

  try {
    const data = await model
      .query()
      .where("id_skpd", "like", `${session?.user?.skpd?.id}%`)
      .andWhere((builder) => {
        if (has(req.query, "username")) {
          builder.where("username", "like", `%${req?.query?.username}%`);
        }
        if (isUser) {
          builder.where("level", "user");
        }
      })
      .page(page, pageSize)
      .modify("simple")
      .withGraphFetched("skpd(simple)");

    res.status(200).json({
      code: 200,
      message: "Success",
      results: {
        data: data?.results,
        page,
        pageSize,
        total: data?.total,
      },
    });
  } catch (error) {
    res.status(400).json({ code: 400, message: "Internal Server Error" });
  }
};

const detail = async (req, res) => {
  const { session } = req;
  try {
    const data = await model
      .query()
      .findById(req?.query?.userId)
      .andWhere("id_skpd", "like", `${session?.user?.skpd?.id}%`)
      .modify("simple")
      .withGraphFetched("skpd(simple)");
    res.status(200).json({ code: 200, message: "Success", data });
  } catch (error) {
    res.status(400).json({ code: 400, message: "Internal Server Error" });
  }
};

const patch = async (req, res) => {
  try {
    const {
      user: { level },
    } = req.session;

    const { userId } = req.query;
    const data = req.body;

    if (has(data, "blokir") && level !== "admin") {
      res.status(403).json({ code: 403, message: "Forbidden" });
    }

    if (has(data, "password") && !!data?.password) {
      const { password: currentPasword } = data;
      const password = md5(currentPasword);
      console.log({ currentPasword, password });

      await model
        .query()
        .patch({ ...data, password })
        .where("username", userId);
    } else {
      await model
        .query()
        .patch(omit(data, ["password"]))
        .where("username", userId);
    }

    res.status(200).json({ code: 200, message: "success" });
  } catch (error) {
    res.status(400).json({ code: 400, message: "Internal Server Error" });
  }
};

const remove = async (req, res) => {
  try {
  } catch (error) {}
};

const post = async (req, res) => {
  const { session, body } = req;
  const isAdmin = session?.user?.level === "admin";
  try {
    // just for fucking admin
    if (!isAdmin) {
      res.status(403).json({ code: 403, message: "Forbidden" });
    } else {
      const password = md5(body?.password);
      const id_session = md5(new Date().getTime() / 1000);
      await model.query().insert({ ...body, password, id_session });
      res.status(200).json({ code: 200, message: "Success" });
    }
  } catch (error) {
    console.log(error);
    res.status(400).json({ code: 400, message: "Internal Server Error" });
  }
};

export default {
  index,
  patch,
  detail,
  remove,
  post,
};
