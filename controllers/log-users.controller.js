const model = require("../models/log.model");
const moment = require("moment");

const index = async (req, res) => {
  try {
    const { session } = req;
    const skpdId = session?.user?.skpd?.id;
    const {
      tgl_awal = moment(new Date()).format("YYYY-DD-MM"),
      tgl_akhir = moment(new Date()).format("YYYY-DD-MM"),
      page = 0,
      pageSize = 20,
    } = req.query;

    const result = await model
      .query()
      .whereBetween("tgl", [tgl_awal, tgl_akhir])
      .andWhere("id_skpd", "like", `${skpdId}%`)
      .limit(20)
      .page(page, pageSize);

    res.status(200).json({
      code: 200,
      message: "success",
      data: {
        total: result?.total,
        data: result?.results,
      },
    });
  } catch (error) {
    console.log(error);
    res.status(400).json({ code: 400, message: "Internal Server Error" });
  }
};

export default {
  index,
};
