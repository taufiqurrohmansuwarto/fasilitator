const model = require("../models/rwyt-anak.model");
const has = require("lodash/has");
const userId = "id_ptt";
const id = "anak_id";

const index = async (req, res) => {
  const { pttId } = req.query;
  try {
    const result = await model
      .query()
      .where(userId, pttId)
      .withGraphFetched(
        "[statusSuamiIstri, statusAnak, suamiIstri(simple), pekerjaanAnak]"
      );
    res.status(200).json(result);
  } catch (error) {
    res.status(400).json({ code: 400, message: "Internal Server Error" });
  }
};

const patch = async (req, res) => {
  const { pttId, anakId } = req.query;
  const { body } = req;

  try {
    const data = { id_ptt: parseInt(pttId, 10), ...body };
    await model
      .query()
      .patch(data)
      .where("id_ptt", pttId)
      .andWhere("anak_id", anakId);
    res.status(200).json({ code: 200, message: "success" });
  } catch (error) {
    console.log(error);
    res.status(400).json({ code: 400, message: "Internal Server Error" });
  }
};

const post = async (req, res) => {
  const { pttId } = req.query;
  const { body } = req;
  try {
    const data = { ...body, id_ptt: parseInt(pttId, 10) };
    await model.query().insert(data);
    res.status(200).json({ code: 200 });
  } catch (error) {
    console.log(error);
    res.status(400).json({ code: 400, message: "Internal Server Error" });
  }
};

const remove = async (req, res) => {
  const { pttId, anakId } = req.query;
  try {
    await model.query().delete().where(userId, pttId).andWhere(id, anakId);
    res.status(200).json({ code: 200, message: "Success" });
  } catch (error) {
    console.log(error);
    res.status(400).json({ code: 400, message: "Internal Server Error" });
  }
};

const detail = async (req, res) => {
  const { pttId, anakId } = req.query;
  try {
    const result = await model
      .query()
      .where(userId, pttId)
      .andWhere(id, anakId)
      .withGraphFetched("[statusSuamiIstri, statusAnak, suamiIstri(simple)]")
      .first();
    res.status(200).json(result);
  } catch (error) {
    res.status(400).json({ code: 400, message: "Internal Server Error" });
  }
};

export default {
  index,
  detail,
  patch,
  post,
  remove,
};
