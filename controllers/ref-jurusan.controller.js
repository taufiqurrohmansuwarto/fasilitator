const model = require("../models/ref-jurusan.model");
const has = require("lodash/has");

const index = async (req, res) => {
  try {
    const { jurusan } = req.query;
    const result = await model
      .query()
      .where((builder) => {
        if (has(req.query, "jurusan")) {
          builder.where("nama_jurusan", "like", `%${jurusan}%`);
        } else {
        }
      })
      .limit(100)
      .orderBy("nama_jurusan", "asc");
    res.status(200).json(result);
  } catch (error) {
    console.log(error);
    res.status(200).json({ code: 400, message: "Internal Server Error" });
  }
};

const post = async (req, res) => {};
const detail = async (req, res) => {};
const patch = async (req, res) => {};
const remove = async (req, res) => {};

export default {
  index,
  post,
  detail,
  patch,
  remove,
};
