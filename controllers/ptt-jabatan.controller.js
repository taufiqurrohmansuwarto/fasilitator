import { upload } from "../lib/file";
import { withFileSerialize } from "../lib/serialize/components.serialize";

const has = require("lodash/has");

const model = require("../models/ptt-jabatan.model");
const userId = "id_ptt";
const id = "id_ptt_jab";

const index = async (req, res) => {
  const { pttId } = req.query;
  try {
    const result = await model
      .query()
      .where("id_ptt", pttId)
      .withGraphFetched("[jabatan]");
    res.status(200).json(withFileSerialize(result, id, "file", "jabatan"));
  } catch (error) {
    res.status(400).json({ code: 400, message: "Internal Server Error" });
  }
};

const patch = async (req, res) => {
  const { pttId, jabatanId } = req.query;
  const { body } = req;

  try {
    if (has(body, "aktif") && body?.aktif === "Y") {
      const result = await model.query().where(userId, pttId).select(id);
      const selainResult = result?.filter(
        (r) => r?.[id] !== parseInt(jabatanId)
      );
      if (selainResult?.length === 0) {
        await model
          .query()
          .patch(body)
          .where(userId, pttId)
          .andWhere(id, jabatanId);
      } else {
        const idSelainResult = selainResult?.map((r) => r?.[id]);

        await model
          .query()
          .patch({ aktif: "Y" })
          .where(userId, parseInt(pttId, 10))
          .andWhere(id, parseInt(jabatanId, 10));

        console.log(jabatanId);
        await model
          .query()
          .patch({ aktif: "N" })
          .whereIn(id, idSelainResult)
          .andWhere(userId, pttId);
      }
    } else {
      let data;
      if (has(req, "file")) {
        const file = await upload(req?.mc, req?.file);
        data = { ...body, id_ptt: pttId, file, id_ptt_jab: jabatanId };
      } else {
        data = { ...body, id_ptt: pttId, id_ptt_jab: jabatanId };
      }
      await model
        .query()
        .patch(data)
        .where(userId, pttId)
        .andWhere(id, jabatanId);
    }
    res.status(200).json({ code: 200 });
  } catch (error) {
    console.log(error);
    res.status(400).json({ code: 400, message: "Internal Server Error" });
  }
};

const post = async (req, res) => {
  const { pttId } = req.query;
  const { body } = req;
  try {
    let data;
    if (has(req, "file")) {
      const file = await upload(req?.mc, req?.file);
      data = { ...body, id_ptt: pttId, file };
    } else {
      data = { ...body, id_ptt: pttId };
    }
    await model.query().insert(data);
    res.status(200).json({ code: 200, message: "Success" });
  } catch (error) {
    console.log(error);
    res.status(400).json({ code: 400, message: "Internal Server Error" });
  }
};

const remove = async (req, res) => {
  const { jabatanId, pttId } = req.query;
  try {
    await model.query().delete().where(userId, pttId).andWhere(id, jabatanId);
    res.status(200).json({ code: 200, message: "success" });
  } catch (error) {
    res.status(400).json({ code: 400, message: "Internal Server Error" });
  }
};

const detail = async (req, res) => {};

export default {
  index,
  detail,
  patch,
  post,
  remove,
};
