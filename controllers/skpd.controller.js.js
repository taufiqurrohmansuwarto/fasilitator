const model = require("../models/skpd.model");
const arrayToTree = require("array-to-tree");

const index = async (req, res) => {
  const { session } = req;
  try {
    const data = await model
      .query()
      .select({
        title: "name",
        label: "name",
        id: "id",
        pId: "pId",
        value: "id",
      })
      .where("id", "like", `${session?.user?.skpd?.id}%`);

    const result = arrayToTree(data, { parentProperty: "pId", customID: "id" });
    res.status(200).json(result);
  } catch (error) {
    res.status(400).json({ code: 400, message: "Internal Server Error" });
  }
};

const detail = async (req, res) => {};
const patch = async (req, res) => {};
const remove = async (req, res) => {};

module.exports = {
  index,
  patch,
  detail,
  remove,
};
