const model = require("../models/setting-agama.model");

const index = async (req, res) => {
  try {
    const result = await model.query();
    res.status(200).json(result);
  } catch (error) {
    res.status(400).json({ code: 400, message: "Internal Server Error" });
  }
};
const detail = async (req, res) => {};
const patch = async (req, res) => {};
const remove = async (req, res) => {};
const post = async (req, res) => {};

export default {
  index,
  detail,
  patch,
  remove,
  post,
};
