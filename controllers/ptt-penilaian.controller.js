import { upload } from "../lib/file";
import { withFileSerialize } from "../lib/serialize/components.serialize";
const has = require("lodash/has");

const model = require("../models/ptt-penilaian.model");
const userId = "id_ptt";
const id = "id_ptt_penilaian";

const index = async (req, res) => {
  const { pttId } = req.query;
  try {
    const result = await model.query().where("id_ptt", pttId);
    res.status(200).json(withFileSerialize(result, id, "file", "bpjs"));
  } catch (error) {
    res.status(400).json({ code: 400, message: "Internal Server Error" });
  }
};

const patch = async (req, res) => {
  const { pttId, penilaianId } = req.query;
  const { body } = req;
  try {
    let data;
    if (has(req, "file")) {
      const file = await upload(req?.mc, req?.file);
      data = { ...body, id_ptt: pttId, file };
    } else {
      data = { ...body, id_ptt: pttId };
    }
    await model
      .query()
      .patch(data)
      .where(userId, pttId)
      .andWhere(id, penilaianId);
    res.status(200).json({ code: 200, message: "Success" });
  } catch (error) {
    console.log(error);
    res.status(400).json({ code: 400, message: "Internal Server Error" });
  }
};

const post = async (req, res) => {
  const { pttId } = req.query;
  const { body } = req;
  try {
    let data;
    if (has(req, "file")) {
      const file = await upload(req?.mc, req?.file);
      data = { ...body, id_ptt: pttId, file };
    } else {
      data = { ...body, id_ptt: pttId };
    }
    await model.query().insert(data);
    res.status(200).json({ code: 200, message: "Success" });
  } catch (error) {
    res.status(400).json({ code: 400, message: "Internal Server Error" });
  }
};

const remove = async (req, res) => {
  const { pttId, penilaianId } = req.query;
  try {
    await model.query().delete().where(userId, pttId).andWhere(id, penilaianId);
    res.status(200).json({ code: 200, message: "success" });
  } catch (error) {
    console.log(error);
    res.status(400).json({ code: 400, message: "Internal Server Error" });
  }
};

const detail = async (req, res) => {};

export default {
  index,
  detail,
  patch,
  post,
  remove,
};
