const model = require("../models/jabatan.model");
const arrayToTree = require("array-to-tree");
const toString = require("lodash/toString");

const index = async (req, res) => {
  try {
    const data = await model.query().select({
      title: "name",
      label: "name",
      id: "id_jabatan",
      key: "id_jabatan",
      pId: "pId",
      value: "id_jabatan",
    });

    const currentData = data?.map((d) => ({
      title: toString(d?.title),
      label: toString(d?.label),
      id: toString(d?.id),
      key: toString(d?.key),
      value: toString(d?.value),
      pId: toString(d?.pId),
    }));

    const result = arrayToTree(currentData, {
      parentProperty: "pId",
      customID: "id",
    });
    res.status(200).json(result);
  } catch (error) {
    res.status(200).json({ code: 400, message: "Internal Server Error" });
  }
};

const post = async (req, res) => {
  const { jabatanId } = req?.query;
  const { body } = req;
  try {
    let data;
    const lastChildrenJabatan = await model
      .query()
      .where("pId", jabatanId)
      .orderBy("id_jabatan", "DESC")
      .first();

    if (!lastChildrenJabatan) {
      data = { pId: jabatanId, id_jabatan: `${skpdId}01` };
    } else {
      data = {
        pId: jabatanId,
        id_jabatan: `${parseInt(lastChildrenJabatan?.id, 10) + 1}`,
      };
    }
    await model.query().insert({ ...body, ...data });
    res.status(200).json({ code: 200, message: "success" });
  } catch (error) {
    res.status(400).json({ code: 400, message: "Internal Server Error" });
  }
};

const detail = async (req, res) => {
  const { jabatanId } = req?.query;
  try {
    const result = await model.query().findById(jabatanId);
    res.status(200).json(result);
  } catch (error) {
    res.status(400).json({ code: 400, message: "Internal Server Error" });
  }
};

const patch = async (req, res) => {
  const { jabatanId } = req?.query;
  const {
    body: { name },
  } = req;
  try {
    await model
      .query()
      .patch({ name: name?.trim()?.toUpperCase() })
      .where("jabatanId", jabatanId);

    res.status(200).json({ code: 200, message: "success" });
  } catch (error) {
    res.status(400).json({ code: 400, message: "Internal Server Error" });
  }
};

const remove = async (req, res) => {
  const { jabatanId } = req?.query;
  try {
    const hasChildren = await model.query().where("pId", skpdId);
    if (hasChildren?.length !== 0) {
      res.status(422).json({
        code: 422,
        message: "Tidak dapat menghapus data! Pilih jabatan yang paling kecil!",
      });
    } else {
      await model.query().deleteById(jabatanId);
      res.status(200).json({ code: 200, message: "Success" });
    }
  } catch (error) {
    res.status(400).json({ code: 400, message: "Internal Server Error" });
  }
};

export default {
  index,
  post,
  detail,
  patch,
  remove,
};
