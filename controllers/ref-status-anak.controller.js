const model = require("../models/ref-status-anak.model");

const index = async (req, res) => {
  try {
    const result = await model.query().select({
      id: "status_anak_id",
      key: "status_anak_id",
      value: "status_anak_id",
      label: "status_anak",
      title: "status_anak",
    });

    res.status(200).json(result);
  } catch (error) {
    console.log(error);
    res.status(200).json({ code: 400, message: "Internal Server Error" });
  }
};

const post = async (req, res) => {};
const detail = async (req, res) => {};
const patch = async (req, res) => {};
const remove = async (req, res) => {};

export default {
  index,
  post,
  detail,
  patch,
  remove,
};
