import { upload } from "../lib/file";
import { withFileSerialize } from "../lib/serialize/components.serialize";

const model = require("../models/ptt-bpjs.model");
const has = require("lodash/has");

const index = async (req, res) => {
  const { pttId } = req.query;
  try {
    const result = await model.query().where("id_ptt", pttId);
    res
      .status(200)
      .json(withFileSerialize(result, "id_ptt_bpjs", "file", "bpjs"));
  } catch (error) {
    res.status(400).json({ code: 400, message: "Internal Server Error" });
  }
};

const detail = async (req, res) => {
  const { pttId, bpjsId } = req.query;
  try {
    const result = await model
      .query()
      .where("id_ptt", pttId)
      .andWhere("id_ptt_bpjs", bpjsId);
    res.status(200).json(result);
  } catch (error) {
    res.status(400).json({ code: 400, message: "Internal Server Error" });
  }
};

const patch = async (req, res) => {
  const { pttId, bpjsId } = req.query;
  const { body } = req;
  try {
    let data;
    if (has(req, "file")) {
      const file = await upload(req?.mc, req?.file);
      data = { ...body, id_ptt: pttId, file };
    } else {
      data = { ...body, id_ptt: pttId };
    }

    await model
      .query()
      .patch(data)
      .where("id_ptt", pttId)
      .andWhere("id_ptt_bpjs", bpjsId);
    res.status(200).json({ code: 200, message: "Success" });
  } catch (error) {
    console.log(error);
    res.status(400).json({ code: 400, message: "Internal Server Error" });
  }
};

const remove = async (req, res) => {
  const { pttId, bpjsId } = req.query;
  try {
    await model
      .query()
      .delete()
      .where("id_ptt", pttId)
      .andWhere("id_ptt_bpjs", bpjsId);
    res.status(200).json({ code: 200, message: "success" });
  } catch (error) {
    console.log(error);
    res.status(400).json({ code: 400, message: "Internal Server Error" });
  }
};

const post = async (req, res) => {
  const { pttId } = req.query;
  const { body } = req;
  try {
    if (has(req, "file")) {
      const name = await upload(req?.mc, req?.file);
      const data = { ...body, file: name, id_ptt: pttId };
      await model.query().insert(data);
    } else {
      await model.query().insert({ ...body, id_ptt: pttId });
    }
    res.status(200).json({ code: 200, message: "success" });
  } catch (error) {
    console.log(error);
    res.status(400).json({ code: 400, message: "Internal Server Error" });
  }
};

export default {
  index,
  detail,
  patch,
  remove,
  post,
};
