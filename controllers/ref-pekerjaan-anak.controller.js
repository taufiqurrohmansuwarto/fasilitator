const model = require("../models/ref-pekerjaan-anak.model");

const index = async (req, res) => {
  try {
    const result = await model.query().select({
      id: "pekerjaan_id",
      pekerjaan: "pekerjaan",
      label: "pekerjaan",
      value: "pekerjaan_id",
    });
    res.status(200).json(result);
  } catch (error) {
    res.status(200).json({ code: 400, message: "Internal Server Error" });
  }
};

const post = async (req, res) => {};
const detail = async (req, res) => {};
const patch = async (req, res) => {};
const remove = async (req, res) => {};

export default {
  index,
  post,
  detail,
  patch,
  remove,
};
