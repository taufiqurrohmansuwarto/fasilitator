import { uploadPendidikan } from "../lib/file";
import { pendidikanWithFileSerialize } from "../lib/serialize/components.serialize";
const has = require("lodash/has");

const model = require("../models/ptt-pendidikan.model");
const userId = "id_ptt";
const id = "id_ptt_pendidikan";

const index = async (req, res) => {
  const { pttId } = req.query;
  try {
    const result = await model
      .query()
      .where("id_ptt", pttId)
      .withGraphFetched("[jenjang]");
    const hasil = pendidikanWithFileSerialize(result);
    res.status(200).json(hasil);
  } catch (error) {
    console.log(error);
    res.status(400).json({ code: 400, message: "Internal Server Error" });
  }
};

const patch = async (req, res) => {
  const { pttId, pendidikanId } = req.query;
  const { body } = req;

  try {
    if (has(body, "aktif") && req?.body?.aktif === "Y") {
      const result = await model.query().where(userId, pttId).select(id);
      const selainResult = result?.filter(
        (r) => r?.[id] !== parseInt(pendidikanId)
      );
      if (selainResult?.length === 0) {
        await model
          .query()
          .patch(body)
          .where(userId, pttId)
          .andWhere(id, pendidikanId);
      } else {
        const idSelainResult = selainResult?.map((r) => r?.[id]);

        await model
          .query()
          .patch({ aktif: "Y" })
          .where(userId, parseInt(pttId, 10))
          .andWhere(id, parseInt(pendidikanId, 10));

        await model
          .query()
          .patch({ aktif: "N" })
          .whereIn(id, idSelainResult)
          .andWhere(userId, pttId);
      }
    } else {
      // import gambar
      let data;
      const idJenjang = body?.id_jenjang;
      if (req.files) {
        let namaFile = {};
        if (has(req.files, "fileNilai")) {
          const [fileNilai] = req?.files?.fileNilai;
          const namaFileNilai = await uploadPendidikan(req?.mc, fileNilai);
          if (idJenjang < 4) {
            namaFile = { ...namaFile, file_nilai_sma: namaFileNilai };
          } else {
            namaFile = { ...namaFile, file_nilai_pt: namaFileNilai };
          }
        }
        if (has(req.files, "fileIjazah")) {
          const [fileIjazah] = req?.files?.fileIjazah;
          const namaFileIjazah = await uploadPendidikan(req?.mc, fileIjazah);
          namaFile = { ...namaFile, file_ijazah_pt: namaFileIjazah };
          if (idJenjang < 4) {
            namaFile = { ...namaFile, file_ijazah_sma: namaFileIjazah };
          } else {
            namaFile = { ...namaFile, file_ijazah_pt: namaFileIjazah };
          }
        }
        data = {
          ...body,
          id_ptt: pttId,
          id_ptt_pendidikan: pendidikanId,
          ...namaFile,
        };
      } else {
        data = { ...body, id_ptt: pttId, id_ptt_pendidikan: pendidikanId };
      }

      await model
        .query()
        .patch(data)
        .where("id_ptt", pttId)
        .andWhere("id_ptt_pendidikan", pendidikanId);
    }
    res.status(200).json({ code: 200, messge: "Success" });
  } catch (error) {
    console.log(error);
    res.status(400).json({ code: 400, message: "Internal Server Error" });
  }
};

const post = async (req, res) => {
  const { pttId } = req.query;
  try {
    let data;
    const { fileNilai, fileIjazah } = req?.files;

    const nilai = fileNilai?.[0];
    const ijazah = fileIjazah?.[0];
    const namaFileNilai = await uploadPendidikan(req?.mc, nilai);
    const namaFileIjazah = await uploadPendidikan(req?.mc, ijazah);

    if (parseInt(req?.body?.id_jenjang, 10) < 4) {
      data = {
        ...req?.body,
        file_nilai_sma: namaFileNilai,
        file_ijazah_sma: namaFileIjazah,
        id_ptt: pttId,
      };
    } else {
      data = {
        ...req?.body,
        file_nilai_pt: namaFileNilai,
        file_ijazah_pt: namaFileIjazah,
        id_ptt: pttId,
      };
    }

    await model.query().insert(data);
    res.status(200).json({ code: 200, message: "success" });
  } catch (error) {
    console.log(error);
    res.status(400).json({ code: 400, message: "Internal Server Error" });
  }
};

const remove = async (req, res) => {
  const { pttId, pendidikanId } = req.query;
  try {
    await model
      .query()
      .delete()
      .where(userId, pttId)
      .andWhere(id, pendidikanId);
    res.status(200).json({ code: 200, message: "Success" });
  } catch (error) {
    res.status(400).json({ code: 400, message: "Internal Server Error" });
  }
};

const detail = async (req, res) => {};

export default {
  detail,
  index,
  patch,
  post,
  remove,
};
