const fasilitator = require("../models/users.model");
const pegawai = require("../models/ptt-biodata.models");

const index = async (req, res) => {
  const { name, type, status } = req.query;

  let result = [];

  if (type === "users") {
    result = await fasilitator
      .query()
      .where("username", "like", `%${name}%`)
      .select("username", "nama_lengkap");
  }

  if (type === "pegawai") {
    result = await pegawai
      .query()
      .where("nama", "like", `%${name}%`)
      .andWhere((builder) => {
        if (status === "blokir") {
          builder.where("blokir", "Y").andWhere("aktif", "N");
        }
        if (status === "aktif") {
          builder.where("blokir", "N").andWhere("aktif", "Y");
        }
      });
  }

  res.status(200).json(result);
};

module.exports = {
  index,
};
