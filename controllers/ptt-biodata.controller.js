import detailSkpd from "../lib/detail-skpd";
import { upload } from "../lib/file";
import pendidikanSerialize from "../lib/pendidikan-serialize";
import { alamatSerialize } from "../lib/serialize/components.serialize";
const has = require("lodash/has");
const omit = require("lodash/omit");
const uniqBy = require("lodash/uniqBy");
const groupBy = require("lodash/groupBy");
const md5 = require("md5");
const model = require("../models/ptt-biodata.models");

const index = async (req, res) => {
  // isi dengan default skpd
  const { session } = req;
  const skpdId = session?.user?.skpd?.id;
  let offset = 0;
  const { active = true } = req.query;

  try {
    let joinRelated = [];
    let idSkpd = `^${skpdId}`;

    let query = model
      .query()
      .modify("minimize")
      .withGraphFetched(
        "[pendidikan(aktif).[jenjang], jabatan(simpleAktif).[jabatan(simple)]]"
      )
      .where((builder) => {
        const { nama, jenis_kelamin, id_agama, id_kawin, usia } = req.query;

        if (has(req.query, "nama")) {
          builder.where("nama", "like", `%${nama}%`);
        }
        if (has(req.query, "jenis_kelamin")) {
          builder.where("jk", jenis_kelamin);
        }
        if (has(req.query, "id_agama")) {
          builder.whereIn("id_agama", id_agama?.split(","));
        }
        if (has(req.query, "id_kawin")) {
          builder.whereIn("id_kawin", id_kawin?.split(","));
        }
        if (has(req.query, "usia")) {
          const currentUsia = usia?.split(",");
          let raw;
          if (currentUsia.length === 1) {
            raw = `(YEAR(NOW()) - YEAR(ptt_biodata.thn_lahir)) = ?`;
          }
          if (currentUsia.length === 2) {
            raw = `(YEAR(NOW()) - YEAR(ptt_biodata.thn_lahir))  BETWEEN ? AND ?`;
          }

          builder.whereRaw(raw, currentUsia);
        }
        if (active) {
          builder
            .where("ptt_biodata.aktif", "Y")
            .andWhere("ptt_biodata.blokir", "N");
        }
        if (!active) {
          builder.where("ptt_biodata.aktif", "N");
        }
      });
    // .andWhere("ptt_biodata.aktif", "Y")
    // .andWhere("ptt_biodata.blokir", "N");

    if (has(req.query, "id_skpd")) {
      const result = req?.query?.id_skpd?.split(",");
      if (result.length === 1) {
        idSkpd = `^${result[0]}`;
      } else {
        idSkpd = result.map((d) => `^${d}`).join("|");
      }
    }

    if (has(req.query, "id_jabatan")) {
      joinRelated.push("jabatan");
      const { id_jabatan } = req.query;
      const a = id_jabatan
        .split(",")
        .map((b) => `^${b}`)
        .join("|");
      console.log(a);
      query = query
        .andWhere("jabatan.aktif", "Y")
        .andWhere("jabatan.id_jabatan", "regexp", a);
    }

    if (has(req.query, "id_jenjang")) {
      joinRelated.push("pendidikan");
      const { id_jenjang } = req.query;
      query = query
        .andWhere("pendidikan.aktif", "Y")
        .whereIn("pendidikan.id_jenjang", id_jenjang.split(","));
    }

    if (has(req.query, "id_jenjang") || has(req.query, "id_jabatan")) {
      query = query.joinRelated(`[${joinRelated.join(",")}]`);
    }

    if (has(req.query, "offset")) {
      offset = req.query?.offset;
    }

    query = query.andWhere("id_skpd", "regexp", `${idSkpd}`);

    const dataWithLimit = await query
      .clone()
      .orderByRaw(`CASE WHEN thn_lahir IS NULL THEN 1 ELSE 0 END, thn_lahir`)
      .offset(offset)
      .limit(20);

    const uniqSkpd = uniqBy(dataWithLimit, "id_skpd").map((e) =>
      detailSkpd(e.id_skpd)
    );

    const hasil = await Promise.all(uniqSkpd);
    const hasilGroupby = groupBy(hasil, "data_id_skpd");
    const data = dataWithLimit.map((d) => ({
      ...d,
      pendidikan:
        d.pendidikan.length === 0 ? [] : pendidikanSerialize(d.pendidikan),
      detailSkpd: hasilGroupby[d.id_skpd],
    }));

    const totalData = await query.clone().countDistinct("ptt_biodata.id_ptt");

    const total = totalData[0]["count(distinct `ptt_biodata`.`id_ptt`)"];

    res.status(200).json({
      total,
      data,
    });
  } catch (error) {
    console.log(error);
    res.status(400).json({ code: 400, message: "Internal Server Error" });
  }
};

const detail = async (req, res) => {
  const { pttId } = req.query;
  try {
    const result = await model
      .query()
      .where("id_ptt", pttId)
      .modify("simple")
      .withGraphFetched("[skpd(simple), agama,  statusKawin]")
      .andWhere("aktif", "Y")
      .andWhere("blokir", "N")
      .first();
    const { alamat } = result;

    // foto
    const uid = result?.id_ptt;
    const name = result?.foto;
    const status = "done";
    const url = `https://iput.online:9000/pttpk/foto/${result?.foto}`;
    const fileList = [{ uid, name, status, url }];

    const data = {
      ...result,
      ...alamatSerialize(alamat),
      fileList,
    };

    res.status(200).json(data);
  } catch (error) {
    res.status(400).json({ code: 400, message: "Internal Server Error" });
  }
};

// fucking create new emplooyee its just fucking admin motherfucker
const post = async (req, res) => {
  const { session, body } = req;
  const isAdmin = session?.user?.level === "admin";
  try {
    if (!isAdmin) {
      res.status(403).json({ code: 403, message: "Forbidden" });
    } else {
      const { niptt } = body;

      const employee = await model
        .query()
        .where("niptt", niptt?.trim())
        .andWhere("blokir", "Y")
        .andWhere("aktif", "N")
        .select("nama", "niptt")
        .first();

      if (employee) {
        res.status(409).json({
          code: 409,
          message: `NIPTT sudah dipakai oleh ${employee?.nama_lengkap} ${employee?.niptt}`,
        });
      } else {
        await model.query().insert({ ...body, password: md5(niptt) });
        res.status(200).json({ code: 200, message: "Success" });
      }
    }
  } catch (error) {
    console.log(error);
    res.status(400).json({ code: 400, message: "Internal Server Error" });
  }
};

// const patch
const patch = async (req, res) => {
  try {
    let data;
    if (has(req, "file")) {
      data = omit(req.body, ["niptt", "nama"]);
      const filename = await upload(req?.mc, req?.file);
      data = { ...data, foto: filename };
    } else {
      data = omit(req.body, ["foto", "niptt", "nama"]);
    }
    await model.query().patch(data).where("id_ptt", data?.id_ptt);

    res.status(200).json({ code: 200, message: "success" });
  } catch (error) {
    console.log(error);
    res.status(400);
  }
};

const remove = async (req, res) => {};

module.exports = {
  index,
  detail,
  post,
  patch,
  remove,
};
