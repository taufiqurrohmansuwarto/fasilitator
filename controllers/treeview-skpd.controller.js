const arrayToTree = require("array-to-tree");
const model = require("../models/skpd.model");

const index = async (req, res) => {
  try {
    const { session } = req;
    const skpdId = session?.user?.skpd?.id;
    const knex = model.knex();
    const [data] = await knex.raw(
      `
   select id, id as 'value', id as 'key', name as 'label', name as 'title', pId, name, count(id) as 'total'
from skpd
         left join ptt_biodata on ptt_biodata.id_skpd like concat(skpd.id, '%')
where (pId = ?
   or id = ?)
    and ptt_biodata.aktif = 'Y'
    and ptt_biodata.blokir = 'N'
group by skpd.id
order by id
 
    `,
      [skpdId, skpdId]
    );

    const results = arrayToTree(
      data?.map((d) => ({
        ...d,
        isLeaf: false,
        title: `${d?.title} - (${d?.total})`,
      })),
      { parentProperty: "pId", customID: "id" }
    );

    res.status(200).json({ items: results });
  } catch (error) {
    res.status(400).json({ code: 400, message: "Internal Server Error" });
  }
};

const detail = async (req, res) => {
  try {
    const { skpdId } = req.query;
    const knex = model.knex();
    const [data] = await knex.raw(
      `
select *
from (select id,
             pId,
             id                                                  as 'value',
             id                                                  as 'key',
             name                                                as 'label',
             name                                                as 'title',
             name,
             count(ptt_biodata.id_skpd)                          as 'total',
             exists(select id from skpd s where skpd.id = s.pId) as 'hasLeaf'
      from skpd
               left join ptt_biodata on ptt_biodata.id_skpd like concat(skpd.id, '%')
      where pId = ?
         or id = ?
          and ptt_biodata.aktif = 'Y'
          and ptt_biodata.blokir = 'N'
      group by (skpd.id)
      order by 1) result
where result.id != ?

 
    `,
      [skpdId, skpdId, skpdId]
    );

    const results = {
      id: skpdId,
      results: arrayToTree(
        data?.map((d) => ({
          ...d,
          isLeaf: d?.hasLeaf ? false : true,
          title: `${d?.title} - (${d?.total})`,
        })),
        { parentProperty: "pId", customID: "id" }
      ),
    };

    res.status(200).json(results);
  } catch (error) {
    console.log(error);
    res.status(400).json({ code: 400, message: "Internal Server Error" });
  }
};

module.exports = {
  index,
  detail,
};
