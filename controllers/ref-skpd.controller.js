import detailSkpd from "../lib/detail-skpd";
const model = require("../models/skpd.model");
const arrayToTree = require("array-to-tree");
const biodataModal = require("../models/ptt-biodata.models");

function pad(num, size) {
  num = num.toString();
  while (num.length < size) num = "0" + num;
  return num;
}

const index = async (req, res) => {
  const { session } = req;
  try {
    const idSkpd = session?.user?.skpd?.id;
    const result = await model
      .query()
      .select({
        title: "name",
        label: "name",
        id: "id",
        key: "id",
        pId: "pId",
        value: "id",
      })
      .where("id", "like", `${idSkpd}%`);

    const data = arrayToTree(result, { parentProperty: "pId", customID: "id" });
    res.status(200).json(data);
  } catch (error) {
    res.status(400).json({ code: 400, message: "Internal Server Error" });
  }
};

// admin
const detail = async (req, res) => {
  const { skpdId } = req?.query;
  try {
    const result = await detailSkpd(skpdId);
    const [totalEmployee] = await biodataModal
      .query()
      .where("id_skpd", "like", `${skpdId}%`)
      .andWhere("aktif", "Y")
      .andWhere("blokir", "N")
      .count("id_ptt")
      .as("totalEmployee");

    res
      .status(200)
      .json({ ...result, totalEmployee: totalEmployee?.["count(`id_ptt`)"] });
  } catch (error) {
    console.log(error);
    res.status(400).json({ code: 400, message: "Internal Server Error" });
  }
};

// admin
const post = async (req, res) => {
  const { skpdId } = req?.query;
  const { body } = req;
  try {
    let data;
    const lastChildrenSkpd = await model
      .query()
      .where("pId", skpdId)
      .orderBy("id", "DESC")
      .first();

    if (!lastChildrenSkpd) {
      data = { pId: skpdId, id: `${skpdId}01` };
    } else {
      data = { pId: skpdId, id: `${parseInt(lastChildrenSkpd?.id, 10) + 1}` };
    }
    await model.query().insert({ ...body, ...data });
    res.status(200).json({ code: 200, message: "success" });
  } catch (error) {
    res.status(400).json({ code: 400, message: "Internal Server Error" });
  }
};

//  admin
const patch = async (req, res) => {
  const { skpdId } = req?.query;
  const {
    body: { name },
  } = req;
  try {
    await model
      .query()
      .patch({ name: name?.trim()?.toUpperCase() })
      .where("id", skpdId);

    res.status(200).json({ code: 200, message: "success" });
  } catch (error) {
    console.log(error);
    res.status(400).json({ code: 400, message: "Internal Server Error" });
  }
};

const remove = async (req, res) => {
  const { skpdId } = req?.query;
  try {
    const hasChildren = await model.query().where("pId", skpdId);
    if (hasChildren?.length !== 0) {
      res.status(422).json({
        code: 422,
        message:
          "Tidak dapat menghapus data! Pilih Satuan Kerja yang paling kecil!",
      });
    } else {
      await model.query().deleteById(skpdId);
      res.status(200).json({ code: 200, message: "Success" });
    }
  } catch (error) {
    res.status(400).json({ code: 400, message: "Internal Server Error" });
  }
};

export default {
  index,
  patch,
  detail,
  post,
  patch,
  remove,
};
