const index = async (req, res) => {};
const detail = async (req, res) => {};
const patch = async (req, res) => {};
const remove = async (req, res) => {};

export default {
  index,
  patch,
  detail,
  remove,
};
