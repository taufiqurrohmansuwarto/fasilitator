import axios from "../lib/axios";
export const fetchPttPendidikan = async (id) => {
  return axios.get(`/ptt-biodata/${id}/pendidikan`).then((res) => res?.data);
};

export const updatePttPendiidkan = async ({ pttId, data, id }) => {
  return axios
    .patch(`/ptt-biodata/${pttId}/pendidikan/${id}`, data, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    })
    .then((res) => res?.data);
};

export const createPttPendidikan = async ({ pttId, data }) => {
  return axios
    .post(`/ptt-biodata/${pttId}/pendidikan`, data, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    })
    .then((res) => res?.data);
};

export const removePttPendidikan = async ({ pttId, id }) => {
  return axios
    .delete(`/ptt-biodata/${pttId}/pendidikan/${id}`)
    .then((res) => res?.data);
};
