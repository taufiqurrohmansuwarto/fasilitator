import axios from "../lib/axios";

export const fetchLogUsers = async () => {
  return axios.get("/log-users").then((res) => res?.data);
};
