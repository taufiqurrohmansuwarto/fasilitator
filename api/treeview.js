import axios from "../lib/axios";

export const fetchTreeviewSkpd = async () => {
  return axios.get("/treeview-skpd").then((res) => res?.data);
};

export const fetchChildrenSkpd = async (id) => {
  return axios.get(`/treeview-skpd/${id}`).then((res) => res.data);
};
