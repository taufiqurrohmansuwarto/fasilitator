import queryString from "query-string";
import axios from "../lib/axios";

export const fetchUsers = async (data) => {
  const query = queryString.stringify(data, {
    skipNull: true,
    skipEmptyString: true,
  });

  return axios.get(`/users?${query}`).then((res) => res?.data);
};

export const createUser = async (data) => {
  return axios.post("/users", data).then((res) => res?.data);
};

export const fetchUser = async (id) => {
  return axios.get(`/users/${id}`).then((res) => res?.data);
};

export const updateUser = async ({ id, data }) => {
  return axios.patch(`/users/${id}`, data).then((res) => res?.data);
};

export const removeUser = async (id) => {
  return axios.patch(`/users/${id}`).then((res) => res?.data);
};
