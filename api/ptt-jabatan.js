import axios from "../lib/axios";

export const fetchPttJabatan = async (id) => {
  return axios.get(`/ptt-biodata/${id}/jabatan`).then((res) => res?.data);
};

export const updatePttJabatan = async ({ pttId, data, id }) => {
  return axios
    .patch(`/ptt-biodata/${pttId}/jabatan/${id}`, data, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    })
    .then((res) => res?.data);
};

export const createPttJabatan = async ({ pttId, data }) => {
  return axios
    .post(`/ptt-biodata/${pttId}/jabatan`, data, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    })
    .then((res) => res?.data);
};

export const removePttJabatan = async ({ pttId, id }) => {
  return axios
    .delete(`/ptt-biodata/${pttId}/jabatan/${id}`)
    .then((res) => res?.data);
};
