import axios from "../lib/axios";
export const fetchPttBpjs = async (id) => {
  return axios.get(`/ptt-biodata/${id}/bpjs`).then((res) => res?.data);
};

export const updatePttBpjs = async ({ pttId, id, data }) => {
  return axios.patch(`/ptt-biodata/${pttId}/bpjs/${id}`, data, {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  });
};

export const detailPttBpjs = async (id) => {};

export const createPttBpjs = async ({ pttId, data }) => {
  return axios.post(`/ptt-biodata/${pttId}/bpjs`, data, {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  });
};

export const removePttBpjs = async ({ pttId, id }) => {
  return axios
    .delete(`/ptt-biodata/${pttId}/bpjs/${id}`)
    .then((res) => res?.data);
};
