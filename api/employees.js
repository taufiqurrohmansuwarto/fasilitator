import queryString from "query-string";
import axios from "../lib/axios";

export const createEmployee = async (data) => {
  return axios.post("/ptt-biodata", data);
};

export const fetchEmployees = async (data = {}) => {
  const query = queryString.stringify(data, {
    skipEmptyString: true,
    skipNull: true,
    arrayFormat: "comma",
  });
  return axios.get(`/ptt-biodata?${query}`).then((res) => res?.data);
};

export const patchEmployee = async ({ id_ptt, data }) => {
  return axios
    .patch(`/ptt-biodata/${id_ptt}/biodata`, data, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    })
    .then((res) => res?.data);
};

export const detailEmployee = async (id) => {
  return axios.get(`/ptt-biodata/${id}/biodata`).then((res) => res?.data);
};
