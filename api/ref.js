import querystring from "query-string";
import axios from "../lib/axios";

export const fetchSkpds = async () => {
  return axios.get("/skpd").then((res) => res.data);
};

export const searchUsers = async (
  query = {
    type: "users",
  }
) => {
  const currentQuery = query;
  const myQuery = querystring.stringify(currentQuery);
  console.log(myQuery);
};

export const refAgama = async () => {
  return axios.get("/ref/agama").then((res) => res?.data);
};

export const refJenjang = async () => {
  return axios.get("/ref/jenjang").then((res) => res?.data);
};

// jabatan
export const refJabatan = async () => {
  return axios.get("/ref/jabatan").then((res) => res?.data);
};

export const detailJabatan = async (id) => {
  return axios.get(`/ref/jabatan/${id}`).then((res) => res?.data);
};

export const removeJabatan = async (id) => {
  return axios.delete(`/ref/jabatan/${id}`).then((res) => res?.data);
};

export const updateJabatan = async ({ id, data }) => {
  return axios.patch(`/ref/jabatan/${id}`, data).then((res) => res?.data);
};

export const createJabatan = async ({ id, data }) => {
  return axios.post(`/ref/jabatan/${id}`, data).then((res) => res?.data);
};

export const refPekerjaan = async () => {
  return axios.get("/ref/pekerjaan").then((res) => res?.data);
};

export const refPekerjaanAnak = async () => {
  return axios.get("/ref/pekerjaan-anak").then((res) => res?.data);
};

export const refStatusAnak = async () => {
  return axios.get("/ref/status-anak").then((res) => res?.data);
};

export const refStatusKawin = async () => {
  return axios.get("/ref/status-kawin").then((res) => res.data);
};

export const refStatusSuamiIstri = async () => {
  return axios.get("/ref/status-suami-istri").then((res) => res.data);
};

// skpd api
export const refSkpd = async () => {
  return axios.get("/ref/skpd").then((res) => res?.data);
};

export const detailSkpd = async (id) => {
  return axios.get(`/ref/skpd/${id}`).then((res) => res?.data);
};

export const updateSkpd = async ({ id, name }) => {
  return axios.patch(`/ref/skpd/${id}`, { name }).then((res) => res?.data);
};

export const removeSkpd = async (id) => {
  return axios.delete(`/ref/skpd/${id}`).then((res) => res?.data);
};

export const createSkpd = async ({ id, data }) => {
  return axios.post(`/ref/skpd/${id}`, data).then((res) => res?.data);
};
