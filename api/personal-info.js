import axios from "../lib/axios";
const url = "/ptt-biodata";

export const fetchBiodata = async (id) => {
  return axios.get(`${url}/${id}/biodata`).then((res) => res?.data);
};
