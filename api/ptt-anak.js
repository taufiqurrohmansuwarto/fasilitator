import axios from "../lib/axios";

export const fetchPttAnak = async (id) => {
  return axios.get(`/ptt-biodata/${id}/anak`).then((res) => res?.data);
};

export const updatePttAnak = async ({ pttId, data, id }) => {
  return axios
    .patch(`/ptt-biodata/${pttId}/anak/${id}`, data)
    .then((res) => res?.data);
};

export const createPttAnak = async ({ pttId, data }) => {
  return axios
    .post(`/ptt-biodata/${pttId}/anak`, data)
    .then((res) => res?.data);
};

export const removePttAnak = async ({ pttId, id }) => {
  return axios.delete(`/ptt-biodata/${pttId}/anak/${id}`);
};
