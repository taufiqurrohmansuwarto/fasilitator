import axios from "../lib/axios";

export const fetchPttSuamiIstri = async (id) => {
  return axios.get(`/ptt-biodata/${id}/suami-istri`).then((res) => res?.data);
};

export const updatePttSuamiIstri = async ({ pttId, data, id }) => {
  return axios
    .patch(`/ptt-biodata/${pttId}/suami-istri/${id}`, data)
    .then((res) => res?.data);
};

export const createPttSuamiIstri = async ({ pttId, data }) => {
  return axios
    .post(`/ptt-biodata/${pttId}/suami-istri`, data)
    .then((res) => res?.data);
};

export const removePttSuamiIstri = async ({ pttId, id }) => {
  return axios.delete(`/ptt-biodata/${pttId}/suami-istri/${id}`);
};
