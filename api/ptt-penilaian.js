import axios from "../lib/axios";

export const fetchPttPenilaian = async (id) => {
  return axios.get(`/ptt-biodata/${id}/penilaian`).then((res) => res?.data);
};

export const updatePttPenilaian = async ({ pttId, id, data }) => {
  return axios
    .patch(`/ptt-biodata/${pttId}/penilaian/${id}`, data, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    })
    .then((res) => res?.data);
};

export const createPttPenilaian = async ({ pttId, data }) => {
  return axios
    .post(`/ptt-biodata/${pttId}/penilaian`, data, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    })
    .then((res) => res?.data);
};

export const removePttPenilaian = async ({ pttId, id }) => {
  return axios
    .delete(`/ptt-biodata/${pttId}/penilaian/${id}`)
    .then((res) => res?.data);
};
