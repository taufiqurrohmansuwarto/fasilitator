import ProForm, {
  ProFormDigit,
  ProFormUploadButton,
} from "@ant-design/pro-form";
import { Button, message } from "antd";
import omit from "lodash/omit";
import dynamic from "next/dynamic";
import { useRef, useState } from "react";
import { useMutation, useQueryClient } from "react-query";
import { createPttBpjs } from "../api/ptt-bpjs";

const ModalForm = dynamic(
  () => import("@ant-design/pro-form").then((mod) => mod.ModalForm),
  { ssr: false }
);

function CreateBpjs({ pttId }) {
  const [show, setShow] = useState(false);

  const ref = useRef();
  const client = useQueryClient();
  const mutation = useMutation((data) => createPttBpjs(data), {
    onSettled: () => {
      client.invalidateQueries(["ptt-bpjs", pttId]);
    },
    onSuccess: () => {
      ref.current?.resetFields();
      setShow(false);
      message.success("Data berhasil ditambahkan");
    },
  });

  return (
    <ModalForm
      formRef={ref}
      visible={show}
      onVisibleChange={(visible) => setShow(visible)}
      onFinish={async (values) => {
        let data;

        const current = omit(values, "fileList");
        const [file] = values?.fileList;
        const fileBpjs = file?.originFileObj;
        data = { ...current, bpjs: fileBpjs };
        const formData = new FormData();
        Object.keys(data).forEach((d) => {
          formData.append(d, data[d]);
        });
        await mutation.mutateAsync({ pttId, data: formData });
      }}
      title="Tambah BPJS"
      trigger={
        <Button type="primary" onClick={() => setShow(true)}>
          Tambah
        </Button>
      }
    >
      <ProForm.Group>
        <ProFormUploadButton
          max={1}
          name="fileList"
          label="Upload"
          title="Upload BPJS"
          placeholder="Upload"
          fieldProps={{
            multiple: false,
            accept: [".jpg", ".png"],
          }}
          extra="File BPJS"
          rules={[{ required: true, message: "Tidak boleh kosong" }]}
        />
      </ProForm.Group>
      <ProForm.Group>
        <ProFormDigit
          name="no_bpjs"
          label="No. BPJS"
          rules={[{ required: true, message: "Tidak boleh kosong" }]}
        />
      </ProForm.Group>
      <ProForm.Group>
        <ProFormDigit
          name="kelas"
          label="Kelas"
          rules={[{ required: true, message: "Tidak boleh kosong" }]}
        />
      </ProForm.Group>
    </ModalForm>
  );
}

export default CreateBpjs;
