import ProForm, {
  ProFormDatePicker,
  ProFormSelect,
  ProFormText,
} from "@ant-design/pro-form";
import { message } from "antd";
import moment from "moment";
import dynamic from "next/dynamic";
import { useEffect, useRef, useState } from "react";
import { useMutation, useQueryClient } from "react-query";
import { updatePttAnak } from "../api/ptt-anak";

const ModalForm = dynamic(
  () => import("@ant-design/pro-form").then((mod) => mod.ModalForm),
  { ssr: false }
);

function UpdateAnak({
  refStatusAnak,
  refPekerjaanAnak,
  refSuamiIstri,
  pttId,
  initialValues,
}) {
  const [show, setShow] = useState(false);
  const { anak_id: id } = initialValues;

  const client = useQueryClient();
  const mutation = useMutation((data) => updatePttAnak(data), {
    onSettled: () => {
      client.invalidateQueries(["rwyt-anak", pttId]);
    },
    onSuccess: () => {
      setShow(false);
      ref?.current?.resetFields();
      message.success("Update Anak Berhasil");
    },
  });

  const ref = useRef();
  useEffect(() => {}, [initialValues]);

  return (
    <ModalForm
      formRef={ref}
      visible={show}
      onVisibleChange={(visible) => {
        if (visible) {
          ref?.current?.setFieldsValue({
            ...initialValues,
            tgl_lahir: moment(initialValues?.tgl_lahir),
          });
        }
        setShow(visible);
      }}
      onFinish={async (values) => {
        const data = { data: values, pttId, id };
        await mutation.mutateAsync(data);
      }}
      title="Update Anak"
      trigger={<a>Edit</a>}
    >
      <ProForm.Group>
        <ProFormSelect
          name="suami_istri_id"
          width="md"
          label="Nama Orang Tua Ayah / Ibu"
          options={refSuamiIstri}
          fieldProps={{
            showSearch: true,
            optionFilterProp: "label",
          }}
          initialValue={initialValues?.suami_istri_id}
        />
        <ProFormText
          name="nama"
          width="md"
          label="Nama Lengkap Anak"
          rules={[{ required: true, message: "tidak boleh kosong" }]}
          initialValue={initialValues?.nama}
        />
      </ProForm.Group>

      <ProForm.Group>
        <ProFormText
          name="tempat_lahir"
          width="md"
          label="Tempat Lahir"
          rules={[{ required: true, message: "tidak boleh kosong" }]}
          initialValue={initialValues?.tempat_lahir}
        />

        <ProFormDatePicker
          name="tgl_lahir"
          width="md"
          label="Tanggal Lahir"
          rules={[{ required: true, message: "tidak boleh kosong" }]}
          fieldProps={{
            format: "YYYY-MM-DD",
          }}
          initialValue={moment(initialValues?.tgl_lahir)}
        />
      </ProForm.Group>

      <ProForm.Group>
        <ProFormSelect
          name="status_anak_id"
          width="md"
          label="Status Anak"
          options={refStatusAnak}
          fieldProps={{
            showSearch: true,
            optionFilterProp: "label",
          }}
          rules={[{ required: true, message: "tidak boleh kosong" }]}
          initialValue={initialValues?.status_anak_id}
        />
        <ProFormSelect
          name="pekerjaan_anak_id"
          width="md"
          label="Pekerjaan Anak"
          options={refPekerjaanAnak}
          fieldProps={{
            showSearch: true,
            optionFilterProp: "label",
          }}
          rules={[{ required: true, message: "tidak boleh kosong" }]}
          initialValue={initialValues?.pekerjaan_anak_id}
        />
      </ProForm.Group>
    </ModalForm>
  );
}

export default UpdateAnak;
