import ProForm, {
  ProFormDatePicker,
  ProFormDigit,
  ProFormSelect,
  ProFormUploadButton,
} from "@ant-design/pro-form";
import { message } from "antd";
import isUndefined from "lodash/isUndefined";
import omit from "lodash/omit";
import omitBy from "lodash/omitBy";
import moment from "moment";
import dynamic from "next/dynamic";
import { useEffect, useRef, useState } from "react";
import { useMutation, useQueryClient } from "react-query";
import { updatePttPenilaian } from "../api/ptt-penilaian";

const ModalForm = dynamic(
  () => import("@ant-design/pro-form").then((mod) => mod.ModalForm),
  { ssr: false }
);

function UpdatePenilaian({ pttId, initialValues }) {
  const [show, setShow] = useState(false);
  const { id_ptt_penilaian: id } = initialValues;

  useEffect(() => {}, [initialValues]);

  const ref = useRef();
  const client = useQueryClient();
  const mutation = useMutation((data) => updatePttPenilaian(data), {
    onSettled: () => {
      client.invalidateQueries(["ptt-penilaian", pttId]);
    },
    onSuccess: () => {
      ref.current?.resetFields();
      setShow(false);
      message.success("Data berhasil ditambahkan");
    },
  });

  return (
    <ModalForm
      formRef={ref}
      visible={show}
      onVisibleChange={(visible) => {
        if (visible) {
          ref.current?.setFieldsValue({
            ...initialValues,
            tahun: moment(initialValues?.tahun, "YYYY").valueOf(),
          });
        }
        setShow(visible);
      }}
      onFinish={async (values) => {
        let data;

        const current = omit(values, "fileList");
        const [file] = values?.fileList;
        const fileBpjs = file?.originFileObj;
        data = omitBy(
          {
            ...current,
            penilaian: fileBpjs,
            tahun: moment(values?.tahun).format("YYYY"),
          },
          isUndefined
        );

        const formData = new FormData();
        Object.keys(data).forEach((d) => {
          formData.append(d, data[d]);
        });
        await mutation.mutateAsync({ pttId, data: formData, id });
      }}
      title="Edit Penilaian"
      trigger={<a onClick={() => setShow(true)}>Edit</a>}
    >
      <ProForm.Group>
        <ProFormUploadButton
          max={1}
          name="fileList"
          label="Upload"
          title="Upload Penilaian"
          placeholder="Upload"
          fieldProps={{
            multiple: false,
            accept: [".pdf"],
          }}
          extra="File Penilaian"
          rules={[{ required: true, message: "Tidak boleh kosong" }]}
          initialValue={initialValues?.fileList}
        />
      </ProForm.Group>
      <ProForm.Group>
        <ProFormDatePicker.Year
          name="tahun"
          label="Tahun Penilaian"
          rules={[{ required: true, message: "Tidak boleh kosong" }]}
          initialValue={initialValues?.tahun}
        />
        <ProFormDigit
          fieldProps={{
            stringMode: true,
          }}
          name="nilai"
          label="Nilai"
          rules={[{ required: true, message: "Tidak boleh kosong" }]}
          initialValue={initialValues?.nilai}
        />
      </ProForm.Group>
      <ProForm.Group>
        <ProFormSelect
          options={[
            { value: "Dilanjutkan", label: "Dilanjutkan" },
            { value: "Tidak Dilanjutkan", label: "Tidak Dilanjutkan" },
          ]}
          width="md"
          name="rekomendasi"
          label="Rekomendasi"
          rules={[{ required: true, message: "Harus diisi" }]}
          initialValue={initialValues?.rekomendasi}
        />
      </ProForm.Group>
    </ModalForm>
  );
}

export default UpdatePenilaian;
