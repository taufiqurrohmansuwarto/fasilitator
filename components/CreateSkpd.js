import ProForm, { ProFormText } from "@ant-design/pro-form";
import { Button, Divider } from "antd";
import dynamic from "next/dynamic";
import { useRef, useState } from "react";
import { useMutation, useQueryClient } from "react-query";
import { createSkpd } from "../api/ref";

const DrawerForm = dynamic(() =>
  import("@ant-design/pro-form").then((mod) => mod?.DrawerForm)
);

const CreateSkpd = ({ skpd, id }) => {
  const ref = useRef();
  const [show, setShow] = useState();

  const client = useQueryClient();
  const createMutation = useMutation((data) => createSkpd(data), {
    onSettled: () => {
      client.invalidateQueries("ref-skpd");
    },
    onSuccess: () => {
      ref.current?.resetFields();
      setShow(false);
    },
  });

  return (
    <DrawerForm
      formRef={ref}
      visible={show}
      onVisibleChange={(visible) => setShow(visible)}
      onFinish={async (values) => {
        const data = { name: values?.name };
        await createMutation.mutateAsync({ id, data });
      }}
      title="Tambah Tempat Kerja"
      trigger={<Button>Tambah</Button>}
      submitter={{
        searchConfig: {
          resetText: "Reset",
          submitText: "Submit",
        },
        resetButtonProps: {
          style: {
            display: "none",
          },
        },
      }}
    >
      <div>{skpd}</div>
      <Divider />
      <ProForm.Group>
        <ProFormText name="name" placeholder="Nama Tempat Kerja" width="lg" />
      </ProForm.Group>
    </DrawerForm>
  );
};

export default CreateSkpd;
