import ProForm, {
  ProFormDatePicker,
  ProFormDigit,
  ProFormSelect,
  ProFormUploadButton,
} from "@ant-design/pro-form";
import { Button, message } from "antd";
import omit from "lodash/omit";
import dynamic from "next/dynamic";
import { useRef, useState } from "react";
import { useMutation, useQueryClient } from "react-query";
import { createPttPenilaian } from "../api/ptt-penilaian";

const ModalForm = dynamic(
  () => import("@ant-design/pro-form").then((mod) => mod.ModalForm),
  { ssr: false }
);

function CreatePenilaian({ pttId }) {
  const [show, setShow] = useState(false);

  const ref = useRef();
  const client = useQueryClient();
  const mutation = useMutation((data) => createPttPenilaian(data), {
    onSettled: () => {
      client.invalidateQueries(["ptt-penilaian", pttId]);
    },
    onSuccess: () => {
      ref.current?.resetFields();
      setShow(false);
      message.success("Data berhasil ditambahkan");
    },
  });

  return (
    <ModalForm
      formRef={ref}
      visible={show}
      onVisibleChange={(visible) => setShow(visible)}
      onFinish={async (values) => {
        let data;

        const current = omit(values, "fileList");
        const [file] = values?.fileList;
        const fileBpjs = file?.originFileObj;
        data = { ...current, penilaian: fileBpjs };
        const formData = new FormData();
        Object.keys(data).forEach((d) => {
          formData.append(d, data[d]);
        });
        await mutation.mutateAsync({ pttId, data: formData });
      }}
      title="Tambah Penilaian"
      trigger={
        <Button type="primary" onClick={() => setShow(true)}>
          Tambah
        </Button>
      }
    >
      <ProForm.Group>
        <ProFormUploadButton
          max={1}
          name="fileList"
          label="Upload"
          title="Upload Penilaian"
          placeholder="Upload"
          fieldProps={{
            multiple: false,
            accept: [".pdf"],
          }}
          extra="File Penilaian"
          rules={[{ required: true, message: "Tidak boleh kosong" }]}
        />
      </ProForm.Group>
      <ProForm.Group>
        <ProFormDatePicker.Year
          name="tahun"
          label="Tahun Penilaian"
          rules={[{ required: true, message: "Tidak boleh kosong" }]}
        />
        <ProFormDigit
          fieldProps={{
            stringMode: true,
          }}
          name="nilai"
          label="Nilai"
          rules={[{ required: true, message: "Tidak boleh kosong" }]}
        />
      </ProForm.Group>
      <ProForm.Group>
        <ProFormSelect
          options={[
            { value: "Dilanjutkan", label: "Dilanjutkan" },
            { value: "Tidak Dilanjutkan", label: "Tidak Dilanjutkan" },
          ]}
          width="md"
          name="rekomendasi"
          label="Rekomendasi"
          rules={[{ required: true, message: "Harus diisi" }]}
        />
      </ProForm.Group>
    </ModalForm>
  );
}

export default CreatePenilaian;
