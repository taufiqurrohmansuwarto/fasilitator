import ProForm, {
  ProFormDatePicker,
  ProFormDigit,
  ProFormText,
  ProFormUploadButton,
} from "@ant-design/pro-form";
import { message } from "antd";
import isUndefined from "lodash/isUndefined";
import omit from "lodash/omit";
import omitBy from "lodash/omitBy";
import moment from "moment";
import dynamic from "next/dynamic";
import { useEffect, useRef, useState } from "react";
import { useMutation, useQueryClient } from "react-query";
import { updatePttJabatan } from "../api/ptt-jabatan";
import ProFormTreeJabatan from "./form/ProFormJabatan";

const ModalForm = dynamic(
  () => import("@ant-design/pro-form").then((mod) => mod.DrawerForm),
  { ssr: false }
);

function UpdateJabatan({ pttId, initialValues }) {
  const [show, setShow] = useState(false);
  const { id_ptt_jab: id } = initialValues;

  useEffect(() => {}, [initialValues]);
  const ref = useRef();
  const client = useQueryClient();
  const mutation = useMutation((data) => updatePttJabatan(data), {
    onSettled: () => {
      client.invalidateQueries(["ptt-jabatan", pttId]);
    },
    onSuccess: () => {
      ref.current?.resetFields();
      setShow(false);
      message.success("Data berhasil ditambahkan");
    },
  });

  return (
    <ModalForm
      formRef={ref}
      submitter={{
        searchConfig: {
          resetText: "Reset",
          submitText: "Submit",
        },
        resetButtonProps: {
          style: {
            // Hide the reset button
            display: "none",
          },
        },
      }}
      visible={show}
      onVisibleChange={(visible) => {
        if (visible) {
          ref?.current?.setFieldsValue({
            ...initialValues,
            tgl_akhir: moment(initialValues?.tgl_akhir),
            tgl_mulai: moment(initialValues?.tgl_mulai),
            tgl_surat: moment(initialValues?.tgl_surat),
          });
        }
        setShow(visible);
      }}
      onFinish={async (values) => {
        let data;
        const current = omit(values, "fileList");
        const [file] = values?.fileList;
        const fileBpjs = file?.originFileObj;
        data = omitBy({ ...current, jabatan: fileBpjs }, isUndefined);
        const formData = new FormData();
        Object.keys(data).forEach((d) => {
          formData.append(d, data[d]);
        });
        console.log(data);
        await mutation.mutateAsync({ pttId, data: formData, id });
      }}
      title="Edit Jabatan"
      trigger={<a onClick={() => setShow(true)}>Edit</a>}
    >
      <ProForm.Group>
        <ProFormTreeJabatan
          rules={[{ required: true, message: "Tidak Boleh kosong" }]}
          name="id_jabatan"
          label="Jabatan"
          tooltip="Jabatan"
          placeholder="Jabatan"
        />
      </ProForm.Group>
      <ProForm.Group>
        <ProFormText
          name="no_surat"
          label="Nomor Surat"
          width="md"
          rules={[{ required: true, message: "tidak boleh kosong" }]}
          initialValue={initialValues?.no_surat}
        />
        <ProFormDatePicker
          name="tgl_surat"
          label="Tanggal Surat"
          fieldProps={{
            format: "YYYY-MM-DD",
          }}
          rules={[{ required: true, message: "tidak boleh kosong" }]}
          initialValue={initialValues?.tgl_surat}
        />
      </ProForm.Group>
      <ProForm.Group>
        <ProFormText
          name="pejabat_penetap"
          label="Pejabat Penatap"
          width="md"
          rules={[{ required: true, message: "tidak boleh kosong" }]}
          initialValue={initialValues?.pejabat_penetap}
        />
      </ProForm.Group>
      <ProForm.Group>
        <ProFormDatePicker
          fieldProps={{
            format: "YYYY-MM-DD",
          }}
          name="tgl_mulai"
          label="Tanggal Mulai"
          rules={[{ required: true, message: "tidak boleh kosong" }]}
          initialValue={initialValues?.tgl_mulai}
        />
        <ProFormDatePicker
          fieldProps={{
            format: "YYYY-MM-DD",
          }}
          name="tgl_akhir"
          label="Tanggal Akhir"
          rules={[{ required: true, message: "tidak boleh kosong" }]}
          initialValue={initialValues?.tgl_akhir}
        />
      </ProForm.Group>
      <ProForm.Group>
        <ProFormDigit
          name="gaji"
          label="Gaji"
          width="md"
          initialValue={initialValues?.gaji}
        />
      </ProForm.Group>
      <ProForm.Group>
        <ProFormUploadButton
          max={1}
          name="fileList"
          label="Upload"
          title="Upload SK"
          placeholder="Upload"
          fieldProps={{
            multiple: false,
            accept: [".pdf"],
          }}
          extra="File SK Jabatan"
          rules={[{ required: true, message: "Tidak boleh kosong" }]}
          initialValue={initialValues?.fileList}
        />
      </ProForm.Group>
    </ModalForm>
  );
}

export default UpdateJabatan;
