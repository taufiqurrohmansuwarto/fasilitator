import ProForm, {
  ProFormDatePicker,
  ProFormSelect,
  ProFormText,
} from "@ant-design/pro-form";
import { Button, message } from "antd";
import dynamic from "next/dynamic";
import { useRef, useState } from "react";
import { useMutation, useQueryClient } from "react-query";
import { createPttSuamiIstri } from "../api/ptt-suami-istri";

const ModalForm = dynamic(
  () => import("@ant-design/pro-form").then((mod) => mod.ModalForm),
  { ssr: false }
);

function CreateSuamiIstri({ refStatusPernikahan, refStatusPekerjaan, pttId }) {
  const [show, setShow] = useState(false);

  const client = useQueryClient();
  const mutation = useMutation((data) => createPttSuamiIstri(data), {
    onSettled: () => {
      client.invalidateQueries(["rwyt-suami-istri", pttId]);
    },
    onSuccess: () => {
      setShow(false);
      ref.current?.resetFields();
      message.success("Data Suami/Istri berhasil dibuat");
    },
  });

  const ref = useRef();

  return (
    <ModalForm
      formRef={ref}
      visible={show}
      onVisibleChange={(visible) => setShow(visible)}
      onFinish={async (data) => {
        await mutation.mutateAsync({ pttId, data });
      }}
      title="Tambah Suami/Istri"
      trigger={
        <Button type="primary" onClick={() => setShow(true)}>
          Tambah
        </Button>
      }
    >
      <ProForm.Group>
        <ProFormText
          name="nama_suami_istri"
          width="md"
          label="Nama Lengkap Suami / Istri"
          rules={[{ required: true, message: "tidak boleh kosong" }]}
        />
      </ProForm.Group>
      <ProForm.Group>
        <ProFormSelect
          name="status_suami_istri_id"
          width="md"
          label="Status Dalam Pernikahan"
          options={refStatusPernikahan}
          fieldProps={{
            showSearch: true,
            optionFilterProp: "label",
          }}
          rules={[{ required: true, message: "tidak boleh kosong" }]}
        />
      </ProForm.Group>
      <ProForm.Group>
        <ProFormText
          name="tempat_lahir"
          width="md"
          label="Tempat Lahir"
          rules={[{ required: true, message: "tidak boleh kosong" }]}
        />
        <ProFormDatePicker
          name="tgl_lahir"
          width="md"
          label="Tanggal Lahir"
          rules={[{ required: true, message: "tidak boleh kosong" }]}
          fieldProps={{
            format: "YYYY-MM-DD",
          }}
        />
      </ProForm.Group>
      <ProForm.Group>
        <ProFormText
          name="instansi"
          width="md"
          label="Instansi/Tempat Bekerja"
          rules={[{ required: true, message: "tidak boleh kosong" }]}
        />

        <ProFormSelect
          name="pekerjaan_id"
          width="md"
          label="Status Pekerjaan"
          options={refStatusPekerjaan}
          fieldProps={{
            showSearch: true,
            optionFilterProp: "label",
          }}
          rules={[{ required: true, message: "tidak boleh kosong" }]}
        />
      </ProForm.Group>
    </ModalForm>
  );
}

export default CreateSuamiIstri;
