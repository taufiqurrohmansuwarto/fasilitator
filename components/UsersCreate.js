import ProForm, { ProFormRadio, ProFormText } from "@ant-design/pro-form";
import { Button, message } from "antd";
import dynamic from "next/dynamic";
import React, { useState } from "react";
import { useMutation } from "react-query";
import { createUser } from "../api/users";
import ProFormTree from "./form/ProFormTree";

const ModalForm = dynamic(
  () => import("@ant-design/pro-form").then((mod) => mod.ModalForm),
  { ssr: false }
);

function UsersCreate({ reload }) {
  const mutation = useMutation((data) => createUser(data), {
    onSuccess: () => {
      message.success("Berhasil ditambahkan");
      setShow(false);
      reload();
    },
    onError: (error) => {
      message.error(error);
    },
  });
  const [show, setShow] = useState(false);

  return (
    <ModalForm
      visible={show}
      onVisibleChange={(visible) => {
        setShow(visible);
      }}
      title="Tambah Operator"
      onFinish={async (values) => {
        mutation.mutate(values);
      }}
      trigger={<Button onClick={() => setShow(true)}>Operator</Button>}
    >
      <ProForm.Group>
        <ProFormText
          name="username"
          width="md"
          label="Username"
          tooltip="Username"
          placeholder="Username"
          rules={[{ required: true, message: "Username tidak boleh kosong" }]}
        />
      </ProForm.Group>
      <ProForm.Group>
        <ProFormText.Password
          name="password"
          width="md"
          label="Password"
          tooltip="Password"
          placeholder="Password"
          rules={[{ required: true, message: "Password tidak boleh kosong" }]}
        />
      </ProForm.Group>
      <ProForm.Group>
        <ProFormText
          name="nama_lengkap"
          width="sm"
          label="Nama Lengkap"
          tooltip="Nama Lengkap"
          placeholder="Nama Lengkap"
          rules={[
            { required: true, message: "Nama Lengkap tidak boleh kosong" },
          ]}
        />
        <ProFormText
          name="email"
          width="sm"
          label="Email"
          tooltip="Email"
          placeholder="Email"
        />
        <ProFormText
          name="no_telp"
          width="sm"
          label="No. Telepon"
          tooltip="No. Telepon"
          placeholder="No. Telepon"
        />
      </ProForm.Group>
      <ProForm.Group>
        <ProFormTree
          width="xl"
          rules={[{ required: true, message: "Tidak Boleh kosong" }]}
          name="id_skpd"
          label="Perangkat Daerah"
          tooltip="Perangkat Daerah"
          placeholder="Perangkat Daerah"
        />
      </ProForm.Group>
      <ProForm.Group>
        <ProFormRadio.Group
          initialValue={"N"}
          name="blokir"
          label="Status"
          options={[
            { label: "Blokir", value: "Y" },
            { label: "Aktif", value: "N" },
          ]}
        />
      </ProForm.Group>
      <ProForm.Group>
        <ProFormRadio.Group
          initialValue={"user"}
          name="level"
          label="Level"
          options={[
            { label: "Admin", value: "admin" },
            { label: "User", value: "user" },
          ]}
        />
      </ProForm.Group>
    </ModalForm>
  );
}

export default UsersCreate;
