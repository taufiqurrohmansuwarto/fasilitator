import ProForm, {
    ProFormDatePicker,
    ProFormSelect,
    ProFormText,
  } from "@ant-design/pro-form";
  import { Button, message } from "antd";
  import dynamic from "next/dynamic";
  import { useRef, useState } from "react";
  import { useMutation, useQueryClient } from "react-query";
  
  const ModalForm = dynamic(
    () => import("@ant-design/pro-form").then((mod) => mod.ModalForm),
    { ssr: false }
  );

  function NonAktivasiPttpk({pttId}) {
    const [show, setShow] = useState(false);
    const client = useQueryClient();
    const mutation = useMutation((data) => NonAktivasiPttpk(data), {
      onSettled: () => {
        client.invalidateQueries(["rwyt-anak", pttId]);
      },
      onSuccess: () => {
        ref.current?.resetFields();
        setShow(false);
        message.success("Berhasil ditambahkan");
      },
    });

    const ref = useRef();
  
    return (
      <ModalForm
        formRef={ref}
        visible={show}
        onVisibleChange={(visible) => setShow(visible)}
        onFinish={async (data) => {
          const currentData = { pttId, data };
          mutation.mutateAsync(currentData);
        }}
        title="Non Aktivasi PTTPK"
        trigger={
          <Button type="primary" onClick={() => setShow(true)}>
            Non Aktifkan
          </Button>
        }
      >
        <ProForm.Group>
          <ProFormText
            name="nama"
            width="md"
            label="Contoh Nama"
            rules={[{ required: true, message: "tidak boleh kosong" }]}
          />
        </ProForm.Group>
        
      </ModalForm>
    );
  }
  
  export default NonAktivasiPttpk;
  