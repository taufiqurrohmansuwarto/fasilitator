import dynamic from "next/dynamic";

export const PageContainer = dynamic(
  import("@ant-design/pro-layout").then((r) => r?.PageContainer),
  {
    ssr: false,
  }
);
