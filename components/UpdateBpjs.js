import ProForm, {
  ProFormDigit,
  ProFormUploadButton,
} from "@ant-design/pro-form";
import { message } from "antd";
import isUndefined from "lodash/isUndefined";
import omit from "lodash/omit";
import omitBy from "lodash/omitBy";
import dynamic from "next/dynamic";
import { useEffect, useRef, useState } from "react";
import { useMutation, useQueryClient } from "react-query";
import { updatePttBpjs } from "../api/ptt-bpjs";

const ModalForm = dynamic(
  () => import("@ant-design/pro-form").then((mod) => mod.ModalForm),
  { ssr: false }
);

function UpdateBpjs({ pttId, initialValues }) {
  const [show, setShow] = useState(false);
  const { id_ptt_bpjs: id, ...currentInitialValues } = initialValues;

  const ref = useRef();
  useEffect(() => {}, [initialValues]);

  const client = useQueryClient();
  const mutation = useMutation((data) => updatePttBpjs(data), {
    onSettled: () => {
      client.invalidateQueries(["ptt-bpjs", pttId]);
    },
    onSuccess: () => {
      ref.current?.resetFields();
      setShow(false);
      message.success("Data berhasil dirubah");
    },
  });

  return (
    <ModalForm
      formRef={ref}
      visible={show}
      onVisibleChange={(visible) => {
        if (visible) {
          ref.current.setFieldsValue({ ...currentInitialValues });
        }
        setShow(visible);
      }}
      onFinish={async (values) => {
        let data;

        const current = omit(values, "fileList");
        const [file] = values?.fileList;
        const fileBpjs = file?.originFileObj;
        data = omitBy({ ...current, bpjs: fileBpjs }, isUndefined);
        const formData = new FormData();
        Object.keys(data).forEach((d) => {
          formData.append(d, data[d]);
        });
        await mutation.mutateAsync({ pttId, data: formData, id });
      }}
      title="Edit BPJS"
      trigger={<a>Edit</a>}
    >
      <ProForm.Group>
        <ProFormUploadButton
          max={1}
          name="fileList"
          label="Upload"
          title="Upload BPJS"
          placeholder="Upload"
          fieldProps={{
            multiple: false,
            accept: [".jpg", ".png"],
          }}
          extra="File BPJS"
          rules={[{ required: true, message: "Tidak boleh kosong" }]}
          initialValue={initialValues?.fileList}
        />
      </ProForm.Group>
      <ProForm.Group>
        <ProFormDigit
          name="no_bpjs"
          label="No. BPJS"
          rules={[{ required: true, message: "Tidak boleh kosong" }]}
          initialValue={initialValues?.no_bpjs}
        />
      </ProForm.Group>
      <ProForm.Group>
        <ProFormDigit
          name="kelas"
          label="Kelas"
          rules={[{ required: true, message: "Tidak boleh kosong" }]}
          initialValue={initialValues?.kelas}
        />
      </ProForm.Group>
    </ModalForm>
  );
}

export default UpdateBpjs;
