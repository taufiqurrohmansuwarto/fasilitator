import ProForm, {
  ProFormDatePicker,
  ProFormDigit,
  ProFormText,
  ProFormUploadButton,
} from "@ant-design/pro-form";
import { Button, message } from "antd";
import omit from "lodash/omit";
import dynamic from "next/dynamic";
import { useRef, useState } from "react";
import { useMutation, useQueryClient } from "react-query";
import { createPttJabatan } from "../api/ptt-jabatan";
import ProFormTreeJabatan from "./form/ProFormJabatan";

const ModalForm = dynamic(
  () => import("@ant-design/pro-form").then((mod) => mod.ModalForm),
  { ssr: false }
);

function CreateJabatan({ pttId }) {
  const [show, setShow] = useState(false);

  const ref = useRef();
  const client = useQueryClient();
  const mutation = useMutation((data) => createPttJabatan(data), {
    onSettled: () => {
      client.invalidateQueries(["ptt-jabatan", pttId]);
    },
    onSuccess: () => {
      ref.current?.resetFields();
      setShow(false);
      message.success("Data berhasil ditambahkan");
    },
  });

  return (
    <ModalForm
      formRef={ref}
      visible={show}
      onVisibleChange={(visible) => setShow(visible)}
      onFinish={async (values) => {
        let data;

        const current = omit(values, "fileList");
        const [file] = values?.fileList;
        const fileBpjs = file?.originFileObj;
        data = { ...current, jabatan: fileBpjs };
        const formData = new FormData();
        Object.keys(data).forEach((d) => {
          formData.append(d, data[d]);
        });
        await mutation.mutateAsync({ pttId, data: formData });
      }}
      title="Tambah Jabatan"
      trigger={
        <Button type="primary" onClick={() => setShow(true)}>
          Tambah
        </Button>
      }
    >
      <ProForm.Group>
        <ProFormTreeJabatan
          rules={[{ required: true, message: "Tidak Boleh kosong" }]}
          name="id_jabatan"
          label="Jabatan"
          tooltip="Jabatan"
          placeholder="Jabatan"
        />
      </ProForm.Group>
      <ProForm.Group>
        <ProFormText
          name="no_surat"
          label="Nomor Surat"
          width="md"
          rules={[{ required: true, message: "tidak boleh kosong" }]}
        />
        <ProFormDatePicker
          name="tgl_surat"
          label="Tanggal Surat"
          fieldProps={{
            format: "YYYY-MM-DD",
          }}
          rules={[{ required: true, message: "tidak boleh kosong" }]}
        />
      </ProForm.Group>
      <ProForm.Group>
        <ProFormText
          name="pejabat_penetap"
          label="Pejabat Penatap"
          width="md"
          rules={[{ required: true, message: "tidak boleh kosong" }]}
        />
      </ProForm.Group>
      <ProForm.Group>
        <ProFormDatePicker
          fieldProps={{
            format: "YYYY-MM-DD",
          }}
          name="tgl_mulai"
          label="Tanggal Mulai"
          rules={[{ required: true, message: "tidak boleh kosong" }]}
        />
        <ProFormDatePicker
          fieldProps={{
            format: "YYYY-MM-DD",
          }}
          name="tgl_akhir"
          label="Tanggal Akhir"
          rules={[{ required: true, message: "tidak boleh kosong" }]}
        />
      </ProForm.Group>
      <ProForm.Group>
        <ProFormDigit name="gaji" label="Gaji" width="md" />
      </ProForm.Group>
      <ProForm.Group>
        <ProFormUploadButton
          max={1}
          name="fileList"
          label="Upload"
          title="Upload SK"
          placeholder="Upload"
          fieldProps={{
            multiple: false,
            accept: [".pdf"],
          }}
          extra="File SK Jabatan"
          rules={[{ required: true, message: "Tidak boleh kosong" }]}
        />
      </ProForm.Group>
    </ModalForm>
  );
}

export default CreateJabatan;
