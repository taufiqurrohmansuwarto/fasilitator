import ProForm, {
  ProFormDatePicker,
  ProFormSelect,
  ProFormText,
} from "@ant-design/pro-form";
import { Button, message } from "antd";
import dynamic from "next/dynamic";
import { useRef, useState } from "react";
import { useMutation, useQueryClient } from "react-query";
import { createPttAnak } from "../api/ptt-anak";

const ModalForm = dynamic(
  () => import("@ant-design/pro-form").then((mod) => mod.ModalForm),
  { ssr: false }
);

function CreateAnak({ refSuamiIstri, refStatusAnak, refPekerjaanAnak, pttId }) {
  const [show, setShow] = useState(false);

  const client = useQueryClient();
  const mutation = useMutation((data) => createPttAnak(data), {
    onSettled: () => {
      client.invalidateQueries(["rwyt-anak", pttId]);
    },
    onSuccess: () => {
      ref.current?.resetFields();
      setShow(false);
      message.success("Berhasil ditambahkan");
    },
  });

  const ref = useRef();

  return (
    <ModalForm
      formRef={ref}
      visible={show}
      onVisibleChange={(visible) => setShow(visible)}
      onFinish={async (data) => {
        const currentData = { pttId, data };
        mutation.mutateAsync(currentData);
      }}
      title="Tambah Anak"
      trigger={
        <Button type="primary" onClick={() => setShow(true)}>
          Tambah
        </Button>
      }
    >
      <ProForm.Group>
        <ProFormSelect
          name="suami_istri_id"
          width="md"
          label="Nama Orang Tua Ayah / Ibu"
          options={refSuamiIstri}
          fieldProps={{
            showSearch: true,
            optionFilterProp: "label",
          }}
        />
        <ProFormText
          name="nama"
          width="md"
          label="Nama Lengkap Anak"
          rules={[{ required: true, message: "tidak boleh kosong" }]}
        />
      </ProForm.Group>

      <ProForm.Group>
        <ProFormText
          name="tempat_lahir"
          width="md"
          label="Tempat Lahir"
          rules={[{ required: true, message: "tidak boleh kosong" }]}
        />

        <ProFormDatePicker
          name="tgl_lahir"
          width="md"
          label="Tanggal Lahir"
          rules={[{ required: true, message: "tidak boleh kosong" }]}
          fieldProps={{
            format: "YYYY-MM-DD",
          }}
        />
      </ProForm.Group>

      <ProForm.Group>
        <ProFormSelect
          name="status_anak_id"
          width="md"
          label="Status Anak"
          options={refStatusAnak}
          fieldProps={{
            showSearch: true,
            optionFilterProp: "label",
          }}
          rules={[{ required: true, message: "tidak boleh kosong" }]}
        />
        <ProFormSelect
          name="pekerjaan_anak_id"
          width="md"
          label="Pekerjaan Anak"
          options={refPekerjaanAnak}
          fieldProps={{
            showSearch: true,
            optionFilterProp: "label",
          }}
          rules={[{ required: true, message: "tidak boleh kosong" }]}
        />
      </ProForm.Group>
    </ModalForm>
  );
}

export default CreateAnak;
