import ProForm, {
  ProFormDatePicker,
  ProFormDigit,
  ProFormText,
  ProFormUploadButton,
} from "@ant-design/pro-form";
import { Col, Divider, message, Row, Select, Typography } from "antd";
import isUndefined from "lodash/isUndefined";
import omit from "lodash/omit";
import omitBy from "lodash/omitBy";
import moment from "moment";
import dynamic from "next/dynamic";
import { useEffect, useRef, useState } from "react";
import { useMutation, useQueryClient } from "react-query";
import { updatePttPendiidkan } from "../api/ptt-pendidikan";

function beforeUpload(file) {
  const isPdf = file.type === "application/pdf";
  if (!isPdf) {
    message.error("You can only upload PDF file!");
  }
  const isLt1M = file.size / 1024 / 1024 < 1;
  if (!isLt1M) {
    message.error("File must smaller than 1MB!");
  }

  return isPdf && isLt1M;
}

const ModalForm = dynamic(
  () => import("@ant-design/pro-form").then((mod) => mod.ModalForm),
  { ssr: false }
);

const FormPendidikan = ({ jenjangId }) => {
  if (!jenjangId) {
    return null;
  }

  if (jenjangId > 3) {
    return <FormPT />;
  }

  if (jenjangId < 4) {
    return <FormSMA />;
  }
};

const FormSMA = () => {
  return (
    <>
      <ProForm.Group>
        <ProFormText
          width="xl"
          name="nama_sekolah_sma"
          label="Nama Sekolah SMA"
          rules={[{ required: true, message: "Tidak boleh kosong" }]}
        />
      </ProForm.Group>
      <ProForm.Group>
        <ProFormText
          width="md"
          name="jurusan_sma"
          label="Jurusan SMA"
          rules={[{ required: true, message: "Tidak boleh kosong" }]}
        />
        <ProFormText
          name="akreditasi_sma"
          label="Akreditasi SMA"
          required
          rules={[{ required: true, message: "Tidak boleh kosong" }]}
        />
      </ProForm.Group>
      <ProForm.Group>
        <ProFormDatePicker.Year
          name="thn_lulus_sma"
          label="Tahun Lulus SMA"
          rules={[{ required: true, message: "Tidak boleh kosong" }]}
        />
        <ProFormText
          name="no_ijazah_sma"
          label="Nomor Ijazah SMA"
          rules={[{ required: true, message: "Tidak boleh kosong" }]}
        />
        <ProFormDatePicker
          name="tgl_ijazah_sma"
          label="Tgl. Ijazah SMA"
          rules={[{ required: true, message: "Tidak boleh kosong" }]}
        />
      </ProForm.Group>
      <ProForm.Group>
        <ProFormDigit
          name="nilai_akhir_sma"
          label="Nilai Akhir SMA"
          rules={[{ required: true, message: "Tidak boleh kosong" }]}
        />
        <ProFormDigit
          name="nilai_un_sma"
          label="Nilai UN SMA"
          width="md"
          rules={[{ required: true, message: "Tidak boleh kosong" }]}
        />
      </ProForm.Group>
      <ProForm.Group>
        <ProFormUploadButton
          max={1}
          name="fileNilaiSma"
          fieldProps={{
            accept: [".pdf"],
            beforeUpload,
          }}
          title="Upload File Nilai"
          label="File Nilai"
          rules={[{ required: true, message: "Tidak boleh kosong" }]}
        />
        <ProFormUploadButton
          max={1}
          name="fileIjazahSma"
          fieldProps={{
            accept: [".pdf"],
            beforeUpload,
          }}
          title="Upload File Ijazah"
          label="File Ijazah"
          rules={[{ required: true, message: "Tidak boleh kosong" }]}
        />
      </ProForm.Group>
    </>
  );
};

const FormPT = () => {
  return (
    <>
      <ProForm.Group>
        <ProFormText
          width="xl"
          name="nama_pt"
          label="Nama Perguruan Tinggi"
          rules={[{ required: true, message: "Tidak boleh kosong" }]}
        />
      </ProForm.Group>
      <ProForm.Group>
        <ProFormText
          width="sm"
          name="fakultas_pt"
          label="Fakultas Perguruan Tinggi"
          rules={[{ required: true, message: "Tidak boleh kosong" }]}
        />

        <ProFormText
          name="akreditasi_pt"
          label="Akreditasi PT"
          required
          rules={[{ required: true, message: "Tidak boleh kosong" }]}
        />
      </ProForm.Group>
      <ProForm.Group>
        <ProFormText
          name="jurusan_prodi_pt"
          width="lg"
          label="Jurusan Prodi"
          required
          rules={[{ required: true, message: "Tidak boleh kosong" }]}
        />
      </ProForm.Group>
      <ProForm.Group>
        <ProFormDatePicker.Year
          name="thn_lulus_pt"
          label="Tahun Lulus"
          rules={[{ required: true, message: "Tidak boleh kosong" }]}
        />
        <ProFormText
          name="no_ijazah_pt"
          label="Nomor Ijazah PT"
          rules={[{ required: true, message: "Tidak boleh kosong" }]}
          width="md"
        />
      </ProForm.Group>
      <ProForm.Group>
        <ProFormDatePicker
          name="tgl_ijazah_pt"
          label="Tgl. Ijazah"
          rules={[{ required: true, message: "Tidak boleh kosong" }]}
        />
        <ProFormDigit
          name="ipk_pt"
          width="md"
          label="IPK"
          rules={[{ required: true, message: "Tidak boleh kosong" }]}
        />
      </ProForm.Group>
      <ProForm.Group>
        <ProFormUploadButton
          width="md"
          max={1}
          name="fileNilaiPT"
          fieldProps={{
            accept: [".pdf"],
            beforeUpload,
          }}
          title="Upload File Nilai"
          label="File Nilai"
          rules={[{ required: true, message: "Tidak boleh kosong" }]}
        />
        <ProFormUploadButton
          width="md"
          max={1}
          name="fileIjazahPT"
          fieldProps={{
            accept: [".pdf"],
            beforeUpload,
          }}
          title="Upload File Ijazah"
          label="File Ijazah"
          rules={[{ required: true, message: "Tidak boleh kosong" }]}
        />
      </ProForm.Group>
    </>
  );
};

const dataSend = (jenjang, data) => {
  return {
    id_jenjang: jenjang,
    ...data,
  };
};

function UpdatePendidikan({ pttId, dataJenjang, initialValues }) {
  const [show, setShow] = useState(false);
  const [jenjang, setJenjang] = useState(initialValues?.id_jenjang);

  useEffect(() => {}, [jenjang]);

  const ref = useRef();
  const client = useQueryClient();
  const mutation = useMutation((data) => updatePttPendiidkan(data), {
    onSettled: () => {
      client.invalidateQueries(["ptt-pendidikan", pttId]);
    },
    onSuccess: () => {
      ref.current?.resetFields();
      setShow(false);
      message.success("Data berhasil ditambahkan");
    },
  });

  const dataOmitIjazahDanNilai = (jenjang, currentData) => {
    if (jenjang < 4) {
      return {
        ...omit(currentData, ["fileNilaiSma", "fileIjazahSma"]),
        fileNilai: currentData?.fileNilaiSma?.[0]?.originFileObj,
        fileIjazah: currentData?.fileIjazahSma?.[0]?.originFileObj,
      };
    } else {
      return {
        ...omit(currentData, ["fileNilaiPT", "fileIjazahPT"]),
        fileNilai: currentData?.fileNilaiPT?.[0]?.originFileObj,
        fileIjazah: currentData?.fileIjazahPT?.[0]?.originFileObj,
      };
    }
  };

  return (
    <ModalForm
      initialValues={initialValues}
      formRef={ref}
      visible={show}
      onVisibleChange={(visible) => {
        if (visible) {
          if (initialValues.id_jenjang < 4) {
            ref.current?.setFieldsValue({
              ...initialValues,
              thn_lulus_sma: moment(
                initialValues?.thn_lulus_sma,
                "YYYY"
              ).valueOf(),
            });
          } else {
            ref.current?.setFieldsValue({
              ...initialValues,
              thn_lulus_pt: moment(
                initialValues?.thn_lulus_pt,
                "YYYY"
              ).valueOf(),
            });
          }
        }
        setShow(visible);
      }}
      onFinish={async (values) => {
        const currentData = dataSend(jenjang, values);
        const data = {
          ...dataOmitIjazahDanNilai(jenjang, currentData),
        };

        //  send it without undefined
        const dataSending = omitBy(data, isUndefined);

        const formData = new FormData();
        Object.keys(dataSending).forEach((d) => {
          formData.append(d, dataSending[d]);
        });

        await mutation.mutateAsync({
          pttId,
          data: formData,
          id: initialValues?.id_ptt_pendidikan,
        });
      }}
      title="Tambah Pendidikan"
      trigger={<a onClick={() => setShow(true)}>Edit</a>}
    >
      <Row>
        <Col>
          <Typography.Title level={5}>Pilih Pendidikan</Typography.Title>
          <Select
            style={{ width: 400 }}
            optionFilterProp="title"
            showSearch
            allowClear
            value={jenjang}
            onChange={(e) => setJenjang(e)}
          >
            {dataJenjang?.map((jenjang) => (
              <Select.Option
                key={jenjang?.id_jenjang}
                value={jenjang?.id_jenjang}
                title={jenjang?.nama_jenjang}
              >
                {jenjang?.nama_jenjang}
              </Select.Option>
            ))}
          </Select>
          <Divider />
        </Col>
        <FormPendidikan jenjangId={jenjang} />
      </Row>
      <Divider />
    </ModalForm>
  );
}

export default UpdatePendidikan;
