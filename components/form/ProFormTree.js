import ProForm from "@ant-design/pro-form";
import { TreeSelect } from "antd";
import { useQuery } from "react-query";
import { fetchSkpds } from "../../api/ref";

const ProFormTree = (props) => {
  const { data, isLoading } = useQuery("skpd", fetchSkpds);
  return (
    <>
      {!isLoading && (
        <ProForm.Item {...props} style={{ width: 500 }}>
          <TreeSelect
            style={{ width: "100%" }}
            showSearch
            treeNodeFilterProp="label"
            treeData={data}
            treeDefaultExpandAll
            placeholder={props.placeholder}
            {...props.fieldProps}
          />
        </ProForm.Item>
      )}
    </>
  );
};

export default ProFormTree;
