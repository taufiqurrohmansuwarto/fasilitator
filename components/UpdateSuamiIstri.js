import ProForm, {
  ProFormDatePicker,
  ProFormSelect,
  ProFormText,
} from "@ant-design/pro-form";
import { message } from "antd";
import moment from "moment";
import dynamic from "next/dynamic";
import { useEffect, useRef, useState } from "react";
import { useMutation, useQueryClient } from "react-query";
import { updatePttSuamiIstri } from "../api/ptt-suami-istri";

const ModalForm = dynamic(
  () => import("@ant-design/pro-form").then((mod) => mod.ModalForm),
  { ssr: false }
);

function UpdateSuamiIstri({
  refStatusPernikahan,
  refStatusPekerjaan,
  pttId,
  initialValues,
}) {
  const [show, setShow] = useState(false);
  const { suami_istri_id: id } = initialValues;

  const client = useQueryClient();
  const mutation = useMutation((data) => updatePttSuamiIstri(data), {
    onSettled: () => {
      client.invalidateQueries(["rwyt-suami-istri", pttId]);
    },
  });

  const ref = useRef();
  useEffect(() => {}, [initialValues]);

  return (
    <ModalForm
      formRef={ref}
      visible={show}
      onVisibleChange={(visible) => {
        if (visible) {
          ref?.current?.setFieldsValue({
            ...initialValues,
            tgl_lahir: moment(initialValues?.tgl_lahir),
          });
        }
        setShow(visible);
      }}
      onFinish={async (values) => {
        const data = { data: values, pttId, id };
        await mutation.mutateAsync(data);
        setShow(false);
        ref?.current?.resetFields();
        message.success("Update Suami Istri Berhasil");
      }}
      title="Tambah Suami/Istri"
      trigger={<a>Edit</a>}
    >
      <ProForm.Group>
        <ProFormText
          name="nama_suami_istri"
          width="md"
          label="Nama Lengkap Suami / Istri"
          rules={[{ required: true, message: "tidak boleh kosong" }]}
          initialValue={initialValues?.nama_suami_istri}
        />
      </ProForm.Group>
      <ProForm.Group>
        <ProFormSelect
          name="status_suami_istri_id"
          width="md"
          label="Status Dalam Pernikahan"
          options={refStatusPernikahan}
          fieldProps={{
            showSearch: true,
            optionFilterProp: "label",
          }}
          rules={[{ required: true, message: "tidak boleh kosong" }]}
          initialValue={initialValues?.status_suami_istri_id}
        />
      </ProForm.Group>
      <ProForm.Group>
        <ProFormText
          name="tempat_lahir"
          width="md"
          label="Tempat Lahir"
          rules={[{ required: true, message: "tidak boleh kosong" }]}
          initialValue={initialValues?.status_suami_istri_id}
        />
        <ProFormDatePicker
          name="tgl_lahir"
          width="md"
          label="Tanggal Lahir"
          rules={[{ required: true, message: "tidak boleh kosong" }]}
          fieldProps={{
            format: "YYYY-MM-DD",
          }}
          initialValue={moment(initialValues?.tgl_lahir)}
        />
      </ProForm.Group>
      <ProForm.Group>
        <ProFormText
          name="instansi"
          width="md"
          label="Instansi/Tempat Bekerja"
          rules={[{ required: true, message: "tidak boleh kosong" }]}
          initialValue={initialValues?.instansi}
        />
        <ProFormSelect
          name="pekerjaan_id"
          width="md"
          label="Status Pekerjaan"
          options={refStatusPekerjaan}
          fieldProps={{
            showSearch: true,
            optionFilterProp: "label",
          }}
          rules={[{ required: true, message: "tidak boleh kosong" }]}
          initialValue={initialValues?.pekerjaan_id}
        />
      </ProForm.Group>
    </ModalForm>
  );
}

export default UpdateSuamiIstri;
