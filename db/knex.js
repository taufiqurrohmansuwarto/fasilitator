import knex from "knex";
import config from "../knexfile";

const env = process.env.NODE_ENV?.trim();

const currentDbConfig = config?.[env];

/**
 * Global is used here to ensure the connection
 * is cached across hot-reloads in development
 *
 * see https://github.com/vercel/next.js/discussions/12229#discussioncomment-83372
 */
let cached = global.mysql;
if (!cached) cached = global.mysql = {};

export function getKnex() {
  if (!cached.instance) cached.instance = knex(currentDbConfig);
  return cached.instance;
}
