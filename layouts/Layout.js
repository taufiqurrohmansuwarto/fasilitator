import dynamic from "next/dynamic";
import Link from "next/link";

const ProLayout = dynamic(() => import("@ant-design/pro-layout"), {
  ssr: false,
});

const menuHeaderRender = (logDom, titleDom, props) => {
  return (
    <Link href="/">
      <a>
        {logDom}
        {!props?.collapsed && titleDom}
      </a>
    </Link>
  );
};

const menuItemRender = (options, element) => {
  return (
    <Link href={options.path}>
      <a>{element}</a>
    </Link>
  );
};

function Layout({ children, session }) {
  return (
    <ProLayout
      waterMarkProps={{
        content: "Pro Layout",
      }}
      style={{ minHeight: "100vh" }}
      layout="top"
      headerTheme="dark"
      navTheme="light"
      route={session?.user?.menu}
      menuItemRender={menuItemRender}
      menuHeaderRender={menuHeaderRender}
      breakpoint={false}
      title="Fasilitator"
      collapsed
    >
      {children}
    </ProLayout>
  );
}

export default Layout;
