import { DatabaseOutlined } from "@ant-design/icons";
import Link from "next/link";
import { useRouter } from "next/router";

const ProLayout = dynamic(() => import("@ant-design/pro-layout"), {
  ssr: false,
});

const menuHeaderRender = (logDom, titleDom, props) => (
  <Link href="/">
    <a>
      {logDom}
      {!props?.collapsed && titleDom}
    </a>
  </Link>
);

const menuItemRender = (options, element) => {
  const router = useRouter();
  const {
    query: { pttId },
  } = router;

  return (
    <Link href={`/biodata/${pttId}${options.path}`}>
      <a>{element}</a>
    </Link>
  );
};

const routes = {
  routes: [
    {
      name: "Data PTT-PK",
      icon: <DatabaseOutlined />,
      routes: [
        { path: "biodata", name: "Biodata" },
        { path: "suami-istri", name: "Suami Istri" },
        { path: "anak", name: "Anak" },
        { path: "pendidikan", name: "Pendidikan" },
        { path: "jabatan", name: "Jabatan" },
        { path: "penilaian", name: "Penilaian" },
        { path: "bpjs", name: "BPJS" },
      ],
    },
  ],
};

function BiodataLayout({ children, session }) {
  return (
    <ProLayout
      waterMarkProps={{
        content: "Pro Layout",
      }}
      style={{ minHeight: "100vh" }}
      // menu={{ defaultOpenAll: true }}
      layout="side"
      collapsed
      title="Biodata"
      route={routes}
      menuItemRender={menuItemRender}
      menuHeaderRender={menuHeaderRender}
      breakpoint={false}
    >
      {children}
    </ProLayout>
  );
}

export default BiodataLayout;
